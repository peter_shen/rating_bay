package com.ratingbay.cache;
  
import java.util.*; 

public class CacheManager {
    private HashMap<String, CachePool> allCatsCachePool = new HashMap<String, CachePool>();
    
    private CacheManager(){}
    private static CacheManager self = null;
    public static CacheManager getInstance(){
        if(null == self){
            self = new CacheManager();
        }
        return self;
    }

    private boolean isEnable = true;
    public boolean getIsEnable(){
        return this.isEnable;
    }
    public void setIsEnable(boolean isEnable){
        this.isEnable = isEnable;
    }

    protected CachePool cachePool;
    protected CachePool getCachePool(String cat){
        if(allCatsCachePool.containsKey(cat)){
            cachePool = allCatsCachePool.get(cat);
        }else{
            CacheConfig cacheConfig = CacheConfigManager.getInstance().getCacheConfig(cat);
            cachePool = new CachePool(cacheConfig);
            allCatsCachePool.put(cat, cachePool);
        }
        return cachePool;
    }

    public Object get(String key){
        if(getIsEnable()){
            return getCachePool(CacheCat.DEFAULT).get(key);
        }else{
            return null;
        }
    }
    public Object get(String cat, String key){
        if(getIsEnable()){
            return getCachePool(cat).get(key);
        }else{
            return null;
        }
    }
    public Object get(String cat, String key, String surfix){
        if(getIsEnable()){
            return getCachePool(cat).get(key+surfix);
        }else{
            return null;
        }
    }
    public Object get(String cat, String key, Long surfix){
        if(getIsEnable()){
            return getCachePool(cat).get(key+surfix);
        }else{
            return null;
        }
    }
    public Object get(String cat, String key, int surfix){
        if(getIsEnable()){
            return getCachePool(cat).get(key+surfix);
        }else{
            return null;
        }
    }

    public void put(String key, Object value){
        if(getIsEnable()){
            getCachePool(CacheCat.DEFAULT).put(key, value);
        }
    }
    public void put(String cat, String key, Object value){
        if(getIsEnable()){
           getCachePool(cat).put(key, value);
        }
    }
    public void put(String cat, String key, String surfix, Object value){
        if(getIsEnable()){
           getCachePool(cat).put(key+surfix, value);
        }
    }
    public void put(String cat, String key, Long surfix, Object value){
        if(getIsEnable()){
           getCachePool(cat).put(key+surfix, value);
        }
    }
    public void put(String cat, String key, int surfix, Object value){
        if(getIsEnable()){
           getCachePool(cat).put(key+surfix, value);
        }
    }

    public void del(String key){
        if(getIsEnable()){
            getCachePool(CacheCat.DEFAULT).remove(key);
        }
    }
    public void del(String cat, String key){
        if(getIsEnable()){
            getCachePool(cat).remove(key);
        }
    }
    public void del(String cat, String key, String surfix){
        if(getIsEnable()){
            getCachePool(cat).remove(key+surfix);
        }
    }
    public void del(String cat, String key, Long surfix){
        if(getIsEnable()){
            getCachePool(cat).remove(key+surfix);
        }
    }
    public void del(String cat, String key, int surfix){
        if(getIsEnable()){
            getCachePool(cat).remove(key+surfix);
        }
    }

    public boolean isExist(String key){
        if(getIsEnable()){
           return getCachePool(CacheCat.DEFAULT).containsKey(key);
        }else{
           return false;
        }
    }
    public boolean isExist(String cat, String key){
        if(getIsEnable()){
           return getCachePool(cat).containsKey(key);
        }else{
           return false;
        }
    }
    public boolean isExist(String cat, String key, String surfix){
        if(getIsEnable()){
           return getCachePool(cat).containsKey(key+surfix);
        }else{
           return false;
        }
    }
    public boolean isExist(String cat, String key, Long surfix){
        if(getIsEnable()){
           return getCachePool(cat).containsKey(key+surfix);
        }else{
           return false;
        }
    }
    public boolean isExist(String cat, String key, int surfix){
        if(getIsEnable()){
           return getCachePool(cat).containsKey(key+surfix);
        }else{
           return false;
        }
    }

    public void clear() {
        if(getIsEnable()){
           allCatsCachePool.clear();
        }
    }
    public void clear(String cat){
        if(getIsEnable()){
           getCachePool(cat).clear();
        }
    }
}  