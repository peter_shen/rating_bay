package com.ratingbay.cache;

public class CacheConfig{
	public CacheConfig(int cachePoolSize){
		this.cachePoolSize = cachePoolSize;
	}
	public CacheConfig(long expireSecends, int cachePoolSize){
		this.expireSecends = expireSecends;
		this.cachePoolSize = cachePoolSize;
	}

	private long expireSecends = (long)0;
	public long getExpireSecends(){
		return expireSecends;
	}

	private int cachePoolSize;
	public int getCachePoolSize(){
		return cachePoolSize;
	}
	public void setCachePoolSize(int cachePoolSize){
		this.cachePoolSize = cachePoolSize;
	}

	private boolean isEnable = true;
	public boolean getIsEnable(){
		return this.isEnable;
	}
	public void setIsEnable(boolean isEnable){
		this.isEnable = isEnable;
	}
}
