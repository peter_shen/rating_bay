package com.ratingbay.cache;

import java.util.*;

class CachePool extends LinkedHashMap{

    private CacheConfig cacheConfig;
    public CachePool(CacheConfig cacheConfig){
        this.cacheConfig = cacheConfig;
    }

    protected boolean removeEldestEntry(Map.Entry eldest) {
        //eldest will be removed, you can do some operations here
        return this.size() > cacheConfig.getCachePoolSize();
    }

    private HashMap<Object, Date> cacheExpireMaps = new HashMap<Object, Date>();

    public Object get(Object key){
    	if(cacheConfig.getIsEnable()){
    		if((long)0 < cacheConfig.getExpireSecends()){
    			if( cacheExpireMaps.get(key).after(new Date(System.currentTimeMillis())) ){
	    			return super.get(key);
	    		}else{
	    			cacheExpireMaps.remove(key);
	    			super.remove(key);
	    			return null;
	    		}
    		}else{
    			return super.get(key);
    		}
    	}else{
    		return null;
    	}
    }

    public Object put(Object key, Object value){
    	if(cacheConfig.getIsEnable()){
    		if((long)0 < cacheConfig.getExpireSecends()){
    			Date toExpire = new Date(System.currentTimeMillis() + (long)cacheConfig.getExpireSecends()*1000);
    		    cacheExpireMaps.put(key, toExpire);
    		}
    		return super.put(key, value);
    	}else{
    		return null;
    	}
    }

    public Object remove(Object key){
    	if(cacheConfig.getIsEnable()){
    		if((long)0 < cacheConfig.getExpireSecends()){
    			cacheExpireMaps.remove(key);
    		}
    		return super.remove(key);
    	}else{
    		return null;
    	}
    }

    public boolean containsKey(Object key){
    	if(cacheConfig.getIsEnable()){
    		return super.containsKey(key);
    	}else{
    		return false;
    	}
    }

    public void clear(){
		if(cacheConfig.getIsEnable()){
			cacheExpireMaps.clear();
    		super.clear();
    	}
    }

}
