package com.ratingbay.cache;

public class CacheCat{
	public static final String DEFAULT   = "DEFAULT";
	
    public static final String HOME_PAGE = "HOME_PAGE";
    public static final String USER_PAGE = "USER_PAGE";
    public static final String GAME_PAGE = "GAME_PAGE";
    public static final String TWITTER_USER   = "TWITTER_USER";

    public static final String TWEET_API  = "TWEET_API";
    public static final String LINK_API   = "LINK_API";
    public static final String GAME_API   = "GAME_API";
    public static final String WORD_API   = "WORD_API";
    public static final String INFLUENTIAL_USER_API = "INFLUENTIAL_USER_API";
}