package com.ratingbay.cache;

import java.util.*; 

public class CacheConfigManager{

    private HashMap<String, CacheConfig> cacheConfigPool = new HashMap<String, CacheConfig>();
	private CacheConfigManager(){

		//CacheCat.DEFAULT
		CacheConfig cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.DEFAULT, cacheConfig);
		
		//CacheCat.HOME_PAGE
		cacheConfig = new CacheConfig(500);
        cacheConfigPool.put(CacheCat.HOME_PAGE, cacheConfig);

		//CacheCat.USER_PAGE
		cacheConfig = new CacheConfig(800);
        cacheConfigPool.put(CacheCat.USER_PAGE, cacheConfig);

        //CacheCat.GAME_PAGE
		cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.GAME_PAGE, cacheConfig);

        //CacheCat.GAME_API
		cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.GAME_API, cacheConfig);

        //CacheCat.LINK_API
		cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.LINK_API, cacheConfig);

        //CacheCat.TWEET_API
		cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.TWEET_API, cacheConfig);

        //CacheCat.INFLUENTIAL_USER_API
        cacheConfig = new CacheConfig(1000);
        cacheConfigPool.put(CacheCat.INFLUENTIAL_USER_API, cacheConfig);

        //CacheCat.TWITTER_USER
        cacheConfig = new CacheConfig(100000);
        cacheConfigPool.put(CacheCat.TWITTER_USER, cacheConfig);

	}

	private static CacheConfigManager self = null;
	public static CacheConfigManager getInstance(){
        if(null == self){
        	self = new CacheConfigManager();
        }
        return self;
	}

	public CacheConfig getCacheConfig(String cat){
		return cacheConfigPool.get(cat);
	}
}

