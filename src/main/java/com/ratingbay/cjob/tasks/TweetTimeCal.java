package com.ratingbay.cjob.tasks; 

import java.io.File;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimerTask;  

import javax.servlet.ServletContext;  

import com.ratingbay.console.naivequery.TweetTimeNaiveQuery;

public class TweetTimeCal extends TimerTask {
    private static boolean isRunning = false;
    private ServletContext context = null;
    public TweetTimeCal(ServletContext context)  {
        this.context = context;
    }

    @Override
    public void run() {
        if(!isRunning) {
            isRunning = true;
            
            Date beginTime = new Date( System.currentTimeMillis() - 5184000000L );
            Date curDate = new Date(System.currentTimeMillis());
            
            List<Long> allIdGames = TweetTimeNaiveQuery.getInstance().getAllIdGames();
            TweetTimeNaiveQuery.getInstance().analysisTweetAndLink( allIdGames,beginTime,curDate );

            isRunning = false;  
        }else {
            //上一次任务执行还未结束
        }
    }
}