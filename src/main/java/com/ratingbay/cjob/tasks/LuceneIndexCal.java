package com.ratingbay.cjob.tasks; 

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;  
import javax.servlet.ServletContext;  

import com.ratingbay.console.naivequery.GameSummaryNaiveQuery;
import com.ratingbay.console.naivequery.IndexNaiveQuery;
import com.ratingbay.lucene.LuceneQuery;

public class LuceneIndexCal extends TimerTask {
    private static boolean isRunning = false;
    private ServletContext context = null;
    public LuceneIndexCal(ServletContext context)  {
        this.context = context;
    }

    @Override
    public  synchronized void run() {
        if(!isRunning) {
            isRunning = true;

            IndexNaiveQuery.getInstance().indexTwitterUser();

            isRunning = false;  
        }else {
            //上一次任务执行还未结束
        }
    }
}