package com.ratingbay.cjob.tasks; 

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.TimerTask;  
import javax.servlet.ServletContext;  

import com.ratingbay.console.naivequery.GameSummaryNaiveQuery;

public class GameSummaryCal extends TimerTask {
    private static boolean isRunning = false;
    private ServletContext context = null;
    public GameSummaryCal(ServletContext context)  {
        this.context = context;
    }

    @Override
    public void run() {
        if(!isRunning) {
            isRunning = true;

            List<Long> allIdGames = GameSummaryNaiveQuery.getInstance().getAllIdGames();
            GameSummaryNaiveQuery.getInstance().assembleGameSummary( allIdGames );

            isRunning = false;  
        }else {
            //上一次任务执行还未结束
        }
    }
}