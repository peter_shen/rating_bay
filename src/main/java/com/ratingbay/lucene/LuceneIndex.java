package com.ratingbay.lucene;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ratingbay.console.common.RatingBayConst;
import com.ratingbay.console.model.TwitterUser;
import com.ratingbay.console.model.User;
import com.ratingbay.console.naivequery.TweetNaiveQuery;

public class LuceneIndex {

	private static Logger log = LoggerFactory.getLogger(LuceneIndex.class);
	
	private LuceneIndex(){
	}
	private static LuceneIndex self = null;
    public static LuceneIndex getInstance(){
    	if(null == self){
    		self = new LuceneIndex();
    	}
        return self;
    }
    
	public  void index(List<TwitterUser> list) {
		IndexWriter writer = null;
		boolean flag=false;
		try {
			File f=new File(RatingBayConst.LUCENE_FILE_PATH);
			if(!f.exists())
				f.mkdirs();
			Directory dir = FSDirectory.open(f);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_48);
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_48,
					analyzer);
			if(f.isDirectory()&&f.listFiles().length==0){
				iwc.setOpenMode(OpenMode.CREATE);
				System.out.println("init index path");
			}
			else{
				iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
				flag=true;
			}
			writer = new IndexWriter(dir, iwc);
			for (TwitterUser u : list) {
				if(flag){
					if(LuceneQuery.searchById(u.getUserId())!=null){
						System.out.println("duplicate row data");
						return;
					}
				}
				Document doc = new Document();
				doc.add(new LongField("id", u.getUserId(), Field.Store.YES));
				doc.add(new LongField("followersCount", u
						.getUserFollowersCount()==null?0l:u.getUserFollowersCount(), Field.Store.YES));
				doc.add(new LongField("friendsCount", u.getUserFriendsCount()==null?0l:u.getUserFriendsCount(),
						Field.Store.YES));
				doc.add(new LongField("tweetsCount", u.getUserTweetsCount()==null?0l:u.getUserTweetsCount(),
						Field.Store.YES));
				doc.add(new TextField("username", u.getUsername()==null?"":u.getUsername(),
						Field.Store.NO));
				writer.addDocument(doc);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
