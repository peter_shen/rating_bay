package com.ratingbay.lucene;

import java.util.*;
 
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
 
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.EntityTransaction;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import com.ratingbay.console.common.RatingBayConst;
import com.ratingbay.console.dataimpl.GamePlayerImpl;
import com.ratingbay.console.model.*;
import com.ratingbay.lucene.LuceneIndex;

public class LuceneMergeTask{
	private LuceneMergeTask(){
	}
	private static LuceneMergeTask instance = null;
    public static LuceneMergeTask getInstance(){
    	if(null == instance){
    		instance = new LuceneMergeTask();
    	}
        return instance;
    }


    //cjob 
    public void merge() {
        System.out.println("CJOB start index merge");
		
        IndexWriter writer = null;
		try {
			Directory dir = FSDirectory.open(new File(
					RatingBayConst.LUCENE_FILE_PATH));
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_48);
			IndexWriterConfig iwc = new IndexWriterConfig(Version.LUCENE_48,
					analyzer);
			iwc.setOpenMode(OpenMode.CREATE_OR_APPEND);
			writer = new IndexWriter(dir, iwc);
			writer.forceMerge(1);
			writer.commit();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (writer != null)
					writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

        System.out.println("CJOB end IndexTwitterUser");
    }
    public static void main(String[] args) {
		LuceneMergeTask.getInstance().merge();
	}
}