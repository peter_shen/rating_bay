package com.ratingbay.lucene;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.flexible.core.nodes.RangeQueryNode;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeQuery;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.WildcardQuery;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ratingbay.console.common.RatingBayConst;

public class LuceneQuery {

	private static Logger log = LoggerFactory.getLogger(LuceneIndex.class);

	public static long searchCounts(String fields, String keywords) {
		IndexReader reader = null;
		long l = 0l;
		try {
			reader = DirectoryReader.open(FSDirectory.open(new File(
					RatingBayConst.LUCENE_FILE_PATH)));
			IndexSearcher searcher = new IndexSearcher(reader);
			WildcardQuery q = new WildcardQuery(new Term(fields, "*" + keywords
					+ "*"));
			TopDocs result = searcher.search(q, RatingBayConst.LUCENE_COUNTS);
			l = result.totalHits;
			return l;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
				return l;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return l;
	}

	public static List<Long> search(String fields, String keywords,
			int pageNo, int pageSize, String sortField, Boolean reverse) {
		IndexReader reader = null;
		List<Long> set = new ArrayList<Long>();
		try {
			reader = DirectoryReader.open(FSDirectory.open(new File(
					RatingBayConst.LUCENE_FILE_PATH)));
			IndexSearcher searcher = new IndexSearcher(reader);
			WildcardQuery q = new WildcardQuery(new Term(fields, "*" + keywords
					+ "*"));
			Sort sort =new  Sort();
			if(sortField!=null&&reverse!=null){
				SortField sf = new SortField(sortField, SortField.Type.LONG, reverse);
				sort.setSort(sf);
			}

			TopDocs result = searcher.search(q, RatingBayConst.LUCENE_COUNTS,sort);
			//pageNo从0开始
			int index =pageNo * pageSize;
			ScoreDoc scoreDoc = null;
			// 若是当前页是第一页面scoreDoc=null
			if (index > 0) {
				// 因为索引是从0开端所以要index-1
				scoreDoc = result.scoreDocs[index - 1];
			}
			TopDocs hits = searcher.searchAfter(scoreDoc, q, pageSize,sort);
			for (ScoreDoc sd : hits.scoreDocs) {
				Document doc = searcher.doc(sd.doc);
				set.add(Long.parseLong(doc.get("id")));
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
				return set;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return set;
	}
	
	
	public static Long searchById(Long no) {
		IndexReader reader = null;
		List<Long> set = new ArrayList<Long>();
		try {
			reader = DirectoryReader.open(FSDirectory.open(new File(
					RatingBayConst.LUCENE_FILE_PATH)));
			IndexSearcher searcher = new IndexSearcher(reader);
			NumericRangeQuery<Long> q=NumericRangeQuery.newLongRange("id", no, no, true, true);

			TopDocs result = searcher.search(q, 10);
			for (ScoreDoc sd : result.scoreDocs) {
				Document doc = searcher.doc(sd.doc);
				set.add(Long.parseLong(doc.get("id")));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (reader != null)
					reader.close();
				if(set==null||set.isEmpty())
					return null;
				else
					return set.get(0);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if(set==null||set.isEmpty())
			return null;
		else
			return set.get(0);
	}
	
	
	public static void main(String[] args) {
		
		Long l = searchById(7875762l);
		System.out.println(l);
	}
}
