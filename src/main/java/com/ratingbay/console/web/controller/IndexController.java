package com.ratingbay.console.web.controller;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.restful.builder.*;
import com.ratingbay.console.naivequery.*;

@Controller
@RequestMapping("/index")
public class IndexController extends BaseController{

	@RequestMapping(value="/helloWorld", method=RequestMethod.GET)
	public String helloWorld(){
		return "index";
	}

	@RequestMapping(value="/helloPlain", method=RequestMethod.GET)
	public String helloPlain(PrintWriter out) {
		out.println("helloPlain");
		return null;
	}

	@RequestMapping(value="/helloJson", method=RequestMethod.GET)
	public @ResponseBody 
	Map helloJson() {
		Map dataJson = new HashMap<String, String>();
		dataJson.put("key", "value");
		return dataJson;
	}

	@RequestMapping(value="/helloRedirect", method=RequestMethod.GET)
	public String helloRedirect() {
		return "redirect:/index/helloWorld.do";
	}

	@RequestMapping(value="/helloParam/{name}", method=RequestMethod.GET)
	public ModelAndView helloParam(@PathVariable String name) {
		ModelMap model = new ModelMap();
		model.addAttribute("name",name);
		return new ModelAndView("helloParam", model);
	}

	@RequestMapping(value="/helloRestful", method=RequestMethod.GET)
	public ModelAndView helloRestful() {
		ModelMap model = new ModelMap();
		model.addAttribute( "IRestfulViewContentBuilder", 
			new RestfulViewContentBuilderDefaultImpl() );
		model.addAttribute( "name", "Restful");
		return new ModelAndView("restful/helloRestful", model);
	}

	@RequestMapping(value="/updateTweetCount/{passwd}", method=RequestMethod.GET)
	public @ResponseBody 
	Map updateTweetCount(@PathVariable String passwd) {
		Map dataJson = new HashMap<String, String>();
		if(passwd.equals("XHYWNS")){
			LinkNaiveQuery.getInstance().updateTweetCount();
			dataJson.put("operation", "success");
			dataJson.put("pageNum", LinkNaiveQuery.getInstance().updateTweetCountPageNum);
		}else{
			dataJson.put("permisson", "deny");
		}
		return dataJson;
	}
}