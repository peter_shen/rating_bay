package com.ratingbay.console.web.view;

import com.ratingbay.console.restful.view.*;

import org.springframework.web.servlet.view.AbstractUrlBasedView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.tiles2.TilesView;

public class MultiViewResolver extends InternalResourceViewResolver{

    protected boolean isRestfulView(String viewName){
    	return viewName.startsWith("restful");
    }

    @Override
    protected AbstractUrlBasedView buildView(String viewName) throws Exception {

        if( this.isRestfulView(viewName) ){
            this.setViewClass(RestfulView.class);
        }else{
            this.setViewClass(JstlView.class);
        }
        return super.buildView(viewName);

    }
}