package com.ratingbay.console.web.widgets;

import java.util.*;

public class PageWidget {

	public PageWidget(Integer totalPages, Integer curPage){
		this.totalPages = totalPages;
		this.curPage = curPage;
		this.linkPrefix = "dyJHS=dyJHS";
	}

	public PageWidget(Integer totalPages, Integer curPage, String linkPrefix){
		this.totalPages = totalPages;
		this.curPage = curPage;
		this.linkPrefix = linkPrefix;
	}

	public String linkPrefix;
	public String getLinkPrefix(){
		return this.linkPrefix;
	}

	public Integer totalPages;
    public Integer getTotalPages(){
        return this.totalPages;
    }

    public Integer curPage;
    public Integer getCurPage(){
        return this.curPage;
    }

    public boolean getIsFirstPage(){
        boolean isFirstPage = true;
        if( (this.curPage > 0) && (this.curPage < this.totalPages) ){
        	isFirstPage = false;
        }
        return isFirstPage;
    }
    public String getFirstPageLink(){
    	return this.linkPrefix + "&pageNum=0";
    }
    protected Integer getPrevPage(){
        return this.curPage - 1;
    }
    public String getPrevPageLink(){
    	return this.linkPrefix + "&pageNum=" + this.getPrevPage();
    }


    public boolean getIsLastPage(){
        boolean isLastPage = true;
        if( (this.totalPages > 0) && ( (this.curPage+1) != this.totalPages ) ){
        	isLastPage = false;
        }
        return isLastPage;
    }
    public String getLastPageLink(){
    	return this.linkPrefix + "&pageNum=" + (this.totalPages -1);
    }
    protected Integer getNextPage(){
        return this.curPage + 1;
    }
    public String getNextPageLink(){
    	return this.linkPrefix + "&pageNum=" + this.getNextPage();
    }


    class PageInfo{
    	public PageInfo(boolean isMorePage){
			this.isMorePage = isMorePage;
		}
		public PageInfo(String link, String display){
			this.link = link;
			this.display = display;
		}
		public PageInfo(String link, String display, boolean isCurPage){
			this.link = link;
			this.display = display;
			this.isCurPage = isCurPage;
		}

		public String link;
		public String getLink(){
			return this.link;
		}

		public String display;
		public String getDisplay(){
			return this.display;
		}

		public boolean isCurPage = false;
		public boolean getIsCurPage(){
			return this.isCurPage;
		}

		public boolean isMorePage = false;
		public boolean getIsMorePage(){
			return this.isMorePage;
		}
	}

    public Integer swingLen = 5;
	public List<PageInfo> getPageList(){
		List<PageInfo> pageList = new ArrayList<PageInfo>();

		Integer start = curPage - swingLen;
		if(start < 0) start = 0;
		
		Integer end = start + 2*swingLen;
		if( (end+1) > totalPages ) end = totalPages - 1;

		if( ((end - start) < 2*swingLen) && (totalPages > 2*swingLen) ){
			start = end - 2*swingLen;
		}

		if(start > 0){
			pageList.add( new PageInfo(getFirstPageLink(), "1") );
			pageList.add( new PageInfo(true) );
		}
		String link;
		String display;
		boolean isCurPage;
		for( Integer i = start; i <= end; i+=1 ){
			link = this.linkPrefix + "&pageNum=" + i;
			display = "" + (i+1);
			isCurPage = (curPage == i);

			pageList.add( new PageInfo(link, display, isCurPage) );
		}
		if( (end+1) < totalPages ){
			pageList.add( new PageInfo(true) );
			pageList.add( new PageInfo( getLastPageLink(), "" + totalPages ) );
		}

		return pageList;
	}

}