package com.ratingbay.console.web.action;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.apache.struts2.interceptor.SessionAware;

import com.ratingbay.console.api.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.dataface.ITwitterUser;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;
import com.ratingbay.console.web.widgets.PageWidget;

public class UserAction extends BaseAction implements SessionAware {

	//input from request
	private Long userId;
	public Long getUserId(){
		return this.userId;
	}
	public void setUserId(Long userId){
		this.userId = userId;
	}
	
	private int pageNum = 0;
    public int getPageNum(){
        return this.pageNum;
    }
    public void setPageNum(int pageNum){
        this.pageNum = pageNum;
    }
    
    private int gpageNum = 0;
    public int getgPageNum(){
        return this.gpageNum;
    }
    public void setgPageNum(int gpageNum){
        this.gpageNum = gpageNum;
    }
	
	private String username;
	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}
	

	//output in view
	UserPageDataBean userPageDataBean;
	public UserPageDataBean getUserPageDataBean(){
		return userPageDataBean;
	}
	public void setUserPageDataBean(UserPageDataBean userPageDataBean){
		this.userPageDataBean = userPageDataBean;
	}


    //load user page
	public String load() {
		if(null == userId && null==username)return "false";
		if(userId!=null){
			Long s1 = System.currentTimeMillis();
			userPageDataBean = new UserPageDataBean();
			userPageDataBean.iTwitterUser =  getTUserApi().getITwitterUser(userId);
			if(null == userPageDataBean.iTwitterUser){
				userPageDataBean.iTwitterUser = getITwitterUser();
				if(null != userPageDataBean.iTwitterUser){
					if( !this.userId.equals(userPageDataBean.iTwitterUser.getId()) ){
						System.out.println( "Logged userId is " + userPageDataBean.iTwitterUser.getId() );
						System.out.println( "But access userId is " + this.userId );
						return "false";
					}
				}else{
					return "false";
				}
			}
			System.out.println("iTwitterUser time is " + (System.currentTimeMillis()-s1));
			//userPageDataBean.userGameCount = getTweetApi().getGameCountByUser(userId);
			Long s2 = System.currentTimeMillis();
			Page<Game> uGames = getGameApi().getGamebyUser(userId, gpageNum, 5);
			System.out.println("....................5 Games time is " + (System.currentTimeMillis()-s2));
			Long s3 = System.currentTimeMillis();
			List<Long> utUserIds = new ArrayList<Long>();
			utUserIds.add(userId);

			Iterator<Game> it = uGames.iterator();
			Game tmpGame = null;
			List <GameAboutDataBean> gameData = new ArrayList<GameAboutDataBean>();
			//PageRequest pageRequest = new PageRequest(pageNum, 5);
			GameAboutDataBean tGADB = null; 
			while(it.hasNext()){
				tmpGame = it.next();
				tGADB = new GameAboutDataBean();
				Long s31 = System.currentTimeMillis();
				tGADB.gameDetail = getGameApi().getGameDetail( tmpGame.getId() );
				System.out.println("gameDetail time is " + (System.currentTimeMillis()-s31));
				Long s32 = System.currentTimeMillis();
				tGADB.gameDetail.setTweetCount( getTweetApi().getTweetCountByGame( tmpGame.getId() ) );
				System.out.println("setTweetCount time is " + (System.currentTimeMillis()-s32));
				Long s33 = System.currentTimeMillis();
				tGADB.latestTweets = getTweetApi().getUsersTweetsByGame( tmpGame.getId(), utUserIds, 0, 5 );
				System.out.println("latestTweets time is " + (System.currentTimeMillis()-s33));
				//ajax load links

				gameData.add(tGADB);
			}
			System.out.println(".......................all gameDetail time is " + (System.currentTimeMillis()-s3));
			Long s4 = System.currentTimeMillis();
			//userPageDataBean.uGameAboutDataBeans = new PageImpl<GameAboutDataBean>(gameData,pageRequest,userPageDataBean.userGameCount);
			userPageDataBean.uGameAboutDataBeans = new PageImpl<GameAboutDataBean>(gameData);
			Integer totalPages = userPageDataBean.uGameAboutDataBeans.getTotalPages();
			userPageDataBean.pageWidget=new PageWidget(totalPages, pageNum);
			
			userPageDataBean.fGameAboutDataBeans = new ArrayList<GameAboutDataBean>();
			List<Long> ftUserIds = getTUserApi().getTwitterFriendsIds(getUserId());
			Set<Long> uids=new HashSet<Long>(ftUserIds);
			uids.add(getUserId());
			List<Long> userIds=new ArrayList<Long>(uids);
			Page<Game> fGames = getGameApi().getGamebyUser(userIds, 0, 5);
			if(null != fGames){
				it = fGames.iterator();
				while(it.hasNext()){
					tmpGame = it.next();
					tGADB = new GameAboutDataBean();
					tGADB.gameDetail = getGameApi().getGameDetail( tmpGame.getId() );
					tGADB.gameDetail.setTweetCount( getTweetApi().getTweetCountByGame( tmpGame.getId() ) );

					tGADB.latestTweets = getTweetApi().getUsersTweetsByGame( tmpGame.getId(), ftUserIds, 0, 5 );
					//ajax load links

					userPageDataBean.fGameAboutDataBeans.add( tGADB );
				}
				if(userIds.size() > 1){
					userPageDataBean.recentActiveGamers = getTweetApi().getGamersByUserIds(ftUserIds, 0 , 5);
				}
			}
			System.out.println("firend time is " + (System.currentTimeMillis()-s4));
		}else{
			Long uid=getTUserApi().getUsersByName(username);
			if(null==uid) return "false";

			userPageDataBean = new UserPageDataBean();
			userPageDataBean.iTwitterUser =  getTUserApi().getITwitterUser(uid);
			if(null == userPageDataBean.iTwitterUser){
				userPageDataBean.iTwitterUser = getITwitterUser();
				if(null != userPageDataBean.iTwitterUser){
					if( !uid.equals(userPageDataBean.iTwitterUser.getId()) ){
						System.out.println( "Logged userId is " + userPageDataBean.iTwitterUser.getId() );
						System.out.println( "But access userId is " + uid );
						return "false";
					}
				}else{
					return "false";
				}
			}
			
			userPageDataBean.userGameCount = getTweetApi().getGameCountByUser(uid);

			Page<Game> uGames = getGameApi().getGamebyUser(uid, gpageNum, 5);
			List<Long> utUserIds = new ArrayList<Long>();
			utUserIds.add(uid);

			Iterator<Game> it = uGames.iterator();
			Game tmpGame = null;
			List <GameAboutDataBean> gameData = new ArrayList<GameAboutDataBean>();
			PageRequest pageRequest = new PageRequest(pageNum, 5);
			GameAboutDataBean tGADB = null; 
			while(it.hasNext()){
				tmpGame = it.next();
				tGADB = new GameAboutDataBean();
				tGADB.gameDetail = getGameApi().getGameDetail( tmpGame.getId() );
				tGADB.gameDetail.setTweetCount( getTweetApi().getTweetCountByGame( tmpGame.getId() ) );

				tGADB.latestTweets = getTweetApi().getUsersTweetsByGame( tmpGame.getId(), utUserIds, 0, 5 );
				//ajax load links

				gameData.add(tGADB);
			}
			userPageDataBean.uGameAboutDataBeans = new PageImpl<GameAboutDataBean>(gameData,pageRequest,userPageDataBean.userGameCount);
			Integer totalPages = userPageDataBean.uGameAboutDataBeans.getTotalPages();
			userPageDataBean.pageWidget=new PageWidget(totalPages, pageNum);

			userPageDataBean.fGameAboutDataBeans = new ArrayList<GameAboutDataBean>();
			
			List<Long> ftUserIds = getTUserApi().getTwitterFriendsIds(uid);
			Set<Long> uids=new HashSet<Long>(ftUserIds);
			uids.add(getUserId());
			List<Long> userIds=new ArrayList<Long>(uids);
			Page<Game> fGames = getGameApi().getGamebyUser(userIds, 0, 5);
			if(null != fGames){
				it = fGames.iterator();
				while(it.hasNext()){
					tmpGame = it.next();
					tGADB = new GameAboutDataBean();
					tGADB.gameDetail = getGameApi().getGameDetail( tmpGame.getId() );
					tGADB.gameDetail.setTweetCount( getTweetApi().getTweetCountByGame( tmpGame.getId() ) );

					tGADB.latestTweets = getTweetApi().getUsersTweetsByGame( tmpGame.getId(), ftUserIds, 0, 5 );
					//ajax load links

					userPageDataBean.fGameAboutDataBeans.add( tGADB );
				}
				if(userIds.size() > 1){
					userPageDataBean.recentActiveGamers = getTweetApi().getGamersByUserIds(ftUserIds, 0 , 5);
				}
			}
		}
        
		return SUCCESS;
	}
	
	@Autowired
	private IUserService userService;
}
