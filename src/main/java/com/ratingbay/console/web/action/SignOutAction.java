package com.ratingbay.console.web.action;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.http.HttpRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.ratingbay.cache.CacheCat;
import com.ratingbay.cache.CacheManager;
import com.ratingbay.console.common.RatingBayConst;


public class SignOutAction extends BaseAction implements SessionAware{
	
	public String signout() {
		
		ServletRequestAttributes attrs =  (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
	    HttpServletRequest request = attrs.getRequest() ;
	    HttpSession s = request.getSession();
	    s.removeAttribute(RatingBayConst.SESSION_KEY_USER);
		s.removeAttribute(RatingBayConst.SESSION_KEY_TWITTER);
		s.invalidate();
		HttpServletRequest req= ServletActionContext.getRequest();
		HttpServletResponse resp= ServletActionContext.getResponse();
		HttpSession session=req.getSession();
		session.removeAttribute(RatingBayConst.SESSION_KEY_USER);
		session.removeAttribute(RatingBayConst.SESSION_KEY_TWITTER);
		session.invalidate();
		
		Cookie[] cookies=ServletActionContext.getRequest().getCookies();
		for(Cookie c:cookies){
			if(RatingBayConst.COOKIE_ID.equals(c.getName())){
				c.setPath("/");
				c.setValue("0");
				c.setMaxAge(3600*24);
				resp.addCookie(c);
			}else{
				c.setPath("/");
				c.setValue("0");
				c.setMaxAge(0);
				resp.addCookie(c);
			}
		}
		SecurityContextHolder.clearContext();
		return SUCCESS;

	}
}
