package com.ratingbay.console.web.action;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.data.domain.Page;

import com.ratingbay.console.common.HTMLInputFilter;
import com.ratingbay.console.databean.SearchPageDataBean;
import com.ratingbay.console.dataface.IGameSummary;
import com.ratingbay.console.dataface.ITwitterUser;
import com.ratingbay.console.model.Tweet;
import com.ratingbay.console.web.widgets.PageWidget;

public class CategoryAction extends BaseAction implements SessionAware {
    private int pageNum = 0;
    public int getPageNum(){
        return this.pageNum;
    }
    public void setPageNum(int pageNum){
        this.pageNum = pageNum;
    }

    private String p;
    public String getP(){
        return this.p;
    }
    public void setP(String p){
        this.p =new HTMLInputFilter().filter(p);
    }

    private Long ge;
    public Long getGe(){
        return this.ge;
    }
    public void setGe(Long ge){
        this.ge = ge;
    }

    private String d;
    public String getD(){
        return this.d;
    }
    public void setD(String d){
        this.d = d;
    }

    private String f;
    public String getF(){
        return this.f;
    }
    public void setF(String f){
        this.f = f;
    }
    
    public SearchPageDataBean searchPageDataBean;
    public SearchPageDataBean getSearchPageDataBean(){
        return searchPageDataBean;
    }
    public void setSearchPageDataBean(SearchPageDataBean searchPageDataBean){
        this.searchPageDataBean = searchPageDataBean;
    }
	
	
	protected void loadGenreGame(){
		searchPageDataBean.kw=String.valueOf(ge);
        if( (null !=getF()) && (null!=getD()) ){
        	searchPageDataBean.gameList = getGameApi().getGamesByGenre(ge,f,d,pageNum, 5);
        }else{
        	searchPageDataBean.gameList = getGameApi().getGamesByGenre(ge,pageNum, 5);
        }
        searchPageDataBean.setKws(getGameApi().findGameGenreName(ge));
        
        
        
        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
        if(tUserIds.size() > 1){
            Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            Page<Tweet> tweetsOfPage;
            Page<Tweet> tweetsOfPageForUserId;
            while(it.hasNext()){
                gameSummary = it.next();
                gameSummary.setGenre(getGameApi().getGenreById(gameSummary.getId()));
                gameSummary.setPlatform(getGameApi().getPlatformById(gameSummary.getId()));
                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                    tUserIds, 0, 5);
                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                        tUserIds, 0, 100);
                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
            	Set<Long> uids=new HashSet<Long>();
                for(Tweet t:tweetsOfPageForUserId.getContent()){
                	if(uids.size() < 5){
                	uids.add(t.getUserId());
                	}
                }
                for(Long id:uids){
                	ITwitterUser u=getTUserApi().getITwitterUser(id);
                	ll.add(u);
                }
                gameSummary.setTwitterUsers(ll);
            }  
        }else{
        	Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            while(it.hasNext()){
                gameSummary = it.next();
                gameSummary.setGenre(getGameApi().getGenreById(gameSummary.getId()));
                gameSummary.setPlatform(getGameApi().getPlatformById(gameSummary.getId()));
            }
        }
        Integer totalPages = searchPageDataBean.gameList.getTotalPages();
        searchPageDataBean.pageWidget = new PageWidget(totalPages, pageNum, getLinkPrefix());
    }
    
    
    protected void loadPlatformGame(){
        if( (null !=getF()) && (null!=getD()) ){
        	searchPageDataBean.gameList = getGameApi().getGamesByPlatform(p, f,d,pageNum, 5);
        }else{
        	searchPageDataBean.gameList = getGameApi().getGamesByPlatform(p,pageNum, 5);
        }
        searchPageDataBean.setKw(p);
        searchPageDataBean.setKws(p);
        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
        if(tUserIds.size() > 1){
            Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            Page<Tweet> tweetsOfPage;
            Page<Tweet> tweetsOfPageForUserId;
            while(it.hasNext()){
                gameSummary = it.next();
                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                    tUserIds, 0, 5);
                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                        tUserIds, 0, 100);
                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
            	Set<Long> uids=new HashSet<Long>();
                for(Tweet t:tweetsOfPageForUserId.getContent()){
                	if(uids.size() < 5){
                	uids.add(t.getUserId());
                	}
                }
                for(Long id:uids){
                	ITwitterUser u=getTUserApi().getITwitterUser(id);
                	ll.add(u);
                }
                gameSummary.setTwitterUsers(ll);
            }  
        }else{
        	Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            while(it.hasNext()){
                gameSummary = it.next();
                gameSummary.setGenre(getGameApi().getGenreById(gameSummary.getId()));
                gameSummary.setPlatform(getGameApi().getPlatformById(gameSummary.getId()));
            }
        }
      
        Integer totalPages = searchPageDataBean.gameList.getTotalPages();
        searchPageDataBean.pageWidget = new PageWidget(totalPages, pageNum, getLinkPrefix());
    }   

    protected String getLinkPrefix(){
        String linkPrefix = "";

        if(null != this.p) {
            linkPrefix += "p=" + this.getP();
        }else{
            linkPrefix += "ge=" + this.getGe();
        }
        
        if( null != this.getF() )linkPrefix += "&f=" + this.getF();
        if( null != this.getD() )linkPrefix += "&d=" + this.getD();

        return linkPrefix;
    }

    //load search page
    public String load() {        
        String retVal = "genre";
        if( (null == this.p)  && (null == this.ge) )return "false";

        searchPageDataBean = new SearchPageDataBean();
        if(null != this.p) {
            retVal = "platform";
            this.loadPlatformGame();
        }else{
            this.loadGenreGame();
        }
        return retVal;
    }
    
}
