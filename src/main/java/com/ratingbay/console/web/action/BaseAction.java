/**
 * This is a part of the CARIS Spatial Fusion Viewer.
 * Copyright (c) 2011 CARIS.  All rights reserved.
 * 
 * This Software is to be used "as is" solely for the authorised
 * Licensee's own development as defined in the Spatial Fusion Viewer 
 * license agreement. CARIS makes no representations about the 
 * suitability of this Software for any purpose.  In no event will CARIS 
 * be liable for special, incidental or consequential losses or damages, 
 * including damages for loss of use and/or loss of profit, direct or 
 * indirect damages of any kind related to the use of this Software.
 */
package com.ratingbay.console.web.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import com.opensymphony.xwork2.ActionSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.twitter.api.CursoredList;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;

import com.ratingbay.cache.*;
import com.ratingbay.console.api.*;
import com.ratingbay.console.common.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.service.*;

/**
 * All of actions should extend this class.
 * @author lwang
 */
public abstract class BaseAction extends ActionSupport implements SessionAware, ServletRequestAware{
    protected Logger logger = LogManager.getLogger(this.getClass());
    protected CacheManager cacheManager = CacheManager.getInstance();

    //used in class inner
    private ApiComponent apiComponent = null;
    private ApiComponent getApiComponent(){
        if(null == apiComponent){
            apiComponent = ApiComponent.getInstance();
        }
        return apiComponent;
    }

    public HttpServletRequest request;
    
    public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request=request;
	}
	//public api
    private TwitterUserApi tUserApi = null;
    public TwitterUserApi getTUserApi(){
        if(null == tUserApi){
            tUserApi = TwitterUserApi.getInstance( ServiceComponent.getInstance().getTwitterUserService(), 
            new Structs2SessionImpl( getSession() ) );
        }
        return tUserApi;
    }
    public TweetApi getTweetApi(){
        return getApiComponent().getTweetApi();
    }
    public GameApi getGameApi(){
        return getApiComponent().getGameApi();
    }
    public LinkApi getLinkApi(){
        return getApiComponent().getLinkApi();
    }
    public WordApi getWordApi(){
        return getApiComponent().getWordApi();
    }
    public InfluentialUserApi getIUserApi(){
        return getApiComponent().getIUserApi();
    }


    // base methods
    public Map session;
    public Map getSession() {
        return this.session;
    }
    public void setSession(Map<String,Object> session) {
        this.session = session;
    }

    private boolean isLogged = false;
    public boolean getIsLogged(){
        if(null != getSession().get("userId")){
            return true;
        }
        return false;
    }
    
    public ITwitterUser getITwitterUser(){
        return getTUserApi().getITwitterUser();
    }
}
