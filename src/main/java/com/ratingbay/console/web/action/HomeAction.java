package com.ratingbay.console.web.action;

import java.util.*;

import org.apache.struts2.interceptor.SessionAware;

import com.ratingbay.console.api.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.service.*;

/**
 * @author zhuxun
 * @version 1.0
 * @date 下午2:05:31 2014年8月1日
 */
public class HomeAction extends BaseAction implements SessionAware {

	//output in view
	HomePageDataBean homePageDataBean;
	public HomePageDataBean getHomePageDataBean(){
		return homePageDataBean;
	}
	public void setHomePageDataBean(HomePageDataBean homePageDataBean){
		this.homePageDataBean = homePageDataBean;
	}


	//load home page
	public String load() {
		Date beforeDate = new Date( System.currentTimeMillis() - 3*5184000000L );
		Date startDate = new Date( System.currentTimeMillis() -5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		Date afterDate = new Date( System.currentTimeMillis() + 6*5184000000L );

		homePageDataBean = new HomePageDataBean();
		homePageDataBean.newReleasedGames = new ArrayList<IGameSummary>();
		homePageDataBean.comingSoonGames = new ArrayList<IGameSummary>();
		homePageDataBean.topRatedGames = null; //ajax load top10
		
		List<IGameSummary> tweetedGames = null;
	
		//按照tweet数量从大到小的顺序获取game
		tweetedGames = getGameApi().getTopTweetedGames(beforeDate, afterDate, 0, 50);
		if(null != tweetedGames){
			int a=0;int b =0;
			for(int i=0; i<tweetedGames.size(); i++){				
				if(tweetedGames.get(i).getReleaseDate().before(curDate)){
					
					if(a <20){
					homePageDataBean.newReleasedGames.add(tweetedGames.get(i));
					a++;
					}
				}else{
					
					if(b<12){
					homePageDataBean.comingSoonGames.add(tweetedGames.get(i));
					b++;}
				}
			}
		}
		//按照评分从高到低获取10条game
		homePageDataBean.topRatedGames = getGameApi().getTopRatedGames(beforeDate, afterDate, 0, 10);
		//获取首页轮换大图
 		homePageDataBean.headlineGames = getGameApi().getHeadlineGames(beforeDate, curDate, 0, 3);
 		//获取最近Tweet
 		homePageDataBean.recentTweets = getTweetApi().getRecentTweets(startDate,curDate);
 		//获取最流行Tweet
		homePageDataBean.popularTweets = getTweetApi().getPopularTweets(beforeDate, curDate);
		//获取最流行视频
		homePageDataBean.popularVideoLinks = getLinkApi().getPopularVideoLinks(beforeDate, curDate, 0, 10);
		//获取影响力最高的用户排名
		homePageDataBean.topInfluencialUsers = getIUserApi().getTopInfluentialUser(0,10);
		return SUCCESS;
	}
}
