//search action
package com.ratingbay.console.web.action;

import java.util.*;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.SessionAware;
import org.springframework.data.domain.Page;

import com.ratingbay.console.api.*;
import com.ratingbay.console.common.HTMLInputFilter;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;
import com.ratingbay.console.web.widgets.*;

public class SearchAction extends BaseAction implements SessionAware {

    //input from request
    private int pageNum = 0;
    public int getPageNum(){
        return this.pageNum;
    }
    public void setPageNum(int pageNum){
        this.pageNum = pageNum;
    }

    private String u;
    public String getU(){
        return this.u;
    }
    public void setU(String u){
        this.u = new HTMLInputFilter().filter(u);
    }

    private String g;
    public String getG(){
        return this.g;
    }
    public void setG(String g){
    	this.g = new HTMLInputFilter().filter(g);
    }

    private String d;
    public String getD(){
        return this.d;
    }
    public void setD(String d){
        this.d = d;
    }

    private String f;
    public String getF(){
        return this.f;
    }
    public void setF(String f){
        this.f = f;
    }

    //output in view
    public SearchPageDataBean searchPageDataBean;
    public SearchPageDataBean getSearchPageDataBean(){
        return searchPageDataBean;
    }
    public void setSearchPageDataBean(SearchPageDataBean searchPageDataBean){
        this.searchPageDataBean = searchPageDataBean;
    }
    

    //used in class inner
    protected void loadSearchUser(){
        searchPageDataBean.kw = u;
        if( (null !=getF()) && (null!=getD()) ){
            searchPageDataBean.userList = getTUserApi().getUsersByName(u, getF(), getD(), pageNum, 5);
        }else{
            searchPageDataBean.userList = getTUserApi().getUsersByName(u, pageNum, 5);
        }

        Integer totalPages = searchPageDataBean.userList.getTotalPages();
        searchPageDataBean.pageWidget = new PageWidget(totalPages, pageNum, getLinkPrefix());
        getRequest().setAttribute("u", u);
    }

    protected void loadSearchGame(){
        searchPageDataBean.kw = g;
        if( (null !=getF()) && (null!=getD()) ){
           searchPageDataBean.gameList = getGameApi().getGamesByTitle(g, getF(), getD(), pageNum, 5); 
        }else{
           searchPageDataBean.gameList = getGameApi().getGamesByTitle(g, pageNum, 5);
        }
        
        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
        if(tUserIds.size() > 1){
            Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            Page<Tweet> tweetsOfPage;
            Page<Tweet> tweetsOfPageForUserId;
            while(it.hasNext()){
                gameSummary = it.next();
                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                    tUserIds, 0, 5);
                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                        tUserIds, 0, 100);
                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
            	Set<Long> uids=new HashSet<Long>();
                for(Tweet t:tweetsOfPageForUserId.getContent()){
                	if(uids.size() < 5){
                	uids.add(t.getUserId());
                	}
                }
                for(Long id:uids){
                	ITwitterUser u=getTUserApi().getITwitterUser(id);
                	ll.add(u);
                }
                gameSummary.setTwitterUsers(ll);
            }  
        }

        Integer totalPages = searchPageDataBean.gameList.getTotalPages();
        searchPageDataBean.pageWidget = new PageWidget(totalPages, pageNum, getLinkPrefix());
        getRequest().setAttribute("g", g);
    }

    protected String getLinkPrefix(){
        String linkPrefix = "";

        if(null != this.u) {
            linkPrefix += "u=" + this.getU();
        }else{
            linkPrefix += "g=" + this.getG();
        }
        
        if( null != this.getF() )linkPrefix += "&f=" + this.getF();
        if( null != this.getD() )linkPrefix += "&d=" + this.getD();

        return linkPrefix;
    }

    //load search page
    public String load() {        
        String retVal = "game";
        searchPageDataBean = new SearchPageDataBean();
        if( (null == this.u)  && (null == this.g) ){
        	g=" ";
        	this.loadSearchGame();
        }	
        
        if(null != this.u) {
            retVal = "user";
            this.loadSearchUser();
        }else{
            this.loadSearchGame();
        }
        return retVal;
    }

}
