//AllGames action
package com.ratingbay.console.web.action;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.struts2.interceptor.SessionAware;
import org.springframework.data.domain.Page;

import com.ratingbay.console.databean.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.web.widgets.*;

public class AllGameAction extends BaseAction implements SessionAware {

    //input from request
    private int pageNum = 0;
    public int getPageNum(){
        return this.pageNum;
    }
    public void setPageNum(int pageNum){
        this.pageNum = pageNum;
    }

    private String ag;
    public String getAg(){
        return this.ag;
    }
    public void setAg(String ag){
        this.ag = ag;
    }

    private String d;
    public String getD(){
        return this.d;
    }
    public void setD(String d){
        this.d = d;
    }

    private String f;
    public String getF(){
        return this.f;
    }
    public void setF(String f){
        this.f = f;
    }

    //output in view
    public SearchPageDataBean searchPageDataBean;
    public SearchPageDataBean getSearchPageDataBean(){
        return searchPageDataBean;
    }
    public void setSearchPageDataBean(SearchPageDataBean searchPageDataBean){
        this.searchPageDataBean = searchPageDataBean;
    }
    

    protected void loadAllGame(String ag){
        Date curDate = new Date( System.currentTimeMillis() );
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        ParsePosition pos = new ParsePosition(0);
		Date now = formatter.parse(formatter.format(curDate), pos);
        if(ag=="released"){
        	if((null !=getF()) && (null!=getD()) ){
        		searchPageDataBean.gameList = getGameApi().getReleasedGames(now, getF(), getD(), pageNum, 5); 
        	}else{
        		//System.out.println("load released ag is " + ag);
        		searchPageDataBean.gameList = getGameApi().getReleasedGames(now, pageNum, 5);
        	}
        }else{
        	if((null !=getF()) && (null!=getD()) ){
        		searchPageDataBean.gameList = getGameApi().getComingGames(now, getF(), getD(), pageNum, 5); 
        	}else{
        		//System.out.println("load coming ag is " + ag);
        		searchPageDataBean.gameList = getGameApi().getComingGames(now, pageNum, 5);
        	}
        }
        
        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
        if(tUserIds.size() > 1){
            Iterator<IGameSummary> it = searchPageDataBean.gameList.iterator();
            IGameSummary gameSummary;
            Page<Tweet> tweetsOfPage;
            Page<Tweet> tweetsOfPageForUserId;
            while(it.hasNext()){
                gameSummary = it.next();
                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                    tUserIds, 0, 5);
                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
                        tUserIds, 0, 100);
                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
            	Set<Long> uids=new HashSet<Long>();
                for(Tweet t:tweetsOfPageForUserId.getContent()){
                	if(uids.size() < 5){
                	uids.add(t.getUserId());
                	}
                }
                for(Long id:uids){
                	ITwitterUser u=getTUserApi().getITwitterUser(id);
                	ll.add(u);
                }
                gameSummary.setTwitterUsers(ll);
            }  
        }

        Integer totalPages = searchPageDataBean.gameList.getTotalPages();
        searchPageDataBean.pageWidget = new PageWidget(totalPages, pageNum, getLinkPrefix());
    }

    protected String getLinkPrefix(){
        String linkPrefix = "";

        if(null!= this.ag)linkPrefix += "ag=" + this.getAg();             
        if( null != this.getF() )linkPrefix += "&f=" + this.getF();
        if( null != this.getD() )linkPrefix += "&d=" + this.getD();

        return linkPrefix;
    }

    //load search page
    public String load() {        
        String retVal = "coming";
        searchPageDataBean = new SearchPageDataBean();
        if(this.ag.equals(retVal)){
        	//System.out.println("coming ag is " + this.ag);
        	this.loadAllGame(ag);
        }else{
        	//System.out.println("released ag is " + this.ag);
        	retVal="released";
        	this.ag=retVal;
        	this.loadAllGame(ag);
        }
        return retVal;
    }
}
