package com.ratingbay.console.web.action;

import java.util.*;

import org.apache.struts2.interceptor.SessionAware;

import com.ratingbay.console.api.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.model.GameGenre;
import com.ratingbay.console.model.GamePlatform;
import com.ratingbay.console.service.*;
import com.ratingbay.console.utils.*;

public class GameAction extends BaseAction implements SessionAware {

	//input from page
	private Long gameId;
	public Long getGameId(){
		return this.gameId;
	}
	public void setGameId(Long gameId){
		this.gameId = gameId;
	}
	
	private String videoId;
	public String getVideoId(){
		return this.videoId;
	}
	public void setVideoId(String videoId){
		this.videoId = videoId;
	}

	//output to page
	private GamePageDataBean gamePageDataBean;
	public GamePageDataBean getGamePageDataBean(){
		return gamePageDataBean;
	}
	public void setGamePageDataBean(GamePageDataBean gamePageDataBean){
		this.gamePageDataBean = gamePageDataBean;
	}


    //load game page
	public String load() {
        if(null == gameId||getGameApi().findGameEnable(gameId).size()<1) return "false";
        gamePageDataBean = new GamePageDataBean();
        gamePageDataBean.gameDetail = getGameApi().getGameDetail(gameId);
        System.out.println("game id is "+gameId);
        
        if(null == gamePageDataBean.gameDetail)return SUCCESS;
        gamePageDataBean.gameDetail.setTweetCount( getTweetApi().getTweetCountByGame(gameId) );
        if(null != videoId){
        	gamePageDataBean.gameDetail.setVideoId(videoId + "?autoplay=1");
		}
        
		gamePageDataBean.gameDetail.setHashTags(getWordApi().getHashtagsOfSet(gameId, 0, 10));
		
		gamePageDataBean.topPlayers = getIUserApi().getTopPlayersByGame(gameId, 0, 10);
		return SUCCESS;
	}
}
