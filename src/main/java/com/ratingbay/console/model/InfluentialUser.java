package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the game database table.
 * 
 */
@Entity
@Table(name="influential_user")
public class InfluentialUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="INFLUENTIAL_USER_ID_GENERATOR", sequenceName="INFLUENTIAL_USER_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="INFLUENTIAL_USER_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="twitter_user_id", nullable=false)
	private Long twitterUserId;
	public Long getTwitterUserId() {
		return this.twitterUserId;
	}
	public void setTwitterUserId(Long twitterUserId) {
		this.twitterUserId = twitterUserId;
	}

	@Column(name="username")
	private String username;
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name="user_followers_count")
	private Long userFollowersCount;
	public Long getUserFollowersCount() {
		return this.userFollowersCount;
	}
	public void setUserFollowersCount(Long userFollowersCount) {
		this.userFollowersCount = userFollowersCount;
	}

	@Column(name="user_profile_img_url")
	private String userProfileImgUrl;
	public String getUserProfileImgUrl() {
		return this.userProfileImgUrl;
	}
	public void setUserProfileImgUrl(String userProfileImgUrl) {
		this.userProfileImgUrl = userProfileImgUrl;
	}

	@Column(name="user_location")
	private String userLocation;
	public String getUserLocation() {
		return this.userLocation;
	}
	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}

	@Column(name="user_tweets_count")
	private Long userTweetsCount;
	public Long getUserTweetsCount() {
		return this.userTweetsCount;
	}
	public void setUserTweetsCount(Long userTweetsCount) {
		this.userTweetsCount = userTweetsCount;
	}

	@Column(name="user_friends_count")
	private Long userFriendsCount;
	public Long getUserFriendsCount() {
		return this.userFriendsCount;
	}
	public void setUserFriendsCount(Long userFriendsCount) {
		this.userFriendsCount = userFriendsCount;
	}

	@Column(name="user_lang")
	private String userLang;
	public String getUserLang() {
		return this.userLang;
	}
	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	@Column(name="big_user_profile_img_url")
	private String bigUserProfileImgUrl;
	public String getBigUserProfileImgUrl() {
		return this.bigUserProfileImgUrl;
	}
	public void setBigUserProfileImgUrl(String bigUserProfileImgUrl) {
		this.bigUserProfileImgUrl = bigUserProfileImgUrl;
	}

	@Column(name="mini_user_profile_img_url")
	private String miniUserProfileImgUrl;
	public String getMiniUserProfileImgUrl() {
		return this.miniUserProfileImgUrl;
	}
	public void setMiniUserProfileImgUrl(String miniUserProfileImgUrl) {
		this.miniUserProfileImgUrl = miniUserProfileImgUrl;
	}

	@Column(name="user_description")
	private String userDescription;
	public String getUserDescription() {
		return this.userDescription;
	}
	public void setUserDescription(String userDescription) {
		this.userDescription = userDescription;
	}

	@Column(name="user_description_urls")
	private String userDescriptionUrls;
	public String getUserDescriptionUrls() {
		return this.userDescriptionUrls;
	}
	public void setUserDescriptionUrls(String userDescriptionUrls) {
		this.userDescriptionUrls = userDescriptionUrls;
	}

	@Column(name="is_user_protected")
	private boolean isUserProtected;
	public boolean getIsUserProtected() {
		return this.isUserProtected;
	}
	public void setIsUserProtected(boolean isUserProtected) {
		this.isUserProtected = isUserProtected;
	}

	@Column(name="id_game", nullable=false)
	private Long idGame;
	public Long getIdGame() {
		return this.idGame;
	}
	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="insert_time")
	private Date insertTime;
	public Date getInsertTime() {
		return this.insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="start_time")
	private Date startTime;
	public Date getStartTime() {
		return this.startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Temporal( TemporalType.DATE)
	@Column(name="end_time")
	private Date endTime;
	public Date getEndTime() {
		return this.endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column(name="h_index")
	private Long hIndex;
	public Long getHIndex() {
		return this.hIndex;
	}
	public void setHIndex(Long hIndex) {
		this.hIndex = hIndex;
	}

	@Column(name="max_retweet_count")
	private Long maxRetweetCount;
	public Long getMaxRetweetCount() {
		return this.maxRetweetCount;
	}
	public void setMaxRetweetCount(Long maxRetweetCount) {
		this.maxRetweetCount = maxRetweetCount;
	}
}