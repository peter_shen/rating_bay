package com.ratingbay.console.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="rb_game_tweet_trend")
public class RbGameTweetTrend implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="RB_GAME_ID_GENERATOR", sequenceName="RB_GAME_TWEET_TREND_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GAME_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name="id_game")
	private Long idGame;

	@Column(name="tweet_count")
	private Long tweetCount;
	
	@Column(name="create_date")
	private Date createDate;
	
	public Long getIdGame() {
		return idGame;
	}

	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

	public Long getTweetCount() {
		return tweetCount;
	}

	public void setTweetCount(Long tweetCount) {
		this.tweetCount = tweetCount;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	
}
