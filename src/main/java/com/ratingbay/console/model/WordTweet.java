package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the word_tweet database table.
 * 
 */
@Entity
@Table(name="word_tweet")
public class WordTweet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="WORD_TWEET_ID_GENERATOR", sequenceName="WORD_TWEET_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="WORD_TWEET_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame() {
		return this.idGame;
	}
	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

	@Column(name="is_hashtag")
	private Boolean isHashtag;
	public Boolean getIsHashtag() {
		return this.isHashtag;
	}
	public void setIsHashtag(Boolean isHashtag) {
		this.isHashtag = isHashtag;
	}

	@Column(length=2147483647)
	private String text;
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Column(name="tweet_count")
	private Long tweetCount;
	public Long getTweetCount() {
		return this.tweetCount;
	}
	public void setTweetCount(Long tweetCount) {
		this.tweetCount = tweetCount;
	}
}