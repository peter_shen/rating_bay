package com.ratingbay.console.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.*;

/**
 * The persistent class for the game database table.
 * 
 */
@Entity
@Table(name="game")
public class Game implements Serializable {
	@Id
	@SequenceGenerator(name="GAME_ID_GENERATOR", sequenceName="GAME_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GAME_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

    @Temporal( TemporalType.DATE)
	@Column(name="active_date")
	private Date activeDate;
	public Date getActiveDate() {
		return this.activeDate;
	}
	public void setActiveDate(Date activeDate) {
		this.activeDate = activeDate;
	}

	@Column(length=2147483647)
	private String description;
	public String getDescription() {
		return this.description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name="dvd_image_path", length=2147483647)
	private String dvdImagePath;
	public String getDvdImagePath() {
		return this.dvdImagePath;
	}
	public void setDvdImagePath(String dvdImagePath) {
		this.dvdImagePath = dvdImagePath;
	}

	@Column(name="gamesdb_id")
	private Long gamesdbId;
	public Long getGamesdbId() {
		return this.gamesdbId;
	}
	public void setGamesdbId(Long gamesdbId) {
		this.gamesdbId = gamesdbId;
	}

	@Column(name="headline_image_path", length=2147483647)
	private String headlineImagePath;
	public String getHeadlineImagePath() {
		return this.headlineImagePath;
	}
	public void setHeadlineImagePath(String headlineImagePath) {
		this.headlineImagePath = headlineImagePath;
	}

    @Temporal( TemporalType.DATE)
	@Column(name="inactive_date")
	private Date inactiveDate;
	public Date getInactiveDate() {
		return this.inactiveDate;
	}
	public void setInactiveDate(Date inactiveDate) {
		this.inactiveDate = inactiveDate;
	}

	@Column(name="is_headline", nullable=false)
	private Boolean isHeadline;
	public Boolean getIsHeadline() {
		return this.isHeadline;
	}
	public void setIsHeadline(Boolean isHeadline) {
		this.isHeadline = isHeadline;
	}

	@Column(length=2147483647)
	private String publisher;
	public String getPublisher() {
		return this.publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}

    @Temporal( TemporalType.DATE)
	@Column(name="release_date")
	private Date releaseDate;
	public Date getReleaseDate() {
		return this.releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	@Column(nullable=false)
	private Integer status;
	public Integer getStatus() {
		return this.status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	
	@Column(name="score", precision=4, scale=2)
	private Double score;
	public Double getScore(){
		return score;
	}
	public void setScore(Double score){
		this.score = score;
	}
	
	@Column(length=2147483647)
	private String title;
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="video_trailer_link", length=2147483647)
	private String videoTrailerLink;
	public String getVideoTrailerLink() {
		return this.videoTrailerLink;
	}
	public void setVideoTrailerLink(String videoTrailerLink) {
		this.videoTrailerLink = videoTrailerLink;
	}

	//bi-directional many-to-one association to GameGenre
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<GameGenre> gameGenres;
	public Set<GameGenre> getGameGenres() {
		return this.gameGenres;
	}
	public void setGameGenres(Set<GameGenre> gameGenres) {
		this.gameGenres = gameGenres;
	}

	//bi-directional many-to-one association to GameName
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<GameName> gameNames;
	public Set<GameName> getGameNames() {
		return this.gameNames;
	}
	public void setGameNames(Set<GameName> gameNames) {
		this.gameNames = gameNames;
	}

	//bi-directional many-to-one association to GamePlatform
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<GamePlatform> gamePlatforms;
	public Set<GamePlatform> getGamePlatforms() {
		return this.gamePlatforms;
	}
	public void setGamePlatforms(Set<GamePlatform> gamePlatforms) {
		this.gamePlatforms = gamePlatforms;
	}

	//bi-directional many-to-one association to Link
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<Link> links;
	public Set<Link> getLinks() {
		return this.links;
	}
	public void setLinks(Set<Link> links) {
		this.links = links;
	}

	//bi-directional many-to-one association to SentimentScoreHistory
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<SentimentScoreHistory> sentimentScoreHistories;
	public Set<SentimentScoreHistory> getSentimentScoreHistories() {
		return this.sentimentScoreHistories;
	}
	public void setSentimentScoreHistories(Set<SentimentScoreHistory> sentimentScoreHistories) {
		this.sentimentScoreHistories = sentimentScoreHistories;
	}

	//bi-directional many-to-one association to TrackingKeyword
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<TrackingKeyword> trackingKeywords;
	public Set<TrackingKeyword> getTrackingKeywords() {
		return this.trackingKeywords;
	}
	public void setTrackingKeywords(Set<TrackingKeyword> trackingKeywords) {
		this.trackingKeywords = trackingKeywords;
	}

	//bi-directional many-to-one association to Tweet
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<Tweet> tweets;
	public Set<Tweet> getTweets() {
		return this.tweets;
	}
	public void setTweets(Set<Tweet> tweets) {
		this.tweets = tweets;
	}

	//bi-directional many-to-one association to Word
	@OneToMany(mappedBy="game", cascade={CascadeType.REMOVE})
	private Set<Word> words;
	public Set<Word> getWords() {
		return this.words;
	}
	public void setWords(Set<Word> words) {
		this.words = words;
	}
}