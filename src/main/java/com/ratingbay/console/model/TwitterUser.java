package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.Set;


/**
 * The persistent class for the twitter_user database table.
 * 
 */
@Entity
@Table(name="twitter_user")
public class TwitterUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TWITTER_USER_ID_GENERATOR", sequenceName="TWITTER_USER_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TWITTER_USER_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}


	@Column(name="user_followers_count")
	private Long userFollowersCount;
	public Long getUserFollowersCount() {
		return this.userFollowersCount;
	}
	public void setUserFollowersCount(Long userFollowersCount) {
		this.userFollowersCount = userFollowersCount;
	}

	@Column(name="user_friends_count")
	private Long userFriendsCount;
	public Long getUserFriendsCount() {
		return this.userFriendsCount;
	}
	public void setUserFriendsCount(Long userFriendsCount) {
		this.userFriendsCount = userFriendsCount;
	}

	@Column(name="user_id", nullable=false)
	private Long userId;
	public Long getUserId() {
		return this.userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name="user_lang", length=2147483647)
	private String userLang;
	public String getUserLang() {
		return this.userLang;
	}
	public void setUserLang(String userLang) {
		this.userLang = userLang;
	}

	@Column(name="user_location", length=2147483647)
	private String userLocation;
	public String getUserLocation() {
		return this.userLocation;
	}
	public void setUserLocation(String userLocation) {
		this.userLocation = userLocation;
	}
	
	@Column(name="user_profile_img_url", length=2147483647)
	private String userProfileImgUrl;
	public String getUserProfileImgUrl() {
		return this.userProfileImgUrl;
	}
	public void setUserProfileImgUrl(String userProfileImgUrl) {
		this.userProfileImgUrl = userProfileImgUrl;
	}
	
	@Column(name="mini_user_profile_img_url", length=2147483647)
	private String miniUserProfileImgUrl;
	public String getMiniUserProfileImgUrl() {
		return this.miniUserProfileImgUrl;
	}
	public void setMiniUserProfileImgUrl(String miniUserProfileImgUrl) {
		this.miniUserProfileImgUrl = miniUserProfileImgUrl;
	}
	
	@Column(name="big_user_profile_img_url", length=2147483647)
	private String bigUserProfileImgUrl;
	public String getBigUserProfileImgUrl() {
		return this.bigUserProfileImgUrl;
	}
	public void setBigUserProfileImgUrl(String bigUserProfileImgUrl) {
		this.bigUserProfileImgUrl = bigUserProfileImgUrl;
	}
	
	
	@Column(name="user_tweets_count")
	private Long userTweetsCount;
	public Long getUserTweetsCount() {
		return this.userTweetsCount;
	}
	public void setUserTweetsCount(Long userTweetsCount) {
		this.userTweetsCount = userTweetsCount;
	}

	@Column(length=2147483647)
	private String username;
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}


	//bi-directional many-to-one association to MentionedUser
	@OneToMany(mappedBy="twitterUser", cascade={CascadeType.REMOVE})
	private Set<MentionedUser> mentionedUsers;
	public Set<MentionedUser> getMentionedUsers() {
		return this.mentionedUsers;
	}
	public void setMentionedUsers(Set<MentionedUser> mentionedUsers) {
		this.mentionedUsers = mentionedUsers;
	}

	@Column(name="is_user_protected", nullable=false)
	private Boolean isUserProtected;
	
	public Boolean getIsUserProtected() {
		return isUserProtected;
	}
	public void setIsUserProtected(Boolean isUserProtected) {
		this.isUserProtected = isUserProtected;
	}


	@Column(name="user_description_urls", length=2147483647)
	private String userDescriptionUrls;
	public String getUserDescriptionUrls(){
		return userDescriptionUrls;
	}
	public void setUserDescriptionUrls(String userDescriptionUrls){
		this.userDescriptionUrls = userDescriptionUrls;
	}
	
	
	@Column(name="last_update_time", nullable=false)
	private Timestamp lastUpdateTime;
	public Timestamp getLastUpdateTime() {
		return this.lastUpdateTime;
	}
	public void setLastUpdateTime(Timestamp lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	
	
	@Column(name="user_description", length=2147483647)
	private String userDescription;
	public String getUserDescription(){
		return userDescription;
	}
	public void setUserDescription(String userDescription){
		this.userDescription = userDescription;
	}
	
	//bi-directional many-to-one association to Tweet
	@OneToMany(mappedBy="twitterUser", cascade={CascadeType.REMOVE})
	private Set<Tweet> tweets;
	public Set<Tweet> getTweets() {
		return this.tweets;
	}
	public void setTweets(Set<Tweet> tweets) {
		this.tweets = tweets;
	}
	
}