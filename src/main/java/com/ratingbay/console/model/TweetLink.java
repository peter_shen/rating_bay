package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tweet_link database table.
 * 
 */
@Entity
@Table(name="tweet_link")
public class TweetLink implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TWEET_LINK_ID_GENERATOR", sequenceName="TWEET_LINK_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TWEET_LINK_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame() {
		return this.idGame;
	}
	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

    @Column(name="id_tweet")
	private Long idTweet;
	public Long getIdTweet() {
		return this.idTweet;
	}
	public void setIdTweet(Long idTweet) {
		this.idTweet = idTweet;
	}

	@Column(name="id_link")
	private Long idLink;
	public Long getIdLink(){
		return this.idLink;
	}
	public void setIdLink(Long idLink){
		this.idLink = idLink;
	}

	//bi-directional many-to-one association to Link
	@ManyToOne
	@JoinColumn(name="id_link")
	private Link link;
	public Link getLink() {
	        return this.link;
	}
	public void setLink(Link link) {
	        this.link = link;
	}

	//bi-directional many-to-one association to Tweet
    @ManyToOne
    @JoinColumn(name="id_tweet")
    private Tweet tweet;
    public Tweet getTweet() {
        return this.tweet;
    }
    public void setTweet(Tweet tweet) {
        this.tweet = tweet;
    }
}