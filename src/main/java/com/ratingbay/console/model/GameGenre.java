package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the game_genre database table.
 * 
 */
@Entity
@Table(name="game_genre")
public class GameGenre implements Serializable {

	@Id
	@SequenceGenerator(name="GAME_GENRE_ID_GENERATOR", sequenceName="GAME_GENRE_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GAME_GENRE_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}

	//bi-directional many-to-one association to Genre
    @ManyToOne
	@JoinColumn(name="id_genre")
	private Genre genre;
	public Genre getGenre() {
		return this.genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
}