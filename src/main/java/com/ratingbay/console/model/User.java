package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name="users")
public class User implements Serializable, UserDetails {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USER_ID_GENERATOR", sequenceName="USER_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USER_ID_GENERATOR")
	@Column(unique=true, nullable=false, length=255)
	private String id;
	public String getId() {
		return this.id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@Column(length=2147483647)
	private String email;
	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="last_login_duration")
	private Timestamp lastLoginDuration;
	public Timestamp getLastLoginDuration() {
		return this.lastLoginDuration;
	}
	public void setLastLoginDuration(Timestamp lastLoginDuration) {
		this.lastLoginDuration = lastLoginDuration;
	}

	@Column(name="last_login_time")
	private Timestamp lastLoginTime;
	public Timestamp getLastLoginTime() {
		return this.lastLoginTime;
	}
	public void setLastLoginTime(Timestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	@Column(length=2147483647)
	private String password;
	public String getPassword() {
		return this.password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

    @Temporal( TemporalType.DATE)
	@Column(name="signup_time", nullable=false)
	private Date signupTime;
	public Date getSignupTime() {
		return this.signupTime;
	}
	public void setSignupTime(Date signupTime) {
		this.signupTime = signupTime;
	}

	@Column(length=2147483647)
	private String username;
	public String getUsername() {
		return this.username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	//bi-directional many-to-one association to UserConnection
	@OneToMany(mappedBy="user")
	private Set<UserConnection> userconnections;
	public Set<UserConnection> getUserconnections() {
		return this.userconnections;
	}
	public void setUserconnections(Set<UserConnection> userconnections) {
		this.userconnections = userconnections;
	}

	static final List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();
	static {
	   AUTHORITIES.add(new SimpleGrantedAuthority("ROLE_USER"));
	}
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.AUTHORITIES;
	}

	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
}