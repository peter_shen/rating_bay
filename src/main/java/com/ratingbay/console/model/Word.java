package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the word database table.
 * 
 */
@Entity
@Table(name="word")
public class Word implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="WORD_ID_GENERATOR", sequenceName="WORD_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="WORD_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="is_hashtag", nullable=false)
	private Boolean isHashtag;
	public Boolean getIsHashtag() {
		return this.isHashtag;
	}
	public void setIsHashtag(Boolean isHashtag) {
		this.isHashtag = isHashtag;
	}

	@Column(nullable=false, length=2147483647)
	private String text;
	public String getText() {
		return this.text;
	}
	public void setText(String text) {
		this.text = text;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame(){
		return this.idGame;
	}
	public void setIdGame(Long idGame){
		this.idGame = idGame;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
}