package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the mentioned_user database table.
 * 
 */
@Entity
@Table(name="mentioned_user")
public class MentionedUser implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="MENTIONED_USER_ID_GENERATOR", sequenceName="MENTIONED_USER_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="MENTIONED_USER_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="user_id", nullable=false)
	private Long userId;
	public Long getUserId() {
		return this.userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	//bi-directional many-to-one association to Tweet
    @ManyToOne
	@JoinColumn(name="id_tweet")
	private Tweet tweet;
	public Tweet getTweet() {
		return this.tweet;
	}
	public void setTweet(Tweet tweet) {
		this.tweet = tweet;
	}
	
	@ManyToOne
	@JoinColumn(name="user_id")
	private TwitterUser twitterUser;
	public TwitterUser getTwitterUser() {
		return this.twitterUser;
	}
	public void setTwitterUser(TwitterUser twtwitterUsereet) {
		this.twitterUser = twitterUser;
	}
		
		
		
}