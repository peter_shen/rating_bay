package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the platform database table.
 * 
 */
@Entity
@Table(name="platform")
public class Platform implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="PLATFORM_ID_GENERATOR", sequenceName="PLATFORM_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PLATFORM_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(nullable=false, length=2147483647)
	private String name;
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//bi-directional many-to-one association to GamePlatform
	@OneToMany(mappedBy="platform", cascade={CascadeType.REMOVE})
	private Set<GamePlatform> gamePlatforms;
	public Set<GamePlatform> getGamePlatforms() {
		return this.gamePlatforms;
	}
	public void setGamePlatforms(Set<GamePlatform> gamePlatforms) {
		this.gamePlatforms = gamePlatforms;
	}
}