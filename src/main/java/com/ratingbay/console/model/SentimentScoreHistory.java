package com.ratingbay.console.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import javax.persistence.*;


/**
 * The persistent class for the sentiment_score_history database table.
 * 
 */
@Entity
@Table(name="sentiment_score_history")
public class SentimentScoreHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="SENTIMENT_SCORE_HISTORY_ID_GENERATOR", sequenceName="SENTIMENT_SCORE_HISTORY_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SENTIMENT_SCORE_HISTORY_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="insert_time", nullable=false)
	private Date insertTime;
	public Date getInsertTime() {
		return this.insertTime;
	}
	public void setInsertTime(Date insertTime) {
		this.insertTime = insertTime;
	}

	@Column(precision=4, scale=2)
	private Double score;
	public Double getScore() {
		return this.score;
	}
	public void setScore(Double score) {
		this.score = score;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame() {
		return this.idGame;
	}
	public void setIdGame(Long idGame) {
		this.idGame = idGame;
	}

    @Column(name="positive_count")
	private Long positiveCount;
	public Long getPositiveCount() {
		return this.positiveCount;
	}
	public void setPositiveCountd(Long positiveCount) {
		this.positiveCount = positiveCount;
	}

	@Column(name="negative_count")
	private Long negativeCount;
	public Long getNegativeCount() {
		return this.negativeCount;
	}
	public void setNegativeCount(Long negativeCount) {
		this.negativeCount = negativeCount;
	}

	@Column(name="duration")
	private Long duration;
	public Long getDuration() {
		return this.duration;
	}
	public void setDuration(Long duration) {
		this.duration = duration;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
}