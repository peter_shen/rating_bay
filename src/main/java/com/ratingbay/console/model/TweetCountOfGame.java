package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tweet_count_of_game.
 * 
 */
@Entity
@Table(name="tweet_count_of_game")
public class TweetCountOfGame implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame(){
		return this.idGame;
	}
	public void setIdGame(Long idGame){
		this.idGame = idGame;
	}

	@Column(name="tweet_count")
	private Long tweetCount;
	public Long getTweetCount(){
		return this.tweetCount;
	}
	public void setTweetCount(Long tweetCount){
		this.tweetCount = tweetCount;
	}
}