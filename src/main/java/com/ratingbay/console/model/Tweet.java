package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.math.BigDecimal;
import java.util.Set;


/**
 * The persistent class for the tweet database table.
 * 
 */
@Entity
@Table(name="tweet")
public class Tweet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TWEET_ID_GENERATOR", sequenceName="TWEET_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TWEET_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(nullable=false, length=2147483647)
	private String content;
	public String getContent() {
		return this.content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	@Column(name="create_date", nullable=false)
	private Timestamp createDate;
	public Timestamp getCreateDate() {
		return this.createDate;
	}
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name="geo_lat", precision=10, scale=5)
	private BigDecimal geoLat;
	public BigDecimal getGeoLat() {
		return this.geoLat;
	}
	public void setGeoLat(BigDecimal geoLat) {
		this.geoLat = geoLat;
	}

	@Column(name="geo_long", precision=10, scale=5)
	private BigDecimal geoLong;
	public BigDecimal getGeoLong() {
		return this.geoLong;
	}
	public void setGeoLong(BigDecimal geoLong) {
		this.geoLong = geoLong;
	}

	@Column(name="is_favourite", nullable=false)
	private Boolean isFavourite;
	public Boolean getIsFavourite() {
		return this.isFavourite;
	}
	public void setIsFavourite(Boolean isFavourite) {
		this.isFavourite = isFavourite;
	}

	@Column(name="is_retweet", nullable=false)
	private Boolean isRetweet;
	public Boolean getIsRetweet() {
		return this.isRetweet;
	}
	public void setIsRetweet(Boolean isRetweet) {
		this.isRetweet = isRetweet;
	}

	@Column(name="new_content", length=2147483647)
	private String newContent;
	public String getNewContent() {
		return this.newContent;
	}
	public void setNewContent(String newContent) {
		this.newContent = newContent;
	}

	@Column(name="relevant_probability", precision=4, scale=2)
	private BigDecimal relevantProbability;
		public BigDecimal getRelevantProbability() {
		return this.relevantProbability;
	}
	public void setRelevantProbability(BigDecimal relevantProbability) {
		this.relevantProbability = relevantProbability;
	}

	@Column(name="retweet_count")
	private Long retweetCount;
	public Long getRetweetCount() {
		return this.retweetCount;
	}
	public void setRetweetCount(Long retweetCount) {
		this.retweetCount = retweetCount;
	}

	@Column(name="retweet_id")
	private Long retweetId;
	public Long getRetweetId() {
		return this.retweetId;
	}
	public void setRetweetId(Long retweetId) {
		this.retweetId = retweetId;
	}

	@Column(name="sentiment_probability", precision=4, scale=2)
	private BigDecimal sentimentProbability;
	public BigDecimal getSentimentProbability() {
		return this.sentimentProbability;
	}
	public void setSentimentProbability(BigDecimal sentimentProbability) {
		this.sentimentProbability = sentimentProbability;
	}

	@Column(name="sentiment_type", nullable=false)
	private Integer sentimentType;
	public Integer getSentimentType() {
		return this.sentimentType;
	}
	public void setSentimentType(Integer sentimentType) {
		this.sentimentType = sentimentType;
	}

	

	@Column(name="user_id", nullable=false)
	private Long userId;
	public Long getUserId() {
		return this.userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}


	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame(){
		return this.idGame;
	}
	public void setIdGame(Long idGame){
		this.idGame = idGame;
	}

	//bi-directional many-to-one association to MentionedUser
	@OneToMany(mappedBy="tweet", cascade={CascadeType.REMOVE})
	private Set<MentionedUser> mentionedUsers;
	public Set<MentionedUser> getMentionedUsers() {
		return this.mentionedUsers;
	}
	public void setMentionedUsers(Set<MentionedUser> mentionedUsers) {
		this.mentionedUsers = mentionedUsers;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
	
	//bi-directional many-to-one association to twitterUser
    @ManyToOne
	@JoinColumn(name="user_id")
	private TwitterUser twitterUser;
	public TwitterUser getTwitterUser() {
		return this.twitterUser;
	}
	public void setTwitterUser(TwitterUser game) {
		this.twitterUser = twitterUser;
	}
	
	//bi-directional many-to-one association to TweetLink
	@OneToMany(mappedBy="tweet", cascade={CascadeType.REMOVE})
	private Set<TweetLink> tweetLinks;
	public Set<TweetLink> getTweetLinks() {
		return this.tweetLinks;
	}

	public void setTweetLinks(Set<TweetLink> tweetLinks) {
		this.tweetLinks = tweetLinks;
	}
}