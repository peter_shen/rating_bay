package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the game_platform database table.
 * 
 */
@Entity
@Table(name="game_platform")
public class GamePlatform implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GAME_PLATFORM_ID_GENERATOR", sequenceName="GAME_PLATFORM_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GAME_PLATFORM_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}

	//bi-directional many-to-one association to Platform
    @ManyToOne
	@JoinColumn(name="id_platform")
	private Platform platform;
	public Platform getPlatform() {
		return this.platform;
	}
	public void setPlatform(Platform platform) {
		this.platform = platform;
	}
}