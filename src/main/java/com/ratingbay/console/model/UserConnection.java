package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the userconnection database table.
 * 
 */
@Entity
@Table(name="userconnection")
public class UserConnection implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="USERCONNECTION_ID_GENERATOR", sequenceName="USERCONNECTION_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="USERCONNECTION_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(length=255)
	private String accesstoken;
	public String getAccesstoken() {
		return this.accesstoken;
	}
	public void setAccesstoken(String accesstoken) {
		this.accesstoken = accesstoken;
	}

	@Column(length=255)
	private String displayname;
	public String getDisplayname() {
		return this.displayname;
	}
	public void setDisplayname(String displayname) {
		this.displayname = displayname;
	}

	private Long expiretime;
	public Long getExpiretime() {
		return this.expiretime;
	}
	public void setExpiretime(Long expiretime) {
		this.expiretime = expiretime;
	}

	@Column(length=512)
	private String imageurl;
	public String getImageurl() {
		return this.imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}

	@Column(length=512)
	private String profileurl;
	public String getProfileurl() {
		return this.profileurl;
	}
	public void setProfileurl(String profileurl) {
		this.profileurl = profileurl;
	}

	@Column(length=255)
	private String providerid;
	public String getProviderid() {
		return this.providerid;
	}
	public void setProviderid(String providerid) {
		this.providerid = providerid;
	}

	@Column(length=255)
	private String provideruserid;
	public String getProvideruserid() {
		return this.provideruserid;
	}
	public void setProvideruserid(String provideruserid) {
		this.provideruserid = provideruserid;
	}

	private Integer rank;
	public Integer getRank() {
		return this.rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}

	@Column(length=255)
	private String refreshtoken;
	public String getRefreshtoken() {
		return this.refreshtoken;
	}
	public void setRefreshtoken(String refreshtoken) {
		this.refreshtoken = refreshtoken;
	}

	@Column(length=255)
	private String secret;
	public String getSecret() {
		return this.secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}

	//bi-directional many-to-one association to User
    @ManyToOne
	@JoinColumn(name="userid", nullable=false)
	private User user;
	public User getUser() {
		return this.user;
	}
	public void setUser(User user) {
		this.user = user;
	}
}