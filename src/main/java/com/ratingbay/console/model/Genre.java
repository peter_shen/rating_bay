package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Set;


/**
 * The persistent class for the genre database table.
 * 
 */
@Entity
@Table(name="genre")
public class Genre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GENRE_ID_GENERATOR", sequenceName="GENRE_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GENRE_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(length=2147483647)
	private String name;
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	//bi-directional many-to-one association to GameGenre
	@OneToMany(mappedBy="genre", cascade={CascadeType.REMOVE})
	private Set<GameGenre> gameGenres;
	public Set<GameGenre> getGameGenres() {
		return this.gameGenres;
	}
	public void setGameGenres(Set<GameGenre> gameGenres) {
		this.gameGenres = gameGenres;
	}
}