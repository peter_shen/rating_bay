package com.ratingbay.console.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tracking_keyword database table.
 * 
 */
@Entity
@Table(name="tracking_keyword")
public class TrackingKeyword implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="TRACKING_KEYWORD_ID_GENERATOR", sequenceName="TRACKING_KEYWORD_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="TRACKING_KEYWORD_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(name="is_primary", nullable=false)
	private Boolean isPrimary;
	public Boolean getIsPrimary() {
		return this.isPrimary;
	}
	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}

	@Column(nullable=false, length=2147483647)
	private String keyword;
	public String getKeyword() {
		return this.keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}
}