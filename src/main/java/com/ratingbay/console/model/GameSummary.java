package com.ratingbay.console.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;
import javax.persistence.*;

/**
 * The persistent class for the game database table.
 * 
 */
@Entity
@Table(name="game_summary")
public class GameSummary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="GAME_SUMMARY_ID_GENERATOR", sequenceName="GAME_SUMMARY_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="GAME_SUMMARY_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	
	private Long id;
	public Long getId(){
		return id;
	}
	public void setId(Long id){
		this.id = id;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame(){
		return idGame;
	}
	public void setIdGame(Long idGame){
		this.idGame = idGame;
	}

	@Column(name="id_most_positive_tweet")
	private Long idMostPositiveTweet;
	public Long getIdMostPositiveTweet(){
		return idMostPositiveTweet;
	}
	public void setIdMostPositiveTweet(Long idMostPositiveTweet){
		this.idMostPositiveTweet = idMostPositiveTweet;
	}

	@Column(name="id_most_negative_tweet")
	private Long idMostNegativeTweet;
	public Long getIdMostNegativeTweet(){
		return idMostNegativeTweet;
	}
	public void setIdMostNegativeTweet(Long idMostNegativeTweet){
		this.idMostNegativeTweet = idMostNegativeTweet;
	}

	@Column(name="id_most_retweet_tweet")
	private Long idMostRetweetTweet;
	public Long getIdMostRetweetTweet(){
		return idMostRetweetTweet;
	}
	public void setIdMostRetweetTweet(Long idMostRetweetTweet){
		this.idMostRetweetTweet = idMostRetweetTweet;
	}

	@Column(name="tweet_count") 
	private Long tweetCount;
	public Long getTweetCount(){
		return tweetCount;
	}
	public void setTweetCount(Long tweetCount){
		this.tweetCount = tweetCount;
	}

	@Column(name="sentiment_score", precision=4, scale=2)
	private BigDecimal sentimentScore;
	public BigDecimal getSentimentScore(){
		return sentimentScore;
	}
	public void setSentimentScore(BigDecimal sentimentScore){
		if(sentimentScore == null){
			sentimentScore = new BigDecimal(0);
		}
		this.sentimentScore = sentimentScore;
	}

    @OneToOne
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame(){
		return game;
	}
	public void setGame(Game game){
		this.game = game;
	}

	@OneToOne
	@JoinColumn(name="id_most_positive_tweet")
	private Tweet mostPositiveTweet;
	public Tweet getMostPositiveTweet(){
		return mostPositiveTweet;
	}
	public void setMostPositiveTweet(Tweet mostPositiveTweet){
		this.mostPositiveTweet = mostPositiveTweet;
	}

	@OneToOne
	@JoinColumn(name="id_most_negative_tweet")
	private Tweet mostNegativeTweet;
	public Tweet getMostNegativeTweet(){
		return mostNegativeTweet;
	}
	public void setMostNegativeTweet(Tweet mostNegativeTweet){
		this.mostNegativeTweet = mostNegativeTweet;
	}

	@OneToOne
	@JoinColumn(name="id_most_retweet_tweet")
	private Tweet mostRetweetTweet;
	public Tweet getMostRetweetTweet(){
		return this.mostRetweetTweet;
	}
	public void setMostRetweetTweet(Tweet mostRetweetTweet){
		this.mostRetweetTweet = mostRetweetTweet;
	}

}