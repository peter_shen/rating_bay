package com.ratingbay.console.model;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;
import java.util.Set;


/**
 * The persistent class for the link database table.
 * 
 */
@Entity
@Table(name="link")
public class Link implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@SequenceGenerator(name="LINK_ID_GENERATOR", sequenceName="LINK_ID_SEQ")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="LINK_ID_GENERATOR")
	@Column(unique=true, nullable=false)
	private Long id;
	public Long getId() {
		return this.id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	@Column(nullable=false, length=2147483647)
	private String link;
	public String getLink() {
		return this.link;
	}
	public void setLink(String link) {
		this.link = link;
	}

	@Column(name="media_direct_link", length=2147483647)
	private String mediaDirectLink;
	public String getMediaDirectLink() {
		return this.mediaDirectLink;
	}
	public void setMediaDirectLink(String mediaDirectLink) {
		this.mediaDirectLink = mediaDirectLink;
	}

	@Column(nullable=false)
	private Integer type;
	public Integer getType() {
		return this.type;
	}
	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name="tweet_count")
	private Integer tweetCount;
	public Integer getTweetCount() {
		return this.tweetCount;
	}
	public void setTweetCount(Integer tweetCount) {
		this.tweetCount = tweetCount;
	}

	@Column(name="id_game")
	private Long idGame;
	public Long getIdGame(){
		return this.idGame;
	}
	public void setIdGame(Long idGame){
		this.idGame = idGame;
	}
	
	@Column(name="create_date")
	private Date createDate;
	public Date getCreateDate(){
		return this.createDate;
	}
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}

	//bi-directional many-to-one association to Game
    @ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_game")
	private Game game;
	public Game getGame() {
		return this.game;
	}
	public void setGame(Game game) {
		this.game = game;
	}

	//bi-directional many-to-one association to TweetLink
    @OneToMany(mappedBy="link", cascade={CascadeType.REMOVE})
    private Set<TweetLink> tweetLinks;
    public Set<TweetLink> getTweetLinks() {
        return this.tweetLinks;
	}
	public void setTweetLinks(Set<TweetLink> tweetLinks) {
        this.tweetLinks = tweetLinks;
	}
}