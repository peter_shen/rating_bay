package com.ratingbay.console.api;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class InfluentialUserApi extends Api{
	protected InfluentialUserApi(IInfluentialUserService iUserService){
		setInfluentialUserService(iUserService);
	}

	protected static InfluentialUserApi iUserApi = null;
	public static InfluentialUserApi getInstance(IInfluentialUserService iUserService){
		if(null == iUserApi){
			iUserApi = new InfluentialUserApi(iUserService);
		}
		return iUserApi;
	}
	
    public IInfluentialUserService iUserService;
	public IInfluentialUserService getInfluentialUserService() {
		return iUserService;
	}
	public void setInfluentialUserService(IInfluentialUserService iUserService) {
		this.iUserService = iUserService;
	}

	public Page<InfluentialUser> getTopInfluentialUsers(int pageNum, int pageSize){
		Page<InfluentialUser> topInfluentialUsers = null;
		if(cacheManager.isExist(CacheCat.INFLUENTIAL_USER_API, "topInfluentialUsers", pageNum)){
		topInfluentialUsers = (Page<InfluentialUser>) cacheManager.get(
			CacheCat.INFLUENTIAL_USER_API, "topInfluentialUsers", pageNum);
		}else{
			topInfluentialUsers = iUserService.findTopPlayers(pageNum, pageSize);
			cacheManager.put(CacheCat.INFLUENTIAL_USER_API, "topInfluentialUsers",
				pageNum, (Object) topInfluentialUsers);
		}
		return topInfluentialUsers;
	}

	public Page<InfluentialUser> getTopPlayersByGame(Long idGame, int pageNum, int pageSize){
		Page<InfluentialUser> topPlayersByGame = null;
		if( cacheManager.isExist(CacheCat.INFLUENTIAL_USER_API, "topPlayersByGame"+ idGame, pageNum) ){
		topPlayersByGame = (Page<InfluentialUser>) cacheManager.get(
			CacheCat.INFLUENTIAL_USER_API, "topPlayersByGame" + idGame, pageNum);
		}else{
			topPlayersByGame = iUserService.findTopPlayersByGame(idGame, pageNum, pageSize);
			cacheManager.put(CacheCat.INFLUENTIAL_USER_API, "topPlayersByGame" + idGame,
				pageNum, (Object) topPlayersByGame);
		}
		return topPlayersByGame;
	}
	
	public Page<InfluentialUser> getTopInfluentialUser(int pageNum ,int pageSize){
		Page<InfluentialUser> topInfluentialUser = null;
		if( cacheManager.isExist(CacheCat.INFLUENTIAL_USER_API, "topInfluentialUser", pageNum) ){
			topInfluentialUser = (Page<InfluentialUser>) cacheManager.get(
					CacheCat.INFLUENTIAL_USER_API, "topInfluentialUser", pageNum);
		}else{
			topInfluentialUser = iUserService.findTopPlayer(pageNum, pageSize);
			cacheManager.put(CacheCat.INFLUENTIAL_USER_API, "topInfluentialUser",
				pageNum, (Object) topInfluentialUser);
		}
		return topInfluentialUser;
	}
}