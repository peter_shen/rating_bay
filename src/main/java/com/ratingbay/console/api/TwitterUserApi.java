package com.ratingbay.console.api;

import java.util.*;

import org.apache.struts2.ServletActionContext;
import org.springframework.data.domain.*;
import org.springframework.social.twitter.api.CursoredList;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.TwitterProfile;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.common.ISession;
import com.ratingbay.console.common.RatingBayConst;
import com.ratingbay.console.dataimpl.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class TwitterUserApi extends SessionApi{
    public TwitterUserApi(ITwitterUserService twitterUserService, ISession session){
        super(session);
        this.twitterUserService = twitterUserService;
    }
    public ITwitterUserService twitterUserService;
    public ITwitterUserService getTwitterUserService() {
        return twitterUserService;
    }
    public void setTwitterUserService(ITwitterUserService twitterUserService) {
        this.twitterUserService = twitterUserService;
    }
    public static TwitterUserApi getInstance(ITwitterUserService twitterUserService, ISession session){
        return new TwitterUserApi(twitterUserService, session);
    }


    //used in class inner
    public List<Long> getTwitterUserIds(List<ITwitterUser> iTwitterUsers) {
        List<Long> tUserIds = new ArrayList<Long>();
        if(null == iTwitterUsers) return tUserIds;

        Iterator<ITwitterUser> it= iTwitterUsers.iterator();

        while(it.hasNext()){
            tUserIds.add( it.next().getId() );
        }
        
        return tUserIds;
    }


    //can used without user logged in
    public ITwitterUser getITwitterUser(Long tUserId){
        ITwitterUser tUser = (ITwitterUser) cacheManager.get(CacheCat.TWITTER_USER, 
            "twitterUser", tUserId);
        if(null == tUser){
            tUser = twitterUserService.findByUserId(tUserId);
            cacheManager.put(CacheCat.TWITTER_USER, "twitterUser", tUserId, tUser);
        }
        return tUser;        
    }
    
    public Long getUsersByName(String uName){
        return twitterUserService.findUserByUserName(uName);
    }
    
    public Page<ITwitterUser> getUsersByName(String uName, int pageNum, int pageSize){
        Page<ITwitterUser> users = twitterUserService.findUsersByUserNameLike(uName, pageNum, pageSize);
        return users;
    }
    public Page<ITwitterUser> getUsersByName(String uName, String f, String d, int pageNum, int pageSize){
        Page<ITwitterUser> users = twitterUserService.findUsersByUserNameLike(uName, f, d, pageNum, pageSize);
        return users;
    }


    List<ITwitterUser> friends = null;
    public List<ITwitterUser> getTwitterFriends(Long tUserId) {
        List<ITwitterUser> friends = (List<ITwitterUser>) cacheManager.get(CacheCat.TWITTER_USER, "friends", tUserId);
        if( (null == friends) && isLogged ){
            Twitter twitter = getTwitter();
            ITwitterUser iTwitterUser = getITwitterUser();

            followers = TwitterUserImplWithTwitterProfile.getITwitterUsers(twitter.friendOperations().getFollowers(tUserId));

            friends = TwitterUserImplWithTwitterProfile.getITwitterUsers(twitter.friendOperations().getFriends(tUserId));

            cacheManager.put(CacheCat.TWITTER_USER, "friends", tUserId, friends);
            cacheManager.put(CacheCat.TWITTER_USER, "followers", tUserId, followers);
        }
        return friends;
    }

    private List<ITwitterUser> followers = null;
    public List<ITwitterUser> getTwitterFollowers(Long tUserId){
        if(null == followers){
            List<ITwitterUser> followers = (List<ITwitterUser>) cacheManager.get(CacheCat.TWITTER_USER, "followers", tUserId);
            if( (null == followers) && isLogged ){
                Twitter twitter = getTwitter();
                ITwitterUser iTwitterUser = getITwitterUser();

                followers = TwitterUserImplWithTwitterProfile.getITwitterUsers(twitter.friendOperations().getFollowers(tUserId));

                friends = TwitterUserImplWithTwitterProfile.getITwitterUsers(twitter.friendOperations().getFriends(tUserId));

                cacheManager.put(CacheCat.TWITTER_USER, "friends", tUserId, friends);
                cacheManager.put(CacheCat.TWITTER_USER, "followers", tUserId, followers);
            }
        }
        return followers;
    }

    public List<Long> getTwitterFriendsIds(Long tUserId){
        return getTwitterUserIds(getTwitterFriends(tUserId));
    }

    
    //below api used only when user logged in
    public Twitter getTwitter(){
        	Twitter twitter=(Twitter)ServletActionContext.getRequest().getSession().getAttribute(RatingBayConst.SESSION_KEY_TWITTER);
           // twitter = (Twitter)this.getSession().get(RatingBayConst.SESSION_KEY_TWITTER);
        return twitter;
    }

    public ITwitterUser getITwitterUser(){
    	ITwitterUser	iTwitterUser=(ITwitterUser)ServletActionContext.getRequest().getSession().getAttribute(RatingBayConst.SESSION_KEY_USER);
    	//ITwitterUser   iTwitterUser = (ITwitterUser)this.getSession().get(RatingBayConst.SESSION_KEY_USER);
        return iTwitterUser;
    }

    public List<Long> getTwitterFriendsIds(){
    	ITwitterUser iTwitterUser = getITwitterUser();
        if(null == iTwitterUser) return new ArrayList<Long>();
        
        return getTwitterUserIds(getTwitterFriends(iTwitterUser.getId()));
    }
    
    public List<TwitterUser> getUsersByNameLike(String uName, int pageNum, int pageSize){
        List<TwitterUser> users = twitterUserService.findUserNameByUserNameLike(uName, pageNum, pageSize);
        return users;
    }
}