package com.ratingbay.console.api;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class LinkApi extends Api{
	protected LinkApi(ILinkService linkService){
		setLinkService(linkService);
	}

	protected static LinkApi linkApi = null;
	public static LinkApi getInstance(ILinkService linkService){
		if(null == linkApi){
			linkApi = new LinkApi(linkService);
		}
		return linkApi;
	}

	public ILinkService linkService;
	public ILinkService getLinkService() {
		return linkService;
	}
	public void setLinkService(ILinkService linkService) {
		this.linkService = linkService;
	}

	public Page<Link> getPopularVideoLinksByGame(Long idGame, Date from, Date to, int pageNum, int pageSize){
		Page<Link> popularVideoLinksByGame = null;
		if(cacheManager.isExist(CacheCat.LINK_API, "popularVideoLinksByGame" + idGame, pageNum)){
			popularVideoLinksByGame = (Page<Link>)
				cacheManager.get(CacheCat.LINK_API, "popularVideoLinksByGame" + idGame, pageNum);
		}else{
			popularVideoLinksByGame = this.linkService.findGameMostPopularLinks(0, idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.LINK_API, "popularVideoLinksByGame" + idGame,
				pageNum, (Object)popularVideoLinksByGame);
		}
		return popularVideoLinksByGame;
	}

	public Page<Link> getPopularImageLinksByGame(Long idGame, Date from, Date to, int pageNum, int pageSize){
		Page<Link> popularImageLinksByGame = null;
		if(cacheManager.isExist(CacheCat.LINK_API, "popularImageLinksByGame" + idGame, pageNum)){
			popularImageLinksByGame = (Page<Link>)
				cacheManager.get(CacheCat.LINK_API, "popularImageLinksByGame" + idGame, pageNum);
		}else{
			popularImageLinksByGame = linkService.findGameMostPopularLinks(1, idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.LINK_API, "popularImageLinksByGame" + idGame, 
					pageNum, (Object)popularImageLinksByGame);
		}
		return popularImageLinksByGame;
	}

	public Page<Link> getPopularLinksByGame(Long idGame, Date from, Date to, int pageNum, int pageSize){
		Page<Link> popularLinksByGame = null;
		if(cacheManager.isExist(CacheCat.LINK_API, "popularLinksByGame" + idGame, pageNum)){
			popularLinksByGame = (Page<Link>)cacheManager.get(CacheCat.LINK_API,
				"popularLinksByGame" + idGame, pageNum);
		}else{
			popularLinksByGame = linkService.findGameMostPopularLinks(2, idGame, from, to,
				pageNum, pageSize);
			cacheManager.put(CacheCat.LINK_API, "popularLinksByGame" + idGame,
					pageNum, (Object)popularLinksByGame);
		}
		return popularLinksByGame;
	}

	public Page<Link> getUsersVideoLinksByGame(Long idGame, List<Long> tUserIds,
		int pageNum, int pageSize){

		Page<Link> usersVideoLinks = null;
		if(cacheManager.isExist(CacheCat.LINK_API, "usersVideoLinks" + idGame, pageNum)){
			usersVideoLinks = (Page<Link>)cacheManager.get(CacheCat.LINK_API,
				"usersVideoLinks" + idGame, pageNum);
		}else{
			usersVideoLinks = linkService.findGameLinksByTwitterUsers(0, idGame, tUserIds, pageNum, pageSize);
			cacheManager.put(CacheCat.LINK_API, "usersVideoLinks" + idGame,
					pageNum, (Object)usersVideoLinks);
		}
		return usersVideoLinks;
	}

	public Page<Link> getUsersImageLinksByGame(Long idGame, List<Long> tUserIds,
		int pageNum, int pageSize){

		Page<Link> usersImageLinks = null;
				if(cacheManager.isExist(CacheCat.LINK_API, "usersImageLinks" + idGame, pageNum)){
					usersImageLinks = (Page<Link>)cacheManager.get(CacheCat.LINK_API,
						"usersImageLinks" + idGame, pageNum);
				}else{
					usersImageLinks = linkService.findGameLinksByTwitterUsers(1, idGame, tUserIds, pageNum, pageSize);
					cacheManager.put(CacheCat.LINK_API, "usersImageLinks" + idGame,
							pageNum, (Object)usersImageLinks);
				}
		return usersImageLinks;
	}

	public Page<Link> getUsersLinksByGame(Long idGame, List<Long> tUserIds,
		int pageNum, int pageSize){
		
		Page<Link> usersLinks = null;
				if(cacheManager.isExist(CacheCat.LINK_API, "usersLinks" + idGame, pageNum)){
					usersLinks = (Page<Link>)cacheManager.get(CacheCat.LINK_API,
						"usersLinks" + idGame, pageNum);
				}else{
					usersLinks = linkService.findGameLinksByTwitterUsers(2, idGame, tUserIds, pageNum, pageSize);
					cacheManager.put(CacheCat.LINK_API, "usersLinks" + idGame,
							pageNum, (Object)usersLinks);
				}
		return usersLinks;
	}

	public Page<Link> getPopularVideoLinks(Date from, Date to, int pageNum, int pageSize){
		Page<Link> popularVideoLinks = null;
		if(cacheManager.isExist(CacheCat.LINK_API, "popularVideoLinks", pageNum)){
			popularVideoLinks = (Page<Link>)cacheManager.get(CacheCat.LINK_API, "popularVideoLinks", pageNum);
		}else{
			popularVideoLinks = linkService.findMostPopularVideoLinks(0, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.LINK_API, "popularVideoLinks", pageNum, (Object)popularVideoLinks);
		}
		return popularVideoLinks;
	}
	
}