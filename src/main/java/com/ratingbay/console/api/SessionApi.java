package com.ratingbay.console.api;

import com.ratingbay.console.common.ISession;

public class SessionApi extends Api{
	
	public SessionApi(ISession session){
		this.setSession(session);
        this.isLogged = this.getIsLogged();
	}

	protected ISession session;
    public ISession getSession() {
        return session;
    }
    public void setSession(ISession session) {
        this.session = session;
    }

    protected boolean isLogged = false;
    public boolean getIsLogged(){
        if(null != getSession().get("userId")){
            return true;
        }
        return false;
    }

}