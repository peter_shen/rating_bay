package com.ratingbay.console.api;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class GameApi extends Api{
	protected GameApi(IGameService gameService){
		setGameService(gameService);
	}

	protected static GameApi gameApi = null;
	public static GameApi getInstance(IGameService gameService){
		if(null == gameApi){
			gameApi = new GameApi(gameService);
		}
		return gameApi;
	}
	
	public IGameService gameService;
	public IGameService getGameService() {
		return gameService;
	}
	public void setGameService(IGameService gameService) {
		this.gameService = gameService;
	}

	public IGameDetail getGameDetail(Long idGame){
		IGameDetail iGameDetail = gameService.findGameDetail(idGame);
		return iGameDetail;
	}

	public Page<IGameSummary> getHeadlineGames(Date from, Date to, int pageNum, int pageSize){
		Page<IGameSummary> headlineGames = null;
		if( cacheManager.isExist(CacheCat.GAME_API, "headlineGames") ){
			headlineGames = (Page<IGameSummary>) 
				cacheManager.get(CacheCat.GAME_API, "headlineGames");
		}else{
			headlineGames = gameService.findHeadlineGames(from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.GAME_API, "headlineGames", (Object) headlineGames);
		}
		return headlineGames;
	}
	
	public List<SentimentScoreHistory> getScoreHistory(Long idGame,Date beforeDate,Date curDate){
		List<SentimentScoreHistory> score = gameService.findScoreHistory(idGame,beforeDate,curDate);
		return score;
	}
	
	public List<RbGameTweetTrend> getTweetHistory(Long idGame,Date beforeDate,Date curDate){
		List<RbGameTweetTrend> tweetHistory = gameService.findTweetHistory(idGame,beforeDate,curDate);
		return tweetHistory;
	}
	
	public Page<IGameSummary> getGamesByTitle(String title, int pageNum, int pageSize){
		Page<IGameSummary> gamesByTitle = 
			gameService.findByTitleContains(title, pageNum, pageSize);
		return gamesByTitle;
	}
	public Page<IGameSummary> getGamesByTitle(String title, String f, String d, int pageNum, int pageSize){
		Page<IGameSummary> gamesByTitle = 
			gameService.findByTitleContains(title, f, d, pageNum, pageSize);
		return gamesByTitle;
	}
	
	public List<IGameSummary> getTopTweetedGamesRestul(Date from, Date to, int pageNum, int pageSize){
		List<IGameSummary> topTweetedGames = null;
		if( cacheManager.isExist(CacheCat.GAME_API, "topTweetedGamesRestul", pageNum) ){
			topTweetedGames = (List<IGameSummary>) 
				cacheManager.get(CacheCat.GAME_API, "topTweetedGamesRestul", pageNum);
		}else{
			topTweetedGames = gameService.findTopTweetedGamesRestul(from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.GAME_API, "topTweetedGamesRestul", pageNum, (Object) topTweetedGames);
		}
		return topTweetedGames;
	}
	
	public List<IGameSummary> getTopTweetedGames(Date from, Date to, int pageNum, int pageSize){
		List<IGameSummary> topTweetedGames = null;
		if( cacheManager.isExist(CacheCat.GAME_API, "topTweetedGames", pageNum) ){
			topTweetedGames = (List<IGameSummary>) 
				cacheManager.get(CacheCat.GAME_API, "topTweetedGames", pageNum);
		}else{
			topTweetedGames = gameService.findTopTweetedGames(from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.GAME_API, "topTweetedGames", pageNum, (Object) topTweetedGames);
		}
		return topTweetedGames;
	}
	
	public List<IGameSummary> getTopRatedGames(Date from, Date to, int pageNum, int pageSize){
		List<IGameSummary> topRatedGames = null;
		if( cacheManager.isExist(CacheCat.GAME_API, "topRatedGames", pageNum) ){
			topRatedGames = (List<IGameSummary>) 
				cacheManager.get(CacheCat.GAME_API, "topRatedGames", pageNum);
		}else{
			topRatedGames = gameService.findTopRatedGames(from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.GAME_API, "topRatedGames", pageNum, (Object) topRatedGames);
		}
		return topRatedGames;
	}
	
	//user page find games
	public Page<Game> getGamebyUser(Long tUserId, int pageNum, int pageSize){
		return gameService.findbyUserId(tUserId, pageNum, pageSize);
	}

	public Page<Game> getGamebyUser(List<Long> tUserIds, int pageNum, int pageSize){
		if(null == tUserIds || tUserIds.size() < 1) return null;

		return gameService.findbyUserIds(tUserIds, pageNum, pageSize);
	}

	public Page<IGameSummary> getRecommendedGamesByGame(Long idGame, int pageNum, int pageSize){
		return gameService.findRelatedGamesByGame(idGame, pageNum, pageSize);
	}
	
	public List<Game> getGameByTitleLike(String content,int pageNum,int pageSize){
		return gameService.findGameByTitleLike(content,pageNum,pageSize);
	}
	
	public Page<IGameSummary> getGamesByGenre(Long genre, int pageNum, int pageSize){
		Page<IGameSummary> gamesByGenre = 
			gameService.findByGenre(genre,pageNum, pageSize);
		return gamesByGenre;
	}
	public Page<IGameSummary> getGamesByGenre(Long genre, String f, String d, int pageNum, int pageSize){
		Page<IGameSummary> gamesByGenre = 
			gameService.findGamesByGenre(genre, f, d, pageNum, pageSize);
		return gamesByGenre;
	}
	
	public Page<IGameSummary> getGamesByPlatform(String platform, int pageNum, int pageSize){
		Page<IGameSummary> gamesByPlatform = 
			gameService.findByPlatform(platform,pageNum, pageSize);
		return gamesByPlatform;
	}
	
	public Page<IGameSummary> getGamesByPlatform(String platform,String f,String d,int pageNum, int pageSize){
		Page<IGameSummary> gamesByPlatform = 
			gameService.findByPlatform(platform,f,d,pageNum, pageSize);
		return gamesByPlatform;
	}
	
	public List<String> getGenreById(Long id){
		return gameService.findGenreById(id);
	}
	
	public List<String> getPlatformById(Long id){
		return gameService.findPlatformById(id);
	}
	
	public Page<IGameSummary> getReleasedGames(Date now,String f,String d,int pageNum, int pageSize){
		Page<IGameSummary> findReleaseGames = 
			gameService.findReleaseGames(now,f,d,pageNum, pageSize);
		return findReleaseGames;
	}
	
	public Page<IGameSummary> getReleasedGames(Date now,int pageNum, int pageSize){
		Page<IGameSummary> findReleaseGames = 
			gameService.findReleaseGames(now,pageNum, pageSize);
		return findReleaseGames;
	}
	
	public Page<IGameSummary> getComingGames(Date now,String f,String d,int pageNum, int pageSize){
		Page<IGameSummary> findComingGames = 
			gameService.findComingGames(now,f,d,pageNum, pageSize);
		return findComingGames;
	}
	
	public Page<IGameSummary> getComingGames(Date now,int pageNum, int pageSize){
		Page<IGameSummary> findComingGames = 
			gameService.findComingGames(now,pageNum, pageSize);
		return findComingGames;
	}
	
	public String findGameGenreName(Long genre){
		String gen = gameService.findGameGenreName(genre);
		return gen;
	}
	
	public List<Game> findGameEnable(Long gameId){
		List<Game> status = gameService.findGameEnable(gameId);
		return status;
	}
}
