package com.ratingbay.console.api;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.databean.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class WordApi extends Api{
	protected WordApi(IWordService wordService){
		setWordService(wordService);
	}

	protected static WordApi wordApi = null;
	public static WordApi getInstance(IWordService wordService){
		if(null == wordApi){
			wordApi = new WordApi(wordService);
		}
		return wordApi;
	}
	
	public IWordService wordService;
	public IWordService getWordService() {
		return wordService;
	}
	public void setWordService(IWordService wordService) {
		this.wordService = wordService;
	}

	public Page<IHashTagSummary> getHashtagsOfPage(Long idGame, int pageNum, int pageSize){
		Page<IHashTagSummary> hashTagsOfPage = null;
		hashTagsOfPage = wordService.findHashTagSummaryByGame(idGame, pageNum, pageSize);
		return hashTagsOfPage;
	}

	public Set<IHashTagSummary> getHashtagsOfSet(Long idGame, int pageNum, int pageSize){
		Page<IHashTagSummary> hashTagsOfPage = getHashtagsOfPage(idGame, pageNum, pageSize);
		if(0 == hashTagsOfPage.getSize()) return null;

		Set<IHashTagSummary> hashTagsOfSet = new HashSet<IHashTagSummary>(hashTagsOfPage.getContent());
		return hashTagsOfSet;
	}

}
