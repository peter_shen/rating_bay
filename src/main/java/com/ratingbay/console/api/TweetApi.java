package com.ratingbay.console.api;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.cache.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.*;

public class TweetApi extends Api{
	protected TweetApi(ITweetService tweetService){
		setTweetService(tweetService);
	}
	public ITweetService tweetService;
	public ITweetService getTweetService() {
		return tweetService;
	}
	public void setTweetService(ITweetService tweetService) {
		this.tweetService = tweetService;
	}
	protected static TweetApi tweetApi = null;
	public static TweetApi getInstance(ITweetService tweetService){
		if(null == tweetApi){
			tweetApi = new TweetApi(tweetService);
		}
		return tweetApi;
	}
	

	public Page<Tweet> getLatestTweetsByGame(Long idGame, Date from, Date to,
			int pageNum, int pageSize) {
		Page<Tweet> latestTweetsByGame = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "latestTweetsByGame" + idGame, pageNum)){
			latestTweetsByGame = (Page<Tweet>)cacheManager.get(CacheCat.TWEET_API, "latestTweetsByGame" + idGame, pageNum);
		}else{
			latestTweetsByGame = this.tweetService.findLatestTweetsByGame(idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.TWEET_API, "latestTweetsByGame" + idGame, pageNum, (Object)latestTweetsByGame);
		}
		return latestTweetsByGame;
	}

	public Page<Tweet> getPopularTweetsByGame(Long idGame, Date from, Date to,
			int pageNum, int pageSize) {
		Page<Tweet> popularTweetsByGame = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "popularTweetsByGame" + idGame, pageNum)){
			popularTweetsByGame = (Page<Tweet>)cacheManager.get(CacheCat.TWEET_API, "popularTweetsByGame" + idGame, pageNum);
		}else{
			popularTweetsByGame = this.tweetService.findPopularTweetsByGame(idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.TWEET_API, "popularTweetsByGame" + idGame, pageNum, (Object)popularTweetsByGame);
		}
		return popularTweetsByGame;
	}
	
	public Page<Tweet> getNegativeTweetsByGame(Long idGame, Date from, Date to,
			int pageNum, int pageSize) {
		Page<Tweet> negativeTweetsByGame = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "negativeTweetsByGame" + idGame, pageNum)){
			negativeTweetsByGame = (Page<Tweet>)cacheManager.get(CacheCat.TWEET_API, "negativeTweetsByGame" + idGame, pageNum);
		}else{
			negativeTweetsByGame = this.tweetService.findNegativeTweetsByGame(idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.TWEET_API, "negativeTweetsByGame" + idGame, pageNum, (Object)negativeTweetsByGame);
		}
		return negativeTweetsByGame;
	}

	public Page<Tweet> getPositiveTweetsByGame(Long idGame, Date from, Date to,
			int pageNum, int pageSize) {
		Page<Tweet> positiveTweetsByGame = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "positiveTweetsByGame" + idGame, pageNum)){
			positiveTweetsByGame = (Page<Tweet>)cacheManager.get(CacheCat.TWEET_API, "positiveTweetsByGame" + idGame, pageNum);
		}else{
			positiveTweetsByGame = this.tweetService.findPositiveTweetsByGame(idGame, from, to, pageNum, pageSize);
			cacheManager.put(CacheCat.TWEET_API, "positiveTweetsByGame" + idGame, pageNum, (Object)positiveTweetsByGame);
		}
		return positiveTweetsByGame;
	}

	public Page<Tweet> getUsersTweetsByGame(Long idGame, List<Long> tUserIds, int pageNum, int pageSize) {
		Page<Tweet> usersTweetsByGame = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "usersTweetsByGame" + tUserIds + idGame, pageNum)){
			usersTweetsByGame = (Page<Tweet>)cacheManager.get(CacheCat.TWEET_API, "usersTweetsByGame" + tUserIds + idGame, pageNum);
		}else{
			usersTweetsByGame = this.tweetService.findUsersTweetsByGameAndUsers(idGame, tUserIds, pageNum, pageSize);
			cacheManager.put(CacheCat.TWEET_API, "usersTweetsByGame" + tUserIds + idGame, pageNum, (Object)usersTweetsByGame);
		}
		return usersTweetsByGame;
	}
	
	public List<Tweet> getRecentTweets(Date begin,Date end){
		List<Tweet> recentTweets = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "recentTweets")){
			recentTweets = (List<Tweet>)cacheManager.get(CacheCat.TWEET_API, "recentTweets");
		}else{
			recentTweets = tweetService.findRecentTweets(begin,end);
			cacheManager.put(CacheCat.TWEET_API, "recentTweets", (Object)recentTweets);
		}
		return recentTweets;
	}

	public List<Tweet> getPopularTweets(Date from, Date to){
		List<Tweet> popularTweets = null;
		if(cacheManager.isExist(CacheCat.TWEET_API, "popularTweets")){
			popularTweets = (List<Tweet>)cacheManager.get(CacheCat.TWEET_API, "popularTweets");
		}else{
			popularTweets = tweetService.findPopularTweets(from, to);
			cacheManager.put(CacheCat.TWEET_API, "popularTweets", (Object)popularTweets);
		}
		return popularTweets;
	}

	public Long getGameCountByUser(Long tUserId){
		Long gameCountByUser = null;
		if( cacheManager.isExist(CacheCat.TWEET_API, "gameCountByUser", tUserId) ){
			gameCountByUser = (Long) 
				cacheManager.get(CacheCat.TWEET_API, "gameCountByUser", tUserId);
		}else{
			gameCountByUser = tweetService.findGameCountByUser(tUserId);
			cacheManager.put(CacheCat.TWEET_API, "gameCountByUser", tUserId, (Object) gameCountByUser);
		}
		return gameCountByUser;
	}

	public Long getTweetCountByGame(Long idGame){
		Long tweetCountByGame = null;
		if( cacheManager.isExist(CacheCat.TWEET_API, "tweetCountByGame", idGame) ){
			tweetCountByGame = (Long) 
				cacheManager.get(CacheCat.TWEET_API, "tweetCountByGame", idGame);
		}else{
			tweetCountByGame = tweetService.findCountByGame(idGame);
			cacheManager.put(CacheCat.TWEET_API, "tweetCountByGame", idGame, (Object) tweetCountByGame);
		}
		return tweetCountByGame;
	}

	public Page<IGamePlayer> getGamersByUserIds(List<Long> tUserIds, int pageNum, int pageSize){
		Page<IGamePlayer> gamePlayers = tweetService.findGamersByUserIds(tUserIds, pageNum, pageSize);
		return gamePlayers;
	}

	public List<Tweet> getTweetsByLinkAndUsers(Long linkId, List<Long> tUserIds, int pageNum, int pageSize){
		return tweetService.getTweetsByLinkAndUsers(linkId, tUserIds, pageNum, pageSize);
	}
	
	public List<Tweet> getTweetsByLink(Long linkId, int pageNum, int pageSize){
		return tweetService.getTweetsByLink(linkId ,pageNum, pageSize);
	}
}