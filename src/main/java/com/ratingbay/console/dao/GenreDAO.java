package com.ratingbay.console.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Genre;

public interface GenreDAO extends JpaRepository<Genre, Long> {

	 @Query("select g from Genre g where g.id = ?1")
	 public List<Genre> findGameGenreName(Long genre);
}
