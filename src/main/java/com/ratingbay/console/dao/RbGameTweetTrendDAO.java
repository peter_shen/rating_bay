package com.ratingbay.console.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Link;
import com.ratingbay.console.model.RbGameTweetTrend;

public interface RbGameTweetTrendDAO extends JpaRepository <RbGameTweetTrend,Long> {
			//game page tweet history		
			@Query(
					value="select * from rb_game_tweet_trend where id_game = ?1 and create_date between ?2 and ?3 order by create_date desc",
				nativeQuery=true
				)
			public List<RbGameTweetTrend> findTweetHistory(Long gameId, Timestamp starttimestamp, Timestamp endtimestamp); 
}
