package com.ratingbay.console.dao;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.SentimentScoreHistory;

public interface SentimentScoreHistoryDAO extends JpaRepository<SentimentScoreHistory, Long> {
	
	@Query( 
			value = "select * from sentiment_score_history where id in(select max(id) from sentiment_score_history "
					+ "where id_game=?1 and insert_time between ?2 and ?3 "
					+ "GROUP BY to_char(insert_time, 'yyyy-mm-dd') order by max(insert_time) desc) order by insert_time desc",
			nativeQuery=true
			)
		public List<SentimentScoreHistory> findScoreHistory(Long idGame,Timestamp starttimestamp,Timestamp endtimestamp);
}