package com.ratingbay.console.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ratingbay.console.model.MentionedUser;
import com.ratingbay.console.model.Tweet;

public interface MentionedUserDAO extends JpaRepository<MentionedUser, Long> {
	
	public Page<MentionedUser> findByTweet(Tweet tweet, Pageable pageable);
	
}
