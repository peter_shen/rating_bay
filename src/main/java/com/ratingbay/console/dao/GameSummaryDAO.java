package com.ratingbay.console.dao;

import java.util.List;
import java.util.Set;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.*;

public interface GameSummaryDAO extends JpaRepository<GameSummary, Long> {
	@Query("select g from GameSummary g where g.idGame in ?1")
	public List<GameSummary> findByIdGameIn(List<Long> idGameList);
	
	@Query("select g from GameSummary g where g.idGame in ?1 order by g.sentimentScore desc")
	public List<GameSummary> findByIdGameOrderByScore(List<Long> idGameList);
	
	@Query("select g.tweetCount from GameSummary g where g.idGame = ?1")
	public Long findCountByGame(Long idGame);
}