package com.ratingbay.console.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import com.ratingbay.console.model.WordTweet;

public interface WordTweetDAO extends JpaRepository<WordTweet, Long> {
	
	@Query("select w.text,max(w.tweetCount) from WordTweet w where w.idGame = ?1 and w.isHashtag = ?2 group by w.text order by max(w.tweetCount) desc")
	public Page<Object[]> findWordSummaryByGameId(Long idGame, Boolean isHashTag, Pageable pageable);
}
