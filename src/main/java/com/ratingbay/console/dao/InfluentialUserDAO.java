package com.ratingbay.console.dao;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ratingbay.console.model.InfluentialUser;

public interface InfluentialUserDAO extends JpaRepository<InfluentialUser, Long> {

	@Query("select i from InfluentialUser i where i.insertTime between ?1 and ?2")
	public Page<InfluentialUser> findTopPlayers(Timestamp starttimestamp, Timestamp endtimestamp, Pageable pageable);

	@Query(
			value = "select i.* from influential_user i where "
					+ " id_game = ?1 and i.insert_time between ?2 and ?3 order by i.h_index desc "
					+ " limit 100",
			nativeQuery = true
			)
	public List<InfluentialUser> findTopPlayersByGame(Long gameid, Timestamp starttimestamp, Timestamp endtimestamp);
	
	@Query(
			value = "select i.* from influential_user i where "
					+ " id_game = -1 and i.insert_time between ?1 and ?2 order by i.h_index desc "
					+ " limit 100",
			nativeQuery = true
			)
    public List<InfluentialUser> findTopPlayer(Timestamp starttimestamp, Timestamp endtimestamp);
	
	@Query(
			value = "select max(insert_time) from influential_user where id_game = -1",
			nativeQuery = true
			)
    public List findInsertTimeByGame();

}