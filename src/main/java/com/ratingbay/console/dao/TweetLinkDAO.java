package com.ratingbay.console.dao;

import java.util.List;
import java.sql.Timestamp;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.*;

public interface TweetLinkDAO extends JpaRepository<TweetLink, Long> {
    @Query("select tl.idLink from TweetLink tl, Tweet t where t.id = tl.idTweet and t.userId in ?1")
	public List<Long> findLinkIdsByTwitterUserIds(List<Long> tUserIds);
}
