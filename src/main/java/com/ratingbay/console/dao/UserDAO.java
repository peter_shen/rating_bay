package com.ratingbay.console.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ratingbay.console.model.TwitterUser;
import com.ratingbay.console.model.User;


public interface UserDAO extends JpaRepository<User, Long> {
	
	public User findById(String userid);
	
	public User findByUsername(String username);

}
