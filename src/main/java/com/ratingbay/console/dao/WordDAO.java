package com.ratingbay.console.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Word;

public interface WordDAO extends JpaRepository<Word, Long> {

	//@Query("select w.text,count(w.idGame) from Word w where w.idGame = ?1 and w.isHashtag = ?2 group by w.text order by count(w.idGame) desc")
	//public Page<Object[]> findWordSummaryByGame(Long idGame, Boolean isHashTag, Pageable pageable);

}
