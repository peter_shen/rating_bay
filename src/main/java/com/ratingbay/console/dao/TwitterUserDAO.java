package com.ratingbay.console.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ratingbay.console.model.TwitterUser;
import com.ratingbay.console.model.User;

@Repository
public interface TwitterUserDAO extends JpaRepository<TwitterUser, Long> {
    
    @Query(
        	value = "select * from twitter_user t where t.user_id = ?1 order by t.last_update_time desc limit 1", 
        	nativeQuery = true
        	)
    public TwitterUser findTwitterUserByUserId(Long tUserId);
    
    @Query(
    		value = "select t.* from twitter_user t where LOWER(t.username) like LOWER(?1) limit ?3 offset ?2*?3",
    		nativeQuery = true
    		)
	public List<TwitterUser> findTwitterUserByUserNameLike(String uName, int pageNum,int pageSize);
}
