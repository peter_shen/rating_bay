package com.ratingbay.console.restful;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ratingbay.cache.*;
import com.ratingbay.console.api.*;
import com.ratingbay.console.dataface.*;
import com.ratingbay.console.common.*;
import com.ratingbay.console.web.controller.*;


@Controller
public class BaseRestful extends BaseController {
	
}