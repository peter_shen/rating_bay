package com.ratingbay.console.restful;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

@Controller
@RequestMapping("/rest/tweet")
public class TweetRestful extends BaseRestful{
	
	@RequestMapping(value="/countGameByUserId/{userId}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getGameCountByUser(@PathVariable Long userId){

		Long count = getTweetApi().getGameCountByUser(userId);

		return decoratCountGame(count);
	}

	@RequestMapping(value="/popularTweetsByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularTweetsByGame(@PathVariable Long idGame, @PathVariable int page){

		Long count = getTweetApi().getTweetCountByGame(idGame);
		Date beforeDate;
		Date curDate = new Date( System.currentTimeMillis() );
		if(count<50000){
			beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		}else{
			beforeDate = new Date( System.currentTimeMillis() - 2592000000L );
		}
		Page<Tweet> popularTweetsByGame = getTweetApi().getPopularTweetsByGame(idGame, beforeDate, curDate, page, 10);
		return decoratTweet(popularTweetsByGame, page);
	}

	@RequestMapping(value="/negativeTweetsByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getNegativeTweetsByGame(@PathVariable Long idGame, @PathVariable int page){

		Long count = getTweetApi().getTweetCountByGame(idGame);
		Date beforeDate;
		Date curDate = new Date( System.currentTimeMillis() );
		if(count<50000){
			beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		}else{
			beforeDate = new Date( System.currentTimeMillis() - 2592000000L );
		}
		
		Page<Tweet> negativeTweetsByGame = getTweetApi().getNegativeTweetsByGame(idGame, beforeDate, curDate, page, 10);
		return decoratTweet(negativeTweetsByGame, page);
	}

	@RequestMapping(value="/positiveTweetsByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPositiveTweetsByGame(@PathVariable Long idGame, @PathVariable int page){

		Long count = getTweetApi().getTweetCountByGame(idGame);
		Date beforeDate;
		if(count<50000){
			beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		}else{
			beforeDate = new Date( System.currentTimeMillis() - 2592000000L );
		}
		Date curDate = new Date( System.currentTimeMillis() );
		Page<Tweet> positiveTweetsByGame = getTweetApi().getPositiveTweetsByGame(idGame, beforeDate, curDate, page, 10);
		return decoratTweet(positiveTweetsByGame, page);
	}

	@RequestMapping(value="/latestTweetsByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getLatestTweetsByGame(@PathVariable Long idGame, @PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 2592000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		Page<Tweet> latestTweetsByGame = getTweetApi().getLatestTweetsByGame(idGame, beforeDate, curDate, page, 10);
		return decoratTweet(latestTweetsByGame, page);
	}
//game tweets friends
	@RequestMapping(value="/usersTweetsByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUsersTweetsByGame(@PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
		Page<Tweet> usersTweetsByGame = getTweetApi().getUsersTweetsByGame(idGame, tUserIds, page, 10);
		return decoratTweet(usersTweetsByGame, page);
	}
	@RequestMapping(value="/userTweetsByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUserTweetsByGame(@PathVariable Long idGame, @PathVariable Long userId, @PathVariable int page){

		List<Long> tUserIds = new ArrayList<Long>();
		tUserIds.add(userId);

		Page<Tweet> userTweetsByGame = getTweetApi().getUsersTweetsByGame(idGame, tUserIds, page, 5);
		return decoratTweet(userTweetsByGame, page);
	}
	@RequestMapping(value="/friendsTweetsByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getFriendsTweetsByGame(@PathVariable Long idGame, @PathVariable Long userId, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds(userId);

		Page<Tweet> friendsTweetsByGame = getTweetApi().getUsersTweetsByGame(idGame, tUserIds, page, 5);
		return decoratTweet(friendsTweetsByGame, page);
	}

	@RequestMapping(value="/topRecommendedGames/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getRecommendedGamesByGame(@PathVariable Long idGame, @PathVariable int page){

		Page<IGameSummary> RecommendedGamesByGame = getGameApi().getRecommendedGamesByGame(idGame, page, 5);
		return decoratIGameSummary(RecommendedGamesByGame, page);
	}
	
	@RequestMapping(value="/popularTweets/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularTweets(@PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		List<Tweet> list = getTweetApi().getPopularTweets(beforeDate, curDate);
		Page<Tweet> popularTweetsByGame = new PageImpl<Tweet>(list);
		return decoratTweet(popularTweetsByGame, page);
	}
	
	@RequestMapping(value="/recentTweets/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getRecentTweets(@PathVariable int page){
		Date beforeDate = new Date( System.currentTimeMillis() - 604800000L );
		Date curDate = new Date( System.currentTimeMillis() );
		List<Tweet> list = getTweetApi().getRecentTweets(beforeDate,curDate);
		Page<Tweet> popularTweetsByGame = new PageImpl<Tweet>(list);
		return decoratTweet(popularTweetsByGame, page);
	}
	
	//used in class inner
	protected Map decoratTweet(Page<Tweet> tweetsOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Iterator<Tweet> it = tweetsOfPage.iterator();
		Tweet tweet; 
		Map resultOne;
		while(it.hasNext()){
			tweet = it.next();
			String s = String.valueOf(tweet.getId());
			resultOne = new HashMap<String, Object>();
			resultOne.put("id",s);
			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", tweetsOfPage.hasNextPage());
		
		return dataJson;
	}
	
	protected Map decoratCountGame(Long count){
		Map dataJson = new HashMap<String, Object>();
		
		dataJson.put("GameCount", count);
		dataJson.put("status", 1);
		
		return dataJson;
	}
	
	protected Map decoratIGameSummary(Page<IGameSummary> RecommendedGamesByGame, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Iterator<IGameSummary> it = RecommendedGamesByGame.iterator();
		IGameSummary gameSummary; 
		Map resultOne;
		while(it.hasNext()){
			gameSummary = it.next();
			resultOne = new HashMap<String, Object>();
			resultOne.put("id", gameSummary.getId());
			resultOne.put("score", gameSummary.getSentimentScore());
            resultOne.put("name", gameSummary.getTitle());
            resultOne.put("tweetsCount", gameSummary.getTweetCount());
            resultOne.put("dvdImagePath", gameSummary.getDvdImagePath());
            result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", RecommendedGamesByGame.hasNextPage());
		
		return dataJson;
    }
}