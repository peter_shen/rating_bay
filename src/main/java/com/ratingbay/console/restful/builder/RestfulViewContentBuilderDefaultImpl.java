package com.ratingbay.console.restful.builder;

import java.util.*;

import com.ratingbay.console.utils.*;

public class RestfulViewContentBuilderDefaultImpl implements IRestfulViewContentBuilder{

    public Map<String, ?> model;
    public void setModel(Map<String, ?> model){
        this.model = model;
    }

    public String getFinalViewContent(String restfulViewContent){
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("content", restfulViewContent);
        map.put("status", true);
        map.put("hasNext", false);

        return JSonUtil.toJSon(map);
    }

    public String getContentType(){
    	return "application/json;charset=UTF-8";
    }
}