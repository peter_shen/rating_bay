package com.ratingbay.console.restful.builder;

import java.util.*;

public interface IRestfulViewContentBuilder  {
	public void setModel(Map<String, ?> model);
    public String getFinalViewContent(String restfulViewContent);
    public String getContentType();
}
