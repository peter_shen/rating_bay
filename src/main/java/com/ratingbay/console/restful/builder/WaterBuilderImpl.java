package com.ratingbay.console.restful.builder;

import java.util.*;
import org.springframework.ui.ModelMap;
import com.ratingbay.console.utils.*;

public class WaterBuilderImpl implements IRestfulViewContentBuilder{
    public Map<String, ?> model;
    public void setModel(Map<String, ?> model){
        this.model = model;
    }
    
    public String getFinalViewContent(String restfulViewContent){
        Map<String, Object> retMap = (Map<String, Object>)model.get("retMap");
        retMap.put("content", restfulViewContent);
        retMap.put("status", true);

        return JSonUtil.toJSon(retMap);
    }

    public String getContentType(){
    	return "application/json;charset=UTF-8";
    }
}
