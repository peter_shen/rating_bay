package com.ratingbay.console.restful;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.service.ITwitterUserService;

@Controller
@RequestMapping("/rest/link")
public class LinkRestful extends BaseRestful{

	@RequestMapping(value="/popularVideoLinks/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularVideoLinks(@PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		
		Page<Link> popularVideoLinks = getLinkApi().getPopularVideoLinks(beforeDate, curDate, page, 10);
		return decoratLinkAssocatUser(popularVideoLinks, page);
	}
//game video
	@RequestMapping(value="/popularVideoLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularVideoLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		Page<Link> popularVideoLinksByGame = getLinkApi().getPopularVideoLinksByGame(idGame, beforeDate, curDate, page, 8);
		return decoratLinkAssocatUser(popularVideoLinksByGame, page);
	}
//game image
	@RequestMapping(value="/popularImageLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularImageLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		Page<Link> popularImageLinksByGame = getLinkApi().getPopularImageLinksByGame(idGame, beforeDate, curDate, page, 8);
		return decoratLinkAssocatUser(popularImageLinksByGame, page);
	}
//game link
	@RequestMapping(value="/popularLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getPopularLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		Date beforeDate = new Date( System.currentTimeMillis() - 5184000000L );
		Date curDate = new Date( System.currentTimeMillis() );
		Page<Link> popularLinksByGame = getLinkApi().getPopularLinksByGame(idGame, beforeDate, curDate, page, 8);
		return decoratLinkAssocatUser(popularLinksByGame, page);
	}
//game video friends
	@RequestMapping(value="/usersVideoLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUsersVideoLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
		Page<Link> usersVideoLinksByGame = getLinkApi().getUsersVideoLinksByGame(idGame, tUserIds, page, 8);
		return decoratLinkAssocatUserIn(usersVideoLinksByGame, page, tUserIds);
	}
	@RequestMapping(value="/userVideoLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUserVideoLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = new ArrayList<Long>();
		tUserIds.add(userId);
	    
		Page<Link> userVideoLinksByGame = getLinkApi().getUsersVideoLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUser(userVideoLinksByGame, page);
	}
	@RequestMapping(value="/friendsVideoLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getFriendsVideoLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds(userId);
		
		Page<Link> friendsVideoLinksByGame = getLinkApi().getUsersVideoLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUserIn(friendsVideoLinksByGame, page, tUserIds);
	}
//game image friends
	@RequestMapping(value="/usersImageLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUsersImageLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
		Page<Link> usersImageLinksByGame = getLinkApi().getUsersImageLinksByGame(idGame, tUserIds, page, 8);
		return decoratLinkAssocatUserIn(usersImageLinksByGame, page, tUserIds);
	}
	@RequestMapping(value="/userImageLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUserImageLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = new ArrayList<Long>();
		tUserIds.add(userId);
		
		Page<Link> userImageLinksByGame = getLinkApi().getUsersImageLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUser(userImageLinksByGame, page);
	}
	@RequestMapping(value="/friendsImageLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getFriendsImageLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds(userId);
		Page<Link> friendsImageLinksByGame = getLinkApi().getUsersImageLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUserIn(friendsImageLinksByGame, page, tUserIds);
	}
//game link friends
	@RequestMapping(value="/usersLinksByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUsersLinksByGame(@PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
		Page<Link> usersLinksByGame = getLinkApi().getUsersLinksByGame(idGame, tUserIds, page, 8);
		return decoratLinkAssocatUserIn(usersLinksByGame, page, tUserIds);
	}
	@RequestMapping(value="/userLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getUserLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){
		
		List<Long> tUserIds = new ArrayList<Long>();
		tUserIds.add(userId);
		
		Page<Link> userLinksByGame = getLinkApi().getUsersLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUser(userLinksByGame, page);
	}	
	@RequestMapping(value="/friendsLinksByGame/{userId}/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getFriendsLinksByGame(@PathVariable Long userId, @PathVariable Long idGame, @PathVariable int page){

		List<Long> tUserIds = getTUserApi().getTwitterFriendsIds(userId);
		
		Page<Link> friendsLinksByGame = getLinkApi().getUsersLinksByGame(idGame, tUserIds, page, 10);
		return decoratLinkAssocatUserIn(friendsLinksByGame, page, tUserIds);
	}

	@RequestMapping(value="/UserNameAndImageByLinkId/{linkId}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getLinkUserNameAndImages(@PathVariable Long linkId){
		return decoratUserNameAndImage(linkId);
	}

	//used in class inner
	protected Map decoratUserNameAndImage(Long linkId){
		Map dataJson = new HashMap<String, Object>();
		
		List<Object> users;
		List<Tweet> tweets;
		HashMap<String, Object> userOne;
		users = new ArrayList<Object>();
		Long s1 = System.currentTimeMillis();
		tweets = getTweetApi().getTweetsByLink(linkId, 0, 2);
		Set<Long> us=new HashSet<Long>();
		for(Tweet t: tweets){
			//排重
			us.add(t.getUserId());
		}
		for(Long userId:us){
			userOne = new HashMap<String, Object>();
			userOne.put("userId", userId);
			Long s2 = System.currentTimeMillis();
			ITwitterUser u=twitterUserService.findUserNameByUserId(userId);
			userOne.put("profileImgUrl", u.getProfileImageURL());
			userOne.put("username", u.getUsername());
			users.add(userOne);
		}
		dataJson.put("users", users);
		dataJson.put("status", 1);
		
		return dataJson;
	}
	
	protected Map decoratLink(Page<Link> linksOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Link link;
		Iterator<Link> it = linksOfPage.iterator();
		Map resultOne;
		while(it.hasNext()){
			link = it.next();
			resultOne = new HashMap<String, Object>();
			resultOne.put("link", link.getLink());
			resultOne.put("mediaDirectLink", link.getMediaDirectLink());
			resultOne.put("gameId", link.getIdGame());
			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", linksOfPage.hasNextPage());
		
		return dataJson;
	}
	
	protected Map decoratLinkAssocatUserajax(Page<Link> linksOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();
		Iterator<Link> it = linksOfPage.iterator();
		Map resultOne;

		Link link;
		while(it.hasNext()){
			link = it.next();
			resultOne = new HashMap<String, Object>();
			resultOne.put("linkId", link.getId());
			resultOne.put("link", link.getLink());
			resultOne.put("mediaDirectLink", link.getMediaDirectLink());
			resultOne.put("gameId", link.getIdGame());
			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", linksOfPage.hasNextPage());
		return dataJson;
	}
	
	protected Map decoratLinkAssocatUser(Page<Link> linksOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Link link;
		Iterator<Link> it = linksOfPage.iterator();		 
		Map resultOne;

		List<Tweet> tweets;
		HashMap<String, Object> userOne;
		List<Object> users;

		while(it.hasNext()){
			link = it.next();
			resultOne = new HashMap<String, Object>();
			resultOne.put("link", link.getLink());
			resultOne.put("mediaDirectLink", link.getMediaDirectLink());

			users= new ArrayList<Object>();
			tweets = getTweetApi().getTweetsByLink(link.getId(), 0, 2);
			Set<Long> us=new HashSet<Long>();
			for(Tweet t: tweets){
				//排重
				us.add(t.getUserId());
			}
			for(Long userId:us){
				userOne = new HashMap<String, Object>();
				userOne.put("userId", userId);
				ITwitterUser u=twitterUserService.findUserNameByUserId(userId);
				userOne.put("profileImgUrl", u.getProfileImageURL());
				userOne.put("username", u.getUsername());
				users.add(userOne);
			}
			resultOne.put("users", users);

			resultOne.put("gameId", link.getIdGame());
			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", linksOfPage.hasNextPage());
		
		return dataJson;
	}
	
	protected Map decoratLinkAssocatUserIn(Page<Link> linksOfPage, int page, List<Long> tUserIds){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Link link;
		Iterator<Link> it = linksOfPage.iterator();		 
		Map resultOne;

		List<Tweet> tweets;
		HashMap<String, Object> userOne;
		List<Object> users;

		while(it.hasNext()){
			link = it.next();
			resultOne = new HashMap<String, Object>();
			resultOne.put("link", link.getLink());
			resultOne.put("mediaDirectLink", link.getMediaDirectLink());

			users= new ArrayList<Object>();
			tweets = getTweetApi().getTweetsByLinkAndUsers(link.getId(), tUserIds, 0, 2);
			Set<Long> us=new HashSet<Long>();
			for(Tweet t: tweets){
				//排重
				us.add(t.getUserId());
			}
			for(Long userId:us){
				userOne = new HashMap<String, Object>();
				userOne.put("userId", userId);
				ITwitterUser u=twitterUserService.findUserNameByUserId(userId);
				userOne.put("profileImgUrl", u.getProfileImageURL());
				userOne.put("username", u.getUsername());
				users.add(userOne);
			}
			resultOne.put("users", users);

			resultOne.put("gameId", link.getIdGame());
			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", linksOfPage.hasNextPage());
		
		return dataJson;
	}
	@Autowired
	private ITwitterUserService twitterUserService;
}