package com.ratingbay.console.restful;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

@Controller
@RequestMapping("/rest/influential")
public class InfluentialRestful extends BaseRestful{

	@RequestMapping(value="/topPlayer/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getTopPlayerByGame(@PathVariable int page){
		Page<InfluentialUser> topPlayers= getIUserApi().getTopInfluentialUsers(page, 5);
		return decoratInfluentialUser(topPlayers, page);
	}

	@RequestMapping(value="/topPlayerByGame/{idGame}/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getTopPlayerByGame(@PathVariable Long idGame, @PathVariable int page){
		Page<InfluentialUser> topPlayersByGame = getIUserApi().getTopPlayersByGame(idGame, page, 5);
		return decoratInfluentialUser(topPlayersByGame, page);
	}


	//used in class inner
	protected Map decoratInfluentialUser(Page<InfluentialUser> iusersOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Iterator<InfluentialUser> it = iusersOfPage.iterator();
		InfluentialUser iUser; 
		Map resultOne;
		while(it.hasNext()){
			iUser = it.next();

			resultOne = new HashMap<String, Object>();
			resultOne.put("username", iUser.getUsername());
			resultOne.put("followersCount", iUser.getUserFollowersCount());
			resultOne.put("friendsCount", iUser.getUserFriendsCount());
			resultOne.put("tweetsCount", iUser.getUserTweetsCount());
			resultOne.put("profileImgUrl", iUser.getUserProfileImgUrl());
			resultOne.put("twitterUserId", iUser.getTwitterUserId());
			resultOne.put("userLocation", iUser.getUserLocation());
			resultOne.put("userDescription", iUser.getUserDescription());

			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", iusersOfPage.hasNextPage());
		
		return dataJson;
	}
}