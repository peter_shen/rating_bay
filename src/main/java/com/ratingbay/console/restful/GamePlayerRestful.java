package com.ratingbay.console.restful;

import java.util.*;
import java.io.*;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

@Controller
@RequestMapping("/rest/gameplayer")
public class GamePlayerRestful extends BaseRestful{

	@RequestMapping(value="/recentFriends/{page}", method=RequestMethod.GET)
	public @ResponseBody 
	Map getRecentActiveGamers(@PathVariable int page){
		List<Long> ftUserIds = getTUserApi().getTwitterFriendsIds();
		Page<IGamePlayer> recentFriends = getTweetApi().getGamersByUserIds(ftUserIds, page , 5);
		return decoratGamePlayer(recentFriends, page);
	}


	//used in class inner
	protected Map decoratGamePlayer(Page<IGamePlayer> gamePlayersOfPage, int page){
		Map dataJson = new HashMap<String, Object>();
		List result = new ArrayList<Map>();

		Iterator<IGamePlayer> it = gamePlayersOfPage.iterator();
		IGamePlayer gameplayer; 
		Map resultOne;
		while(it.hasNext()){
			gameplayer = it.next();
			
			resultOne = new HashMap<String, Object>();
			resultOne.put("userId", gameplayer.getUserId());
			resultOne.put("username", gameplayer.getUsername());
			resultOne.put("gameCount", gameplayer.getGameCount());
			resultOne.put("profileImageURL", gameplayer.getProfileImageURL());

			result.add(resultOne);
		}
		
		dataJson.put("result", result);
		dataJson.put("status", 1);
		dataJson.put("page", page);
		dataJson.put("hasNext", gamePlayersOfPage.hasNextPage());
		
		return dataJson;
	}
}