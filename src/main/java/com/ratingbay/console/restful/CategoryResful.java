package com.ratingbay.console.restful;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;




import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ratingbay.console.dataface.IGameSummary;
import com.ratingbay.console.dataface.ITwitterUser;
import com.ratingbay.console.model.Tweet;
import com.ratingbay.console.restful.builder.WaterBuilderImpl;
@Controller
@RequestMapping("/rest/category")
public class CategoryResful extends BaseRestful{

	 @RequestMapping(value="/getGameByGenre/{genre}/{page}", method=RequestMethod.GET) 
	    public 
	    ModelAndView getGameByGenre(@PathVariable Long genre, @PathVariable int page){
	        Page<IGameSummary> searchGames = getGameApi().getGamesByGenre(genre, page, 5);
	        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
	        Set<Long> ids=new HashSet<Long>(tUserIds);
	        List<Long> userIds=new ArrayList<Long>(ids);
	        if(userIds.size() > 1){
	            Iterator<IGameSummary> it = searchGames.iterator();
	            IGameSummary gameSummary;
	            Page<Tweet> tweetsOfPage;
	            Page<Tweet> tweetsOfPageForUserId;
	            while(it.hasNext()){
	                gameSummary = it.next();
	                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                		userIds, 0, 5);
	                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                        tUserIds, 0, 100);
	                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
	                //查询用户
	                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
	            	Set<Long> uids=new HashSet<Long>();
	                for(Tweet t:tweetsOfPageForUserId.getContent()){
	                	if(uids.size() < 5){
	                	uids.add(t.getUserId());
	                	}
	                }
	                for(Long id:uids){
	                	ITwitterUser u=getTUserApi().getITwitterUser(id);
	                	ll.add(u);
	                }
	                gameSummary.setTwitterUsers(ll);
	            }  
	        }
	        return decoratIGameSummary(searchGames,page);
	    }
	 
	 @RequestMapping(value="/getGameByGenre/{genre}/{f}/{d}/{page}", method=RequestMethod.GET) 
	    public 
	    ModelAndView getGameByGenre(@PathVariable Long genre, @PathVariable String f, @PathVariable String d, @PathVariable int page){
	        Page<IGameSummary> searchGames = getGameApi().getGamesByGenre(genre, f, d, page, 5);
	        
	        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
	        Set<Long> ids=new HashSet<Long>(tUserIds);
	        List<Long> userIds=new ArrayList<Long>(ids);
	        if(userIds.size() > 1){
	            Iterator<IGameSummary> it = searchGames.iterator();
	            IGameSummary gameSummary;
	            Page<Tweet> tweetsOfPage;
	            Page<Tweet> tweetsOfPageForUserId;
	            while(it.hasNext()){
	                gameSummary = it.next();
	                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                		userIds, 0, 5);
	                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                        tUserIds, 0, 100);
	                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
	                //查询用户
	                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
	            	Set<Long> uids=new HashSet<Long>();
	                for(Tweet t:tweetsOfPageForUserId.getContent()){
	                	if(uids.size() < 5){
	                	uids.add(t.getUserId());
	                	}
	                }
	                for(Long id:uids){
	                	ITwitterUser u=getTUserApi().getITwitterUser(id);
	                	ll.add(u);
	                }
	                gameSummary.setTwitterUsers(ll);
	            }  
	        }
	        return decoratIGameSummary(searchGames,page);
	 }
	 
	 @RequestMapping(value="/getGameByPlatform/{platform}/{page}", method=RequestMethod.GET) 
	    public 
	    ModelAndView getGameByPlatform(@PathVariable String platform, @PathVariable int page){
	        Page<IGameSummary> searchGames = getGameApi().getGamesByPlatform(platform, page, 5);
	        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
	        Set<Long> ids=new HashSet<Long>(tUserIds);
	        List<Long> userIds=new ArrayList<Long>(ids);
	        if(userIds.size() > 1){
	            Iterator<IGameSummary> it = searchGames.iterator();
	            IGameSummary gameSummary;
	            Page<Tweet> tweetsOfPage;
	            Page<Tweet> tweetsOfPageForUserId;
	            while(it.hasNext()){
	                gameSummary = it.next();
	                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                		userIds, 0, 5);
	                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                        tUserIds, 0, 100);
	                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
	                //查询用户
	                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
	            	Set<Long> uids=new HashSet<Long>();
	                for(Tweet t:tweetsOfPageForUserId.getContent()){
	                	if(uids.size() < 5){
	                	uids.add(t.getUserId());
	                	}
	                }
	                for(Long id:uids){
	                	ITwitterUser u=getTUserApi().getITwitterUser(id);
	                	ll.add(u);
	                }
	                gameSummary.setTwitterUsers(ll);
	            }  
	        }
	        return decoratIGameSummary(searchGames,page);
	    }
	 
	 @RequestMapping(value="/getGameByPlatform/{platfrom}/{f}/{d}/{page}", method=RequestMethod.GET) 
	    public 
	    ModelAndView getGameByGenre(@PathVariable String platfrom,@PathVariable String f, @PathVariable String d, @PathVariable int page){
	        Page<IGameSummary> searchGames = getGameApi().getGamesByPlatform(platfrom,f,d, page, 5);
	        List<Long> tUserIds = getTUserApi().getTwitterFriendsIds();
	        Set<Long> ids=new HashSet<Long>(tUserIds);
	        List<Long> userIds=new ArrayList<Long>(ids);
	        if(userIds.size() > 1){
	            Iterator<IGameSummary> it = searchGames.iterator();
	            IGameSummary gameSummary;
	            Page<Tweet> tweetsOfPage;
	            Page<Tweet> tweetsOfPageForUserId;
	            while(it.hasNext()){
	                gameSummary = it.next();
	                tweetsOfPage = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                		userIds, 0, 5);
	                tweetsOfPageForUserId = getTweetApi().getUsersTweetsByGame(gameSummary.getId(),
	                        tUserIds, 0, 100);
	                gameSummary.setFriendsTweets(tweetsOfPage.getContent());
	                //查询用户
	                List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
	            	Set<Long> uids=new HashSet<Long>();
	                for(Tweet t:tweetsOfPageForUserId.getContent()){
	                	if(uids.size() < 5){
	                	uids.add(t.getUserId());
	                	}
	                }
	                for(Long id:uids){
	                	ITwitterUser u=getTUserApi().getITwitterUser(id);
	                	ll.add(u);
	                }
	                gameSummary.setTwitterUsers(ll);
	            }  
	        }
	        return decoratIGameSummary(searchGames,page);
	    }
	 
	 protected ModelAndView decoratIGameSummary(Page<IGameSummary> searchGames, int page){
	        Map dataJson = new HashMap<String, Object>();
	        
	        List gameSummary = new ArrayList<Map>();
	        List twitterUser = new ArrayList<Map>();
	        Map gameOne;
	        Map tweet;
	        Iterator<IGameSummary> it = searchGames.iterator();
	        while(it.hasNext()){
	        	IGameSummary g;
	        	g=it.next();
	        	gameOne = new HashMap<String, Object>();
	        	tweet = new HashMap<String, Object>();
	        	gameOne.put("id", g.getId());
	        	gameOne.put("dvdImagePath", g.getDvdImagePath());
	        	gameOne.put("sentimentScore", g.getSentimentScore());
	        	gameOne.put("tweetCount", g.getTweetCount());
	        	gameOne.put("title", g.getTitle());
	        	gameOne.put("releaseDate", g.getReleaseDate());
	        	gameOne.put("description", g.getDescription());
	        	
	        	Long id = g.getId();
	        	List<String> genre = getGameApi().getGenreById(id);
	        	List<String> platform = getGameApi().getPlatformById(id);
	        	gameOne.put("genre", genre);
	        	gameOne.put("platform",platform);
	        	
	        	List<ITwitterUser> ll=new ArrayList<ITwitterUser>();
	        	ll = g.getTwitterUsers();
	        	if(ll!=null){
	        	Iterator<ITwitterUser> itw = ll.iterator();
	        	while(itw.hasNext()){
	        		String s = String.valueOf(itw.next().getId());
	        		tweet.put("id", s);
	        		tweet.put("username", itw.next().getUsername());
	        		tweet.put("profileImageURL", itw.next().getProfileImageURL());
	        		twitterUser.add(tweet);
	        	}}
	        	gameOne.put("twitterUser", twitterUser);
	        	gameSummary.add(gameOne);
	        }
	        
	        ModelMap model = new ModelMap();
	        model.addAttribute( "gameCategory", gameSummary);
	        model.addAttribute( "IRestfulViewContentBuilder", 
	            new WaterBuilderImpl() );

	        Map<String, Object> retMap = new HashMap();
	        retMap.put( "hasNext", searchGames.hasNextPage());
	        model.addAttribute( "retMap", retMap );

	        return new ModelAndView("restful/categoryWater", model);
	    }
}
