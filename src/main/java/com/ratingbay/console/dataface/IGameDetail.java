package com.ratingbay.console.dataface;

import java.util.Date;
import java.util.Set;

import com.ratingbay.console.model.*;

public interface IGameDetail {

	public Long getId();

	public Date getActiveDate();

	public String getVideoId();
	public void setVideoId(String videoId);

	public Long getTweetCount() ;
	public void setTweetCount(Long tweetCount);

	public Long getScore();

	public String getDescription();

	public String getDvdImagePath();

	public String getHeadlineImagePath();

	public Date getInactiveDate();

	public Boolean getIsHeadline();

	public String getPublisher();

	public Date getReleaseDate();

	public Integer getStatus();

	public String getTitle();

	public String getVideoTrailerLink();

	public Set<GameGenre> getGameGenres();

	public Set<GameName> getGameNames();

	public Set<GamePlatform> getGamePlatforms();

	public Set<SentimentScoreHistory> getSentimentScoreHistories();

	public Set<TrackingKeyword> getTrackingKeywords();

	public Set<Word> getWords();

	public Set<IHashTagSummary> getHashTags();
	public void setHashTags(Set<IHashTagSummary> hashTags);
}
