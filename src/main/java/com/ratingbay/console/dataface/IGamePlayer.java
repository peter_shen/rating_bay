package com.ratingbay.console.dataface;

import org.springframework.social.twitter.api.TwitterProfile;

public interface IGamePlayer {

	public Long getUserId();

	public String getUsername();

	public String getProfileImageURL();

	public Long getGameCount();
}
