package com.ratingbay.console.dataface;

import java.util.Date;
import java.util.Set;

import com.ratingbay.console.model.*;

public interface IGameInfoBase {

	public Long getId() ;

	public Date getActiveDate();

	public String getDescription();

	public String getDvdImagePath() ;

	public String getHeadlineImagePath();
	
	public Date getInactiveDate();

	public Boolean getIsHeadline() ;

	public String getPublisher() ;

	public Date getReleaseDate() ;

	public Integer getStatus() ;

 	public String getVideoTrailerLink() ;

 	public String getTitle();
}
