package com.ratingbay.console.dataface;

import org.springframework.social.twitter.api.TwitterProfile;

public interface ITwitterUser {

	public Long getId();

	public String getUsername();

	public String getProfileImageURL();

	public String getDescription();

	public String getLocation();

	public Long getFollowersCount();

	public Long getFriendsCount();

	public Long getTweetsCount();

	public Long getGameCount();
}
