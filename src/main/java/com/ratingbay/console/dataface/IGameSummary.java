package com.ratingbay.console.dataface;

import java.util.*;

import com.ratingbay.console.model.Tweet;

public interface IGameSummary extends IGameInfoBase {

	public Long getSentimentScore();
	
	public Long getTweetCount();
	
	public Tweet getMostPositiveTweet();
	
	public Tweet getMostNegativeTweet();
	
	public Tweet getMostRetweetTweet();

	public List<Tweet> getFriendsTweets();
	public void setFriendsTweets(List<Tweet> friendsTweets);
	
	public List<ITwitterUser> getTwitterUsers();
	public void setTwitterUsers(List<ITwitterUser> twitterUsers);
	
	public List<String> getGenre();
	public void setGenre(List<String> genre);
    public List<String> getPlatform();
    public void setPlatform(List<String> platform);
}
