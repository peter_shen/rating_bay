package com.ratingbay.console.dataface;

import java.util.Date;
import java.util.Set;

import com.ratingbay.console.model.*;

public interface IHashTagSummary {
	public String getText();
	
	public Long getCount();
}
