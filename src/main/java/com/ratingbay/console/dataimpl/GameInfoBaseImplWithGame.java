package com.ratingbay.console.dataimpl;

import java.util.Date;
import java.util.Set;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

public class GameInfoBaseImplWithGame implements IGameInfoBase{

	private Game game;
	protected GameInfoBaseImplWithGame(Game game){
		this.game = game;
	}

	public static IGameInfoBase getIGameInfoBase(Game game){
		return new GameInfoBaseImplWithGame(game);
	}

	public Long getId(){
		return game.getId();
	}

	public Date getActiveDate(){
		return game.getActiveDate();
	}

	public String getDescription(){
		return game.getDescription();
	}

	public String getDvdImagePath(){
		return game.getDvdImagePath();
	}

	public String getHeadlineImagePath(){
		return game.getHeadlineImagePath();
	}
	
	public Date getInactiveDate(){
		return game.getInactiveDate();
	}

	public Boolean getIsHeadline(){
		return game.getIsHeadline();
	}

	public String getPublisher(){
		return game.getPublisher();
	}

	public Date getReleaseDate(){
		return game.getReleaseDate();
	}

	public Integer getStatus(){
		return game.getStatus();
	}

	public String getTitle(){
		return game.getTitle();
	}

	public String getVideoTrailerLink(){
		return game.getVideoTrailerLink();
	}

}