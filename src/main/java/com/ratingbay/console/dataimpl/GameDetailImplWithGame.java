package com.ratingbay.console.dataimpl;

import java.util.*;
import java.math.*;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.utils.*;

public class GameDetailImplWithGame implements IGameDetail{

	private Game game;
	protected GameDetailImplWithGame(Game game){
		this.game = game;
	}

	public static IGameDetail getIGameDetail(Game game){
		return new GameDetailImplWithGame(game);
	}

	public Long getId(){
		return game.getId();
	}

	public Date getActiveDate(){
		return game.getActiveDate();
	}

	public String getDescription(){
		return game.getDescription();
	}

	public String getDvdImagePath(){
		return game.getDvdImagePath();
	}

	public String getHeadlineImagePath(){
		return game.getHeadlineImagePath();
	}
	
	public Date getInactiveDate(){
		return game.getInactiveDate();
	}

	public Boolean getIsHeadline(){
		return game.getIsHeadline();
	}

	public String getPublisher(){
		return game.getPublisher();
	}

	public Date getReleaseDate(){
		return game.getReleaseDate();
	}

	public Integer getStatus(){
		return game.getStatus();
	}

	public String getTitle(){
		return game.getTitle();
	}

	public String getVideoTrailerLink(){
		return game.getVideoTrailerLink();
	}

	public Set<GameGenre> getGameGenres(){
		return game.getGameGenres();
	}

	public Set<GameName> getGameNames(){
		return game.getGameNames();
	}

	public Set<GamePlatform> getGamePlatforms(){
		return game.getGamePlatforms();
	}

	public Set<SentimentScoreHistory> getSentimentScoreHistories(){
		return game.getSentimentScoreHistories();
	}

	public Set<TrackingKeyword> getTrackingKeywords(){
		return game.getTrackingKeywords();
	}

	public Set<Word> getWords(){
		return game.getWords();
	}

	String videoId = null;
	public String getVideoId(){
		if(null == videoId){
			videoId = Helper.getVideoId(getVideoTrailerLink());
		}
		return videoId;
	}
	public void setVideoId(String videoId){
		this.videoId = videoId;
	}

	Long tweetCount = null;
	public Long getTweetCount(){
		if(null == tweetCount){
			tweetCount = 0L;
		}
		return tweetCount;
	}
	public void setTweetCount(Long tweetCount){
		this.tweetCount = tweetCount;
	}

	Long lscore = null;
	public Long getScore(){
		/*
		if(null == lscore){
			Set<SentimentScoreHistory> sentimentScoreHistories = getSentimentScoreHistories();
			if(sentimentScoreHistories.isEmpty()){
				lscore = new Long(0);
			}else{
				BigDecimal bdScore = null;
				long ts = -1;
				for (SentimentScoreHistory sentimentScoreHistory : sentimentScoreHistories) {  
			    	if(null == bdScore){
			    		bdScore = sentimentScoreHistory.getScore();
			    		ts = sentimentScoreHistory.getInsertTime().getTime();
			    		continue;
			    	}else{
			    		if(sentimentScoreHistory.getInsertTime().getTime() > ts){
			    			bdScore = sentimentScoreHistory.getScore();
			    		}
			    	}
				}
				lscore = (long) (bdScore.floatValue() * 100);
			}
		}*/
		return game.getScore()==null?0l:Math.round(game.getScore()*100);
	}
	
    Set<IHashTagSummary> hashTags;
	public Set<IHashTagSummary> getHashTags(){
		return this.hashTags;
	}
	public void setHashTags(Set<IHashTagSummary> hashTags){
		this.hashTags = hashTags;
	}
}