package com.ratingbay.console.dataimpl;

import java.util.*;

import org.springframework.social.twitter.api.TwitterProfile;

import com.ratingbay.console.dataface.ITwitterUser;

public class TwitterUserImplWithTwitterProfile implements ITwitterUser {
	private TwitterProfile twitterProfile;
	private TwitterUserImplWithTwitterProfile(TwitterProfile twitterProfile){
		this.twitterProfile = twitterProfile;
	}

	public static ITwitterUser getITwitterUser(TwitterProfile twitterProfile){
		return new TwitterUserImplWithTwitterProfile(twitterProfile);
	}

	public static List<ITwitterUser> getITwitterUsers(List<TwitterProfile> twitterProfileList){

        List<ITwitterUser> iTwitterUsers= new ArrayList<ITwitterUser>();

        Iterator<TwitterProfile> it= twitterProfileList.iterator();
		while(it.hasNext()){
		    iTwitterUsers.add( getITwitterUser(it.next()) );
		}
        return iTwitterUsers;
	}

	public Long getId(){
		return this.twitterProfile.getId();
	}

	public String getUsername(){
		return this.twitterProfile.getScreenName();
	}

	public String getProfileImageURL(){
		return this.twitterProfile.getProfileImageUrl();
	}

	public String getDescription(){
		return this.twitterProfile.getDescription();
	}

	public String getLocation(){
		return this.twitterProfile.getLocation();
	}

	public Long getFollowersCount(){
		return new Long( (long) this.twitterProfile.getFollowersCount() );
	}

	public Long getFriendsCount(){
		return new Long( (long) this.twitterProfile.getFriendsCount() );
	}

	public Long getTweetsCount(){
		return new Long( (long) this.twitterProfile.getStatusesCount() );
	}

	Long gameCount;
	public Long getGameCount(){
		return gameCount;
	}
	public void setGameCount(Long gameCount){
		this.gameCount = gameCount;
	}
}