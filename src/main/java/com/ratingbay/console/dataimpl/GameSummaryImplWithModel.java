package com.ratingbay.console.dataimpl;

import java.util.*;
import java.math.*;

import com.ratingbay.console.dataface.IGameSummary;
import com.ratingbay.console.dataface.ITwitterUser;
import com.ratingbay.console.model.*;

public class GameSummaryImplWithModel extends GameInfoBaseImplWithGame  implements IGameSummary{

	private GameSummary gameSummary;
	protected GameSummaryImplWithModel(GameSummary gameSummary){
		super(gameSummary.getGame());
		this.gameSummary = gameSummary;
	}

	public static IGameSummary getIGameSummary(GameSummary gameSummary){
		return new GameSummaryImplWithModel(gameSummary);
	}

	public static List<IGameSummary> getIGameSummarys(List<GameSummary> gameSummary){
		List<IGameSummary> iGameSummaryList= new ArrayList<IGameSummary>();
		Iterator<GameSummary> it= gameSummary.iterator();
		while(it.hasNext()){
            iGameSummaryList.add( getIGameSummary(it.next()) );
        }
		return iGameSummaryList;
	}

	public Long getSentimentScore(){
		BigDecimal bdScore = gameSummary.getSentimentScore();
		if(null == bdScore){
			return 0L;
		}
		return new Long( (long)(bdScore.floatValue() * 100) );
	}
	
	public Long getTweetCount(){
		return gameSummary.getTweetCount();
	}

	public Tweet getMostPositiveTweet(){
		return gameSummary.getMostPositiveTweet();
	}
	
	public Tweet getMostNegativeTweet(){
		return gameSummary.getMostNegativeTweet();
	}
	
	public Tweet getMostRetweetTweet(){
		return gameSummary.getMostRetweetTweet();
	}

	List<Tweet> friendsTweets;
	public List<Tweet> getFriendsTweets(){
		return friendsTweets;
	}
	public void setFriendsTweets(List<Tweet> friendsTweets){
		this.friendsTweets = friendsTweets;
	}
	
	List<ITwitterUser> twitterUsers;
	public List<ITwitterUser> getTwitterUsers(){
		return twitterUsers;
	}
	public void setTwitterUsers(List<ITwitterUser> twitterUsers){
		this.twitterUsers = twitterUsers;
	}
	
	List<String> genre;
	public List<String> getGenre(){
		return genre;
	}
	public void setGenre(List<String> genre){
		this.genre = genre;
	}
	
	List<String> platform;
	public List<String> getPlatform(){
		return platform;
	}
	public void setPlatform(List<String> platform){
		this.platform = platform;
	}
   
    
	
}