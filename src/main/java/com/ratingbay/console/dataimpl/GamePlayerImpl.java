package com.ratingbay.console.dataimpl;

import java.util.*;
import java.math.*;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.utils.*;

public class GamePlayerImpl implements IGamePlayer{

    Long userId;
    @Override
	public Long getUserId(){
		return userId;
	}
	public void setUserId(Long userId){
		this.userId = userId;
	}

    String username;
    @Override
	public String getUsername(){
		return username;
	}
	public void setUsername(String username){
		this.username = username;
	}

	String profileImageURL;
	@Override
	public String getProfileImageURL(){
		return profileImageURL;
	}
	public void setProfileImageURL(String profileImageURL){
		this.profileImageURL = profileImageURL;
	}

	Long gameCount;
	@Override
	public Long getGameCount(){
		return gameCount;
	}
	public void setGameCount(Long gameCount){
		this.gameCount = gameCount;
	}
}