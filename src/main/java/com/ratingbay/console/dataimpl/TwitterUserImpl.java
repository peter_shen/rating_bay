package com.ratingbay.console.dataimpl;

import org.springframework.social.twitter.api.TwitterProfile;

import com.ratingbay.console.dataface.ITwitterUser;

public class TwitterUserImpl implements ITwitterUser {
	
	private Long id = null;
	public Long getId(){
		return this.id;
	}
	public void setId(Long id){
		this.id = id;
	}

	public String username = null;
	public String getUsername(){
		return this.username;
	}
	public void setUsername(String username){
		this.username = username;
	}

	public String userProImg = null;
	public String getProfileImageURL(){
		return this.userProImg;
	}
	public void setProfileImageURL(String userProImg){
		this.userProImg = userProImg;
	}

	public String description = null;
	public String getDescription(){
		return this.description;
	}
	public void setDescription(String description){
		this.description = description;
	}

	public String location = null;
	public String getLocation(){
		return this.location;
	}
	public void setLocation(String location){
		this.location = location;
	}

	public Long followersCount = null;
	public Long getFollowersCount(){
		return this.followersCount;
	}
	public void setFollowersCount(Long followersCount){
		this.followersCount = followersCount;
	}

	public Long friendsCount = null;
	public Long getFriendsCount(){
		return this.friendsCount;
	}
	public void setFriendsCount(Long friendsCount){
		this.friendsCount = friendsCount;
	}

	public Long tweetsCount = null;
	public Long getTweetsCount(){
		return this.tweetsCount;
	}
	public void setTweetsCount(Long tweetsCount){
		this.tweetsCount = tweetsCount;
	}

	public Long gameCount = null;
	public Long getGameCount(){
		return gameCount;
	}
	public void setGameCount(Long gameCount){
		this.gameCount = gameCount;
	}
}
