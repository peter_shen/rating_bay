package com.ratingbay.console.dataimpl;

import java.util.*;
import java.math.*;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.utils.*;

public class HashTagSummaryImpl implements IHashTagSummary{

	String text;
	@Override
	public String getText(){
		return this.text;
	}
	public void setText(String text){
		this.text = text;
	}
	
	Long count;
	@Override
	public Long getCount(){
		return this.count;
	}
	public void setCount(Long count){
		this.count = count;
	}
	
}