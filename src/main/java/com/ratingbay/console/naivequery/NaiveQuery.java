package com.ratingbay.console.naivequery;

import java.util.List;
 
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

public class NaiveQuery{
    public NaiveQuery(EntityManagerFactory emf){
        this.emf = emf;
        self = this;
    }

    private static NaiveQuery self = null;
    public static NaiveQuery getInstance(){
        return self;
    }

    private EntityManagerFactory emf = null;
    public EntityManagerFactory getEntityManagerFactory(){
        return this.emf;
    }
    
    private EntityManager entityManager = null;
    public EntityManager getEntityManager(){
        return this.entityManager;
    }
    @PersistenceContext
    public void setEntityManager(EntityManager entityManager){
        this.entityManager = entityManager;
    }
}