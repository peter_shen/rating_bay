package com.ratingbay.console.social;

import java.util.Date;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.social.connect.UserProfile;

import com.ratingbay.console.model.User;
import com.ratingbay.console.service.IUserService;

public class UserConnectionSignUp implements ConnectionSignUp {

    public String execute(Connection<?> connection) {
    
        UserProfile profile = connection.fetchUserProfile();
        String userName = profile.getUsername();
        String email = profile.getEmail();

        User user = new User();
        user.setUsername(userName);
        user.setEmail(email);
        user.setSignupTime(new Date());

        SigninHelper.getInstance().getUserService().save(user);
        String userId = user.getId();
        SigninHelper.getInstance().signin(userName, email);
        
        return userId;
    }
}
