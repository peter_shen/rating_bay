package com.ratingbay.console.databean;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

public class HomePageDataBean extends DataBean{
	public List<IGameSummary> topRatedGames;
	public List<IGameSummary> getTopRatedGames() {
		return this.topRatedGames;
	}
	public void setTopRatedGames(List<IGameSummary> topRatedGames) {
		this.topRatedGames = topRatedGames;
	}

	public List<IGameSummary> topTweetedGames;
	public List<IGameSummary> getTopTweetedGames() {
		return this.topTweetedGames;
	}
	public void setTopTweetedGames(List<IGameSummary> topTweetedGames) {
		this.topTweetedGames = topTweetedGames;
	}

	public List<IGameSummary> newReleasedGames;
	public List<IGameSummary> getNewReleasedGames() {
		return this.newReleasedGames;
	}
	public void setNewReleasedGames(List<IGameSummary> newReleasedGames) {
		this.newReleasedGames = newReleasedGames;
	}

	public List<IGameSummary> comingSoonGames;
	public List<IGameSummary> getComingSoonGames() {
		return this.comingSoonGames;
	}
	public void setComingSoonGames(List<IGameSummary> comingSoonGames) {
		this.comingSoonGames = comingSoonGames;
	}


	public Page<IGameSummary> headlineGames;
	public Page<IGameSummary> getHeadlineGames() {
		return this.headlineGames;
	}
	public void setHeadlineGames(Page<IGameSummary> headlineGames) {
		this.headlineGames = headlineGames;
	}


	public List<Tweet> recentTweets;
	public List<Tweet> getRecentTweets() {
		return this.recentTweets;
	}
	public void setRecentTweets(List<Tweet> recentTweets) {
		this.recentTweets = recentTweets;
	}

	public List<Tweet> popularTweets;
	public List<Tweet> getPopularTweets() {
		return this.popularTweets;
	}
	public void setPopularTweets(List<Tweet> popularTweets) {
		this.popularTweets = popularTweets;
	}

	public Page<Link> popularVideoLinks;
	public Page<Link> getPopularVideoLinks() {
		return this.popularVideoLinks;
	}
	public void setPopularVideoLinks(Page<Link> popularVideoLinks) {
		this.popularVideoLinks = popularVideoLinks;
	}

	public Page<InfluentialUser> topInfluencialUsers;
	public Page<InfluentialUser> getTopInfluencialUsers() {
		return this.topInfluencialUsers;
	}
	public void setTopInfluencialUsers(Page<InfluentialUser> topInfluencialUsers) {
		this.topInfluencialUsers = topInfluencialUsers;
	}

}