package com.ratingbay.console.databean;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.utils.Helper;

public class GamePageDataBean extends DataBean {
	public IGameDetail gameDetail;
	public IGameDetail getGameDetail(){
		return this.gameDetail;
	}
	public void setIGameDetail(IGameDetail gameDetail){
		this.gameDetail = gameDetail;
	}

	public Page<InfluentialUser> topPlayers;
	public Page<InfluentialUser> getTopPlayers(){
		return this.topPlayers;
	}
	public void setTopPlayers(Page<InfluentialUser> topPlayers) {
		this.topPlayers = topPlayers;
	}

	public Page<IGameSummary> topRecommendedGames;
	public Page<IGameSummary> getTopRecommendedGames() {
		return topRecommendedGames;
	}
	public void setTopRecommendedGames(Page<IGameSummary> topRecommendedGames) {
		this.topRecommendedGames = topRecommendedGames;
	}

	public Page<Tweet> latestTweets;
	public Page<Tweet> getLatestTweets() {
		return latestTweets;
	}
	public void setLatestTweets(Page<Tweet> latestTweets) {
		this.latestTweets = latestTweets;
	}

	public Page<Tweet> negativeTweets;
	public Page<Tweet> getNegativeTweets() {
		return negativeTweets;
	}
	public void setNegativeTweets(Page<Tweet> negativeTweets) {
		this.negativeTweets = negativeTweets;
	}

	public Page<Tweet> positiveTweets;
	public Page<Tweet> getPositiveTweets() {
		return positiveTweets;
	}
	public void setPositiveTweets(Page<Tweet> positiveTweets) {
		this.positiveTweets = positiveTweets;
	}

	public Page<Tweet> popularTweets;
	public Page<Tweet> getPopularTweets() {
		return popularTweets;
	}
	public void setPopularTweets(Page<Tweet> popularTweets) {
		this.popularTweets = popularTweets;
	}

	public Page<Tweet> friendsTweets;
	public Page<Tweet> getFriendsTweets() {
		return this.friendsTweets;
	}
	public void setFriendsTweets(Page<Tweet> friendsTweets) {
		this.friendsTweets = friendsTweets;
	}

	public Page<Link> popularVideoLinks;
	public Page<Link> getPopularVideoLinks() {
		return popularVideoLinks;
	}
	public void setPopularVideoLinks(Page<Link> popularVideoLinks) {
		this.popularVideoLinks = popularVideoLinks;
	}

	public Page<Link> friendsVideoLinks;
	public Page<Link> getFriendsVideoLinks() {
		return friendsVideoLinks;
	}
	public void setFriendsVideoLinks(Page<Link> friendsVideoLinks) {
		this.friendsVideoLinks = friendsVideoLinks;
	}

	public Page<Link> popularImageLinks;
	public Page<Link> getPopularImageLinks() {
		return popularImageLinks;
	}
	public void setPopularImageLinks(Page<Link> popularImageLinks) {
		this.popularImageLinks = popularImageLinks;
	}

	public Page<Link> friendsImageLinks;
	public Page<Link> getFriendsImageLinks() {
		return this.friendsImageLinks;
	}
	public void setFriendsImageLinks(Page<Link> friendsImageLinks) {
		this.friendsImageLinks = friendsImageLinks;
	}

	public Page<Link> popularLinks;
	public Page<Link> getPopularLinks() {
		return popularLinks;
	}
	public void setPopularLinks(Page<Link> popularLinks) {
		 this.popularLinks = popularLinks;
	}

	public Page<Link> friendsLinks;
	public Page<Link> getFriendsLinks() {
		return this.friendsLinks;
	}
	public void setFriendsLinks(Page<Link> friendsLinks) {
		this.friendsLinks = friendsLinks;
	}
	
}