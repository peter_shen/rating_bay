package com.ratingbay.console.databean;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.web.widgets.*;

public class SearchPageDataBean extends DataBean {
    public Page<ITwitterUser> userList;
    public Page<ITwitterUser> getUserList(){
        return this.userList;
    }
    public void setUserList(Page<ITwitterUser> userList){
        this.userList = userList;
    }

    public Page<IGameSummary> gameList;
    public Page<IGameSummary> getGameList(){
        return this.gameList;
    }
    public void setGameList(Page<IGameSummary> gameList){
        this.gameList = gameList;
    }

    public PageWidget pageWidget;
    public PageWidget getPageWidget(){
        return this.pageWidget;
    }
    public void setPageWidget(PageWidget pageWidget){
        this.pageWidget = pageWidget;
    }

    public String kw;
    public String getKw(){
        return this.kw;
    }
    public void setKw(String kw){
        this.kw = kw;
    }
    
    public String kws;
	public String getKws() {
		return kws;
	}
	public void setKws(String kws) {
		this.kws = kws;
	}
}
