package com.ratingbay.console.databean;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;

public class GameAboutDataBean extends DataBean {
	public IGameDetail gameDetail;
	public IGameDetail getGameDetail() {
		return gameDetail;
	}
	public void setGameDetail(IGameDetail gameDetail){
		this.gameDetail = gameDetail;
	}

	public Page<Tweet> latestTweets;
	public Page<Tweet> getLatestTweets() {
		return this.latestTweets;
	}
	public void setLatestTweets(Page<Tweet> latestTweets) {
		this.latestTweets = latestTweets;
	}

	public Page<Link> popularVideoLinks;
	public Page<Link> getPopularVideoLinks() {
		return this.popularVideoLinks;
	}
	public void setPopularVideoLinks(Page<Link> popularVideoLinks) {
		this.popularVideoLinks = popularVideoLinks;
	}

	public Page<Link> popularImageLinks;
	public Page<Link> getPopularImageLinks() {
		return this.popularImageLinks;
	}
	public void setPopularImageLinks(Page<Link> popularImageLinks) {
		this.popularImageLinks = popularImageLinks;
	}

	public Page<Link> popularLinks;
	public Page<Link> getPopularLinks() {
		return this.popularLinks;
	}
	public void setPopularLinks(Page<Link> popularLinks) {
		this.popularLinks = popularLinks;
	}
	
	public Long gameUserId;
	public Long getGameUserId() {
		return gameUserId;
	}
	public void setGameUserId(Long gameUserId) {
		this.gameUserId = gameUserId;
	}
	
	
}