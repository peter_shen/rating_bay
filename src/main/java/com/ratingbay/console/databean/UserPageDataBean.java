package com.ratingbay.console.databean;

import java.util.*;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.web.widgets.PageWidget;

public class UserPageDataBean extends DataBean {
	public Long userGameCount;
	public Long getUserGameCount(){
		return this.userGameCount;
	}
	public void setUserGameCount(long userGameCount){
		this.userGameCount = userGameCount;
	}

	public ITwitterUser iTwitterUser;
	public ITwitterUser getITwitterUser(){
		return iTwitterUser;
	}
	public void setITwitterUser(ITwitterUser iTwitterUser){
		this.iTwitterUser = iTwitterUser;
	}

    public Page<GameAboutDataBean> uGameAboutDataBeans;
    public Page<GameAboutDataBean> getUGameAboutDataBeans(){
    	return uGameAboutDataBeans;
    }
    public void setUGameAboutDataBeans(Page<GameAboutDataBean> uGameAboutDataBeans){
    	this.uGameAboutDataBeans = uGameAboutDataBeans;
    }

    public List<GameAboutDataBean> fGameAboutDataBeans;
    public List<GameAboutDataBean> getFGameAboutDataBeans(){
    	return fGameAboutDataBeans;
    }
    public void setFGameAboutDataBeans(List<GameAboutDataBean> fGameAboutDataBeans){
    	this.fGameAboutDataBeans = fGameAboutDataBeans;
    }

    public Page<IGamePlayer> recentActiveGamers;
    public Page<IGamePlayer> getRecentActiveGamers(){
    	return recentActiveGamers;
    }
    public void setRecentActiveGamers(Page<IGamePlayer> recentActiveGamers){
    	this.recentActiveGamers = recentActiveGamers;
    }
    
    public PageWidget pageWidget;
    public PageWidget getPageWidget(){
        return this.pageWidget;
    }
    public void setPageWidget(PageWidget pageWidget){
        this.pageWidget = pageWidget;
    } 
    
}