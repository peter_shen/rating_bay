package com.ratingbay.console.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.*;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Service;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.dataimpl.*;
import com.ratingbay.console.dao.*;
import com.ratingbay.console.model.*;
import com.ratingbay.lucene.LuceneQuery;

@Service
public class TwitterUserServiceImpl extends BaseService implements ITwitterUserService {
	//need injected objects
	public ITweetService tweetService;
	public ITweetService getTweetService() {
		return tweetService;
	}
	public void setTweetService(ITweetService tweetService) {
		this.tweetService = tweetService;
	}

	public TweetDAO tweetDao;
	public void setTweetDao(TweetDAO tweetDao){
		this.tweetDao = tweetDao;
	}
	public TweetDAO getTweetDao(){
		return this.tweetDao;
	}

	//used in class inner
	protected Long findGameCountByUser(Long tUserId){
		Long gamecount = 0L;
		gamecount = this.tweetDao.findGameCountByUser(tUserId);
		return gamecount;
	}

	@Autowired
	private TwitterUserDAO twitterUserDao;
	
	// implement interfaces
	@Override
	public ITwitterUser findUserByUserId(Long userId){
        TwitterUser t = twitterUserDao.findTwitterUserByUserId(userId);
        if(null == t)return null;
        TwitterUserImpl twitterUser = new TwitterUserImpl();
        
        twitterUser.setId(t.getUserId());
        twitterUser.setProfileImageURL(t.getUserProfileImgUrl());
        twitterUser.setUsername(t.getUsername());
        twitterUser.setLocation(t.getUserLocation());
        twitterUser.setDescription(t.getUserDescription());

        twitterUser.setFollowersCount(t.getUserFollowersCount());
        twitterUser.setFriendsCount(t.getUserFriendsCount());
        twitterUser.setTweetsCount(t.getUserTweetsCount());
        twitterUser.setGameCount(findGameCountByUser(userId));
        return twitterUser;
	}
	
	@Override
	public ITwitterUser findByUserId(Long userId){
        TwitterUser t = twitterUserDao.findTwitterUserByUserId(userId);
        if(null == t)return null;
        TwitterUserImpl twitterUser = new TwitterUserImpl();
        
        twitterUser.setId(t.getUserId());
        twitterUser.setProfileImageURL(t.getUserProfileImgUrl());
        twitterUser.setUsername(t.getUsername());
        twitterUser.setLocation(t.getUserLocation());
        twitterUser.setDescription(t.getUserDescription());

        twitterUser.setFollowersCount(t.getUserFollowersCount());
        twitterUser.setFriendsCount(t.getUserFriendsCount());
        twitterUser.setTweetsCount(t.getUserTweetsCount());
        return twitterUser;
	}
	
	@Override
	public ITwitterUser findUserNameByUserId(Long userId){
        TwitterUser t = twitterUserDao.findTwitterUserByUserId(userId);
        if(null == t)return null;
        TwitterUserImpl twitterUser = new TwitterUserImpl();
        twitterUser.setProfileImageURL(t.getUserProfileImgUrl());
        twitterUser.setUsername(t.getUsername());
        return twitterUser;
	}

	@Override
	public Page<ITwitterUser> findUsersByUserNameLike(String uName, int pageNum, int pageSize){
		Page<ITwitterUser> iUsersOfPage = new PageImpl<ITwitterUser>(new ArrayList<ITwitterUser>());
		String searchUName = uName.toLowerCase();
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		Long countUserId = LuceneQuery.searchCounts("username", searchUName);
		if(countUserId < 1)return iUsersOfPage;

		List<Long> userIdsOfPage = LuceneQuery.search("username", searchUName,pageNum,pageSize,null,null);
		Iterator<Long> it = userIdsOfPage.iterator();
		List<ITwitterUser> iUsersOfList = new ArrayList<ITwitterUser>();

		Long userIdOne;
		while(it.hasNext()){
			userIdOne = it.next();
			iUsersOfList.add( findUserByUserId( (Long) userIdOne ) );
		}
		iUsersOfPage = new PageImpl<ITwitterUser>(iUsersOfList, pageRequest, countUserId);
		return iUsersOfPage;
	}
	@Override
	public Page<ITwitterUser> findUsersByUserNameLike(String uName, String orderField, String orderDirection, int pageNum, int pageSize){
		Page<ITwitterUser> iUsersOfPage = new PageImpl<ITwitterUser>(new ArrayList<ITwitterUser>());
		String searchUName = uName.toLowerCase();
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		List<Long> userIdsOfPage = new ArrayList<Long>();
		Long counts=LuceneQuery.searchCounts("username", searchUName);
		//Long countUserId = tweetDao.findCountUserIdByUserNameLike(searchUName);
		if(counts<1)return iUsersOfPage;


		if(orderField.equals("UN")){
			
			if(orderDirection.equals("ASC")){
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeASCOrderUN(searchUName, pageNum, pageSize);
				//userIdsOfPage=LuceneQuery.search("username", uName, pageSize, pageNum, "username", false);
				
			}
			else{
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeDESCOrderUN(searchUName, pageNum, pageSize);
				//userIdsOfPage=LuceneQuery.search("username", uName, pageSize, pageNum, "username", true);
			}
		}else if(orderField.equals("GC")){
			if(orderDirection.equals("ASC")){
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeASCOrderGC(searchUName, pageNum, pageSize);
			}
			else{
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeDESCOrderGC(searchUName, pageNum, pageSize);
			}
		}else if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeASCOrderTC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "tweetsCount", false);
			}
			else{
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeDESCOrderTC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "tweetsCount", true);
			}
		}else if(orderField.equals("FINGC")){
			if(orderDirection.equals("ASC")){
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeASCOrderFINGC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "friendsCount", false);
			}
			else{
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeDESCOrderFINGC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "friendsCount", true);
			}
		}else if(orderField.equals("FEDC")){
			if(orderDirection.equals("ASC")){
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeASCOrderFEDC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "followersCount", false);
			}
			else{
				//userIdsOfPage = tweetDao.findUserIdByUserNameLikeDESCOrderFEDC(searchUName, pageNum, pageSize);
				userIdsOfPage=LuceneQuery.search("username", searchUName, pageNum, pageSize, "followersCount", true);
			}
		}

		Iterator<Long> it = userIdsOfPage.iterator();
		List<ITwitterUser> iUsersOfList = new ArrayList<ITwitterUser>();

		Long userIdOne;
		while(it.hasNext()){
			userIdOne = it.next();
			iUsersOfList.add( findUserByUserId( (Long) userIdOne ) );
		}
		iUsersOfPage = new PageImpl<ITwitterUser>(iUsersOfList, pageRequest, counts);
		return iUsersOfPage;
	}
	@Override
	public Long findUserByUserName(String uName) {
		return tweetDao.findByUsername(uName);
	}
	
	@Override
	public List<TwitterUser>findUserNameByUserNameLike(String uName,int pageNum ,int pageSize){
		String searchUname = "%" + uName + "%";
		return twitterUserDao.findTwitterUserByUserNameLike(searchUname,pageNum,pageSize);
	}
	
}
