package com.ratingbay.console.service;

import java.util.Date;
import java.util.Set;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Link;
import com.ratingbay.console.model.User;

public interface ILinkService {

	/**
	 * Find the most popular links
	 * @param type the type of the link
	 * @param from The start date
	 * @param to The end date
	 * @param pageNum
	 * @param pageSize
	 * @return The popular links between the start date and the end date
	 */
	public Page<Link> findMostPopularLinks(Integer type, Date from, Date to,
		int pageNum, int pageSize); 
	
	/**
	 * find most popular video links
	 * @param type
	 * @param from
	 * @param to
	 * @param pageNum
	 * @param pageSize
	 * @return The links order by retweet_count
	 */
	public Page<Link> findMostPopularVideoLinks(Integer type, Date from, Date to,
			int pageNum, int pageSize);
	/**
	 * Find the most popular links of specific game
	 * @param type the type of the link
	 * @param game the game related to this link
	 * @param pageNum
	 * @param pageSize
	 * @return The popular links based on specific game
	 */
	public Page<Link> findGameMostPopularLinks(Integer type, Long idGame, Date from, Date to,
		int pageNum, int pageSize);

	/**
	 * Find the links of a specific game posted by TwitterUsers(user or friends)
	 * @param type the type of the link
	 * @param game the game related to this link
	 * @param tUserIds The twitter user id 
	 * @param pageNum
	 * @param pageSize
	 * @return The links of the specific game posted by the friends of the input user
	 */
	public Page<Link> findGameLinksByTwitterUsers(Integer type, Long idGame, List<Long> tUserIds,
		int pageNum, int pageSize);
	
	/**
	 * Find all the links post by TwitterUsers(user or friends)
	 * @param type the type of the link
	 * @param from The start date
	 * @param to The end date
	 * @param tUserIds The twitter user id 
	 * @param pageNum
	 * @param pageSize
	 * @return The links from the friends of the specific user
	 */
	public Page<Link> findLinksByTwitterUsers(Integer type, Date from, Date to, List<Long> tUserIdss, int pageNum, int pageSize);
		
}
