package com.ratingbay.console.service;

import org.springframework.data.domain.Page;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Word;

import com.ratingbay.console.dataface.*;

public interface IWordService {
	public Page<IHashTagSummary> findHashTagSummaryByGame(Long idGame, int pageNum, int pageSize);
}
