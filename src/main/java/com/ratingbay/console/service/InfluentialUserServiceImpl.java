package com.ratingbay.console.service;

import java.util.*;
import java.sql.Timestamp;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.ratingbay.console.dao.InfluentialUserDAO;
import com.ratingbay.console.dataface.IGameSummary;
import com.ratingbay.console.model.InfluentialUser;

public class InfluentialUserServiceImpl extends BaseService implements IInfluentialUserService  {
	public InfluentialUserDAO influentialUserDao;
	public InfluentialUserDAO getInfluentialUserDao() {
		return influentialUserDao;
	}
	public void setInfluentialUserDao(InfluentialUserDAO influentialUserDao) {
		this.influentialUserDao = influentialUserDao;
	}

    @Override
	public Page<InfluentialUser> findTopPlayers (int pageNum, int pageSize){
		final PageRequest pageRequest = new PageRequest(pageNum, pageSize, 
			new Sort(new Order(Direction.DESC, "maxRetweetCount")));

		Date from = new Date(System.currentTimeMillis()-30*1000*24*3600L);
	    Date to = new Date(System.currentTimeMillis()+30*1000*24*3600L);
	    Timestamp starttimestamp = new Timestamp(from.getTime());
	    Timestamp endtimestamp = new Timestamp(to.getTime());
	    Page<InfluentialUser> allIUsersOfPage = influentialUserDao.findTopPlayers(starttimestamp,
	    	endtimestamp, pageRequest);

		List<InfluentialUser> tIUsersOfList = getUniqInfluentialUserList(allIUsersOfPage);
		Page<InfluentialUser> tIUsersofPage = new PageImpl(tIUsersOfList, pageRequest,
				allIUsersOfPage.getTotalElements());
		return tIUsersofPage;
	}

    @Override
	public Page<InfluentialUser> findTopPlayersByGame (Long idGame, int pageNum, int pageSize){
		
    	Date insertTime = (Date)influentialUserDao.findInsertTimeByGame().iterator().next();
	    Date from = DateUtils.addDays(insertTime, -7);
	    Date to = DateUtils.addDays(insertTime, 7);
	    Timestamp starttimestamp = new Timestamp(from.getTime());
	    Timestamp endtimestamp = new Timestamp(to.getTime());
	    List<InfluentialUser> allIUsers  = influentialUserDao.findTopPlayersByGame(idGame, 
	    	starttimestamp, endtimestamp);
	    
	    List<InfluentialUser> tIUsersOfList =new ArrayList<InfluentialUser>();
	    HashSet<Long> loadsSet = new HashSet<Long>();
	    int i = 0;
	    Iterator<InfluentialUser> it = allIUsers.iterator();
	    InfluentialUser iUser = null;
	    while(it.hasNext()&& i<pageSize){
	    	iUser = it.next();
	    	if(loadsSet.add(iUser.getTwitterUserId())){
	    		tIUsersOfList.add(iUser);
	    		i++;
	    	}
	    }
	 
	    Page<InfluentialUser> iPage = new PageImpl<InfluentialUser>(new ArrayList<InfluentialUser>());
	    iPage = new PageImpl<InfluentialUser>(tIUsersOfList);
		return iPage;
	}


	//used in class inner
	List<InfluentialUser> getUniqInfluentialUserList(Page<InfluentialUser> iUsersOfPage){
		List<InfluentialUser> tIUsersOfList = new ArrayList<InfluentialUser>();
		Map<Long, Boolean> isRepeat = new HashMap<Long, Boolean>();
		Iterator<InfluentialUser> it = iUsersOfPage.iterator();
		InfluentialUser iUser = null;
		while(it.hasNext()){
			iUser = it.next();
			if( !isRepeat.containsKey(iUser.getTwitterUserId()) ){
				isRepeat.put(iUser.getTwitterUserId(), true);
				tIUsersOfList.add(iUser);
			}
		}
		return tIUsersOfList;
	}
	
	@Override
	public Page<InfluentialUser> findTopPlayer (int pageNum , int pageSize){
	    Date insertTime = (Date)influentialUserDao.findInsertTimeByGame().iterator().next();
	    Date from = DateUtils.addDays(insertTime, -7);
	    Date to = DateUtils.addDays(insertTime, 7);
	    Timestamp starttimestamp = new Timestamp(from.getTime());
	    Timestamp endtimestamp = new Timestamp(to.getTime());
	    List<InfluentialUser> allIUsers = influentialUserDao.findTopPlayer(starttimestamp,
	    	endtimestamp);
	    
	    List<InfluentialUser> tIUsersOfList =new ArrayList<InfluentialUser>();
	    HashSet<Long> loadsSet = new HashSet<Long>(); 
	    int i=0;
	    Iterator<InfluentialUser> it = allIUsers.iterator();
	    InfluentialUser iUser = null;
	    while(it.hasNext()&& i<pageSize){
	    	iUser = it.next();
	    	if(loadsSet.add(iUser.getTwitterUserId())){
	    		tIUsersOfList.add(iUser);
	    		i++;
	    	}
	    }
	 
	    Page<InfluentialUser> iPage = new PageImpl<InfluentialUser>(new ArrayList<InfluentialUser>());
	    iPage = new PageImpl<InfluentialUser>(tIUsersOfList);
		return iPage;
	}
}