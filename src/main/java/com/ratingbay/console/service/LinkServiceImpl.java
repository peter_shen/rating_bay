package com.ratingbay.console.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.Iterator;

import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.*;

import com.ratingbay.console.dao.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.naivequery.LinkNaiveQuery;
import com.ratingbay.console.utils.Helper;

public class LinkServiceImpl  extends BaseService implements ILinkService {
	public LinkDAO linkDao;
	public LinkDAO getLinkDao() {
		return linkDao;
	}
	public void setLinkDao(LinkDAO linkDao) {
		this.linkDao = linkDao;
	}

	public TweetLinkDAO tweetlinkDao;
	public TweetLinkDAO getTweetLinkDao() {
		return tweetlinkDao;
	}
    public void setTweetLinkDao(TweetLinkDAO tweetlinkDao) {
		this.tweetlinkDao = tweetlinkDao;
	}

	public TweetDAO tweetDao;	
	public TweetDAO getTweetDao() {
		return tweetDao;
	}
	public void setTweetDao(TweetDAO tweetDao) {
		this.tweetDao = tweetDao;
	}

	public ITweetService tweetService;
	public ITweetService getTweetService() {
		return tweetService;
	}
	public void setTweetService(ITweetService tweetService) {
		this.tweetService = tweetService;
	}
	
	@Override
	public Page<Link> findMostPopularLinks(Integer type, Date from, Date to,
			int pageNum, int pageSize) {
		final PageRequest pageRequest = new PageRequest(pageNum, pageSize, 
			new Sort(new Order(Direction.DESC, "tweetCount")));
		Page<Link> links = this.linkDao.findByTypeWithMDLnot(type, "", pageRequest);
		return links;
	}
	
	@Override
	public Page<Link> findMostPopularVideoLinks(Integer type, Date from, Date to,
			int pageNum, int pageSize) {
		List<Link> list = this.linkDao.findByTypeWithMDLnotOrderByTweetCountDesc(type,pageSize);
		Page<Link> links = new PageImpl<Link>(list);
		return links;
	}

	@Override
	public Page<Link> findGameMostPopularLinks(Integer type, Long idGame,
			Date from, Date to, int pageNum, int pageSize) {
		//Long ll= this.linkDao.findCountByTypeAndGame(type, idGame);
		//PageRequest pageRequest = new PageRequest(pageNum, 10);
		Page<Link> linksOfPage = new PageImpl<Link>(new ArrayList<Link>());
		List<Link> list = new ArrayList<Link>();
		if(type==0){
			list = this.linkDao.findVideoByGameGroupByMedioDirectLinkOrderByTweetCountDesc(type, idGame,pageSize, pageNum);
		}else{
			list = this.linkDao.findByGameGroupByMedioDirectLinkOrderByTweetCountDesc(type, idGame,pageSize, pageNum);
		}
		if(list.size()<1)return linksOfPage;
		linksOfPage = new PageImpl<Link>(list);
		return linksOfPage;
	}

    @Override
	public Page<Link> findLinksByTwitterUsers(Integer type, Date from, Date to,
			List<Long> tUserIds, int pageNum, int pageSize) {
		final PageRequest pageRequest = new PageRequest(pageNum, pageSize,
			new Sort(new Order(Direction.DESC, "tweetCount")));
		Page<Link> linksOfPage = new PageImpl<Link>(new ArrayList<Link>());
		if(tUserIds.size() < 1)return linksOfPage;
	
		List<Long> linkIds = this.tweetlinkDao.findLinkIdsByTwitterUserIds(tUserIds);
		if(linkIds.size() < 1)return linksOfPage;

		linksOfPage = this.linkDao.findByTypeAndLinkIdsWithMDLnot(type, linkIds,
			"", pageRequest);
		return linksOfPage;
	}

	@Override
	public Page<Link> findGameLinksByTwitterUsers(Integer type, Long idGame,
			List<Long> tUserIds, int pageNum, int pageSize) {
		
		Page<Link> linksOfPage = new PageImpl<Link>(new ArrayList<Link>());
		if(tUserIds.size() < 1){
			return linksOfPage;
		}
	    List<Long> linkIds = this.tweetlinkDao.findLinkIdsByTwitterUserIds(tUserIds);
		if(linkIds.size() < 1)return linksOfPage;
		List<Link> ll = new ArrayList<Link>();
		List<Long> result=LinkNaiveQuery.getInstance().findByTypeAndGameAndLinkIdsGroupByTweetCountDescLimit(type,idGame,tUserIds,pageSize,pageNum);
		for(Long l :result){
			Link list= this.linkDao.findById(l).get(0);
			ll.add(list);
		}
		linksOfPage = new PageImpl<Link>(ll);
		return linksOfPage;	
	}
}
