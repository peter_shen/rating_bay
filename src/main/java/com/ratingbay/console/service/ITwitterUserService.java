package com.ratingbay.console.service;

import java.util.List;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.social.twitter.api.TwitterProfile;

import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.TwitterUser;
import com.ratingbay.console.dataface.ITwitterUser;
import com.ratingbay.console.dataimpl.TwitterUserImpl;

public interface ITwitterUserService {
    //get ITwitterUser from tweet table by tUserId
    ITwitterUser findUserByUserId(Long tUserId);
    ITwitterUser findUserNameByUserId(Long tUserId);

	ITwitterUser findByUserId(Long userId);

    /**
     * find users by userName like	
     * @param uName
     * @param pageNum
     * @param pageSize
     * @return search page show users by userName
     */
    Page<ITwitterUser> findUsersByUserNameLike(String uName, int pageNum, int pageSize);
    
    /**
     * find users by userName like	
     * @param uName
     * @param orderField
     * @param orderDirection
     * @param pageNum
     * @param pageSize
     * @return search page show users by userName and order by something
     */
    Page<ITwitterUser> findUsersByUserNameLike(String uName, String orderField, String orderDirection, int pageNum, int pageSize);
    List<TwitterUser> findUserNameByUserNameLike(String uName ,int pageNum, int pageSize);
    
    
    Long findUserByUserName(String uName);
}
