package com.ratingbay.console.service;

import java.util.Date;

import org.springframework.data.domain.Page;

import com.ratingbay.console.model.User;

public interface IUserService {
    //TODO without test
	public User findUserById(String id);
	
    //TODO without test
	public User findByUsername(String username);
	
	//TODO without test	
	public void saveUser(User user);

    //TODO without test
	public void save(User user);
}
