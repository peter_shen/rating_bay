package com.ratingbay.console.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.ratingbay.console.dao.MentionedUserDAO;
import com.ratingbay.console.model.*;

public class MentionedUserServiceImpl extends BaseService implements IMentionedUserService{
	public MentionedUserDAO mentioneduserDao = null;
	public MentionedUserDAO getMentionedUserDao() {
		return mentioneduserDao;
	}
	public void setMentionedUserDao(MentionedUserDAO mentioneduserDao) {
		this.mentioneduserDao = mentioneduserDao;
	}

	@Override
	public Page<MentionedUser> findMentionedUsers(Tweet tweet, int pageNum,
			int pageSize) {
		final PageRequest mentioneduserpage = new PageRequest(pageNum, pageSize, new Sort(new Order(Direction.ASC, "userId")));
		return this.mentioneduserDao.findByTweet(tweet, mentioneduserpage);
	}
}
