package com.ratingbay.console.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ratingbay.console.model.*;
import com.ratingbay.console.dataface.*;

public interface ITweetService {
	Long findGameCountByUser(Long tUserId);
	Long findCountByGame(Long idGame);
	Page<IGamePlayer> findGamersByUserIds(List<Long> tUserIds, int pageNum, int pageSize);
	
	/**
	 * Find the latest tweets 
	 * @param pageNum
	 * @param pageSize
	 * @return The latest tweets
	 */
	List<Tweet> findRecentTweets(Date begin,Date end);
	
	/**
	 * Find the popular tweets
	 * @param from The start date
	 * @param to The end date
	 * @param pageNum
	 * @param pageSize
	 * @return The popular tweets between the start date and the end date
	 */
	List<Tweet> findPopularTweets(Date from, Date to);
	
	/**
	 * Find the tweets sent by a tweeter user
	 * @param from The start date
	 * @param to The end date
	 * @param tUserIds The twitter user Id
	 * @param pageNum
	 * @param pageSize
	 * @return The popular tweets between the start date and the end date
	 */
	Page<Tweet> findTweetsByUserId(Date from, Date to, Long tUserIds, int pageNum, int pageSize);
	
	/**
	 * Find the tweets sent by the friends of a twitter user for a particular game
	 * @param tUserId 
	 * @param from
	 * @param to
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public Page<Tweet> findTweetsByUserIds(List<Long> tUserIds, Date from, Date to, int pageNum, int pageSize);

	/**
	 * Find the popular tweets
	 * @param game: the game id
	 * @param pageNum
	 * @param pageSize
	 * @return The popular tweets between the start date and the end date
	 */
	public Page<Tweet> findPopularTweetsByGame(Long idGame, Date from, Date to, int pageNum, int pageSize);
	
	/**
	 * Find the positive tweets
	 * @param game: the game id
	 * @param pageNum
	 * @param pageSize
	 * @return The positve tweets based on specific game
	 */
	public Page<Tweet> findPositiveTweetsByGame(Long idGame, Date from, Date to, int pageNum, int pageSize);
	
	/**
	 * Find the negative tweets
	 * @param game: the game id
	 * @param pageNum
	 * @param pageSize
	 * @return The negative tweets based on specific game
	 */
	public Page<Tweet> findNegativeTweetsByGame(Long idGame, Date from, Date to, int pageNum, int pageSize);
	
	/**
	 * Find the latest tweets
	 * @param game: the game id
	 * @param pageNum
	 * @param pageSize
	 * @return The latest tweets based on specific game
	 */
	public Page<Tweet> findLatestTweetsByGame(Long idGame, Date from, Date to, int pageNum, int pageSize);

	/**
	 * @param tUserIds 
	 * @param idGame
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public Page<Tweet> findUsersTweetsByGameAndUsers(Long idGame, List<Long> tUserIds,
		int pageNum, int pageSize);

	/**
	 * @param linkId
	 * @param tUserIds
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public List<Tweet> getTweetsByLinkAndUsers(Long linkId, List<Long> tUserIds, int pageNum, int pageSize);

	/**
	 * @param linkId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public List<Tweet> getTweetsByLink(Long linkId, int pageNum, int pageSize);
}
