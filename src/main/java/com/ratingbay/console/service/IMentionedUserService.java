package com.ratingbay.console.service;

import java.util.Date;

import org.springframework.data.domain.Page;

import com.ratingbay.console.model.MentionedUser;
import com.ratingbay.console.model.Tweet;

public interface IMentionedUserService {
	/**
	 * Find the mentioned users based on specific tweet
	 * @param tweet The tweet information associated with the mentioneduser
	 * @param pageNum
	 * @param pageSize
	 * @return The mentioned user based on specific tweets
	 */
	//TODO:impl without test
	Page<MentionedUser> findMentionedUsers(Tweet tweet, int pageNum, int pageSize);
}
