package com.ratingbay.console.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.model.Game;
import com.ratingbay.console.model.Link;
import com.ratingbay.console.model.RbGameTweetTrend;
import com.ratingbay.console.model.SentimentScoreHistory;

/**
 * @author zhuxun
 * @version 1.0
 * @date 下午2:05:03 2014年8月1日
 */
public interface IGameService {
	Game findById(Long idGame);
	
	/**
	 * is status
	 * @param gameId
	 * @return true when gameId in(select id from game where status =1)
	 */
	List<Game> findGameEnable(Long gameId);
	
	/**
	 * find game genre name
	 * @param genre
	 * @return game genre to category action
	 */
	String findGameGenreName(Long genre);
	
	/**
	 * find score history
	 * @param idGame
	 * @param beforeDate
	 * @param curDate
	 * @return 1 month game's score
	 */
	List<SentimentScoreHistory> findScoreHistory(Long idGame,Date beforeDate,Date curDate);
	
	/**
	 * find tweet history
	 * @param idGame
	 * @param beforeDate
	 * @param curDate
	 * @return count game's twitter num everyDay in 3 months
	 */
	List<RbGameTweetTrend> findTweetHistory(Long idGame,Date beforeDate,Date curDate);
	
	/**
	 * find platform by id
	 * @param id
	 * @return platform by gameId
	 */
	List<String> findPlatformById(Long id);
	
	/**
	 * find genre by id
	 * @param id
	 * @return genre by gameId
	 */
	List<String> findGenreById(Long id);
	
	Page<IGameSummary> findReleaseGames(Date now,int pageNum,int pageSize);
	Page<IGameSummary> findReleaseGames(Date now, String f, String d, int pageNum, int pageSize);
	
	Page<IGameSummary> findComingGames(Date now, int pageNum, int pageSize);
	Page<IGameSummary> findComingGames(Date now,String f,String d, int pageNum, int pageSize);
	
	Page<IGameSummary> findByGenre(Long genre,int pageNum,int pageSize);
	Page<IGameSummary> findGamesByGenre(Long genre, String f, String d, int pageNum, int pageSize);
	
	Page<IGameSummary> findByPlatform(String platform, int pageNum, int pageSize);
	Page<IGameSummary> findByPlatform(String platform,String f,String d, int pageNum, int pageSize);
	
	/**
	 * find by userId
	 * @param tUserId
	 * @param pageNum
	 * @param pageSize
	 * @return games which the user talked
	 */
	Page<Game> findbyUserId(Long tUserId, int pageNum, int pageSize);
	
	/**
	 * find by userIds
	 * @param tUserIds
	 * @param pageNum
	 * @param pageSize
	 * @return games which the user friends talked
	 */
	Page<Game> findbyUserIds(List<Long> tUserIds, int pageNum, int pageSize);
	
	IGameDetail findGameDetail(Long idGame);
	
	/**
	 * Find the games whose title contains (ignore case) the input
	 * @param title  The complete or partial game's title
	 * @return The games whose title contains (ignore case) the input
	 */
	Page<IGameSummary> findByTitleContains(String title, int pageNum, int pageSize);
	Page<IGameSummary> findByTitleContains(String title, String orderField, String orderDirection, int pageNum, int pageSize);		
		
	/**
	 * Find the headline games used in the banner 
	 * @param releasedBefore  the games are released before this date 
	 * @param releaseAfter the games are released after this date 
	 * @param limit  The max number of games returned 
	 * @return The most tweeted games between releaseAfter and releasedBefore
	 */
	Page<IGameSummary> findHeadlineGames(Date releasedBefore, Date releaseAfter, int pageNu, int pageSize);
	
	/**
	 * Find the top recommended games given a particular game
	 * @param idGame
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */

	Page<IGameSummary> findRelatedGamesByGame(Long idGame, int pageNum, int pageSize);
	
	/**
	 * Find the top games recommended by a twitter user's friends given a particular game
	 * @param idGame
	 * @param tUserIds
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	Page<IGameSummary> findRelatedGamesByGameAndUsers(Long idGame, List<Long> tUserIds, 
		int pageNum, int pageSize);

	/**
	 * Find the top tweeted games 
	 * @param releasedBefore  the games are released before this date 
	 * @param releaseAfter the games are released after this date 
	 * @param pageNum The page number 
	 * @param pageSize  The page size
	 * @return The most tweeted games between releaseAfter and releasedBefore
	 */
	List<IGameSummary> findTopTweetedGames(Date releasedBefore, Date releaseAfter, int pageNum, int pageSize);
	
	/**
	 * Find the top rated games 
	 * @param releasedBefore  the games are released before this date 
	 * @param releaseAfter the games are released after this date 
	 * @param pageNum The page number 
	 * @param pageSize  The page size
	 * @return The games order by score desc between releaseAfter and releasedBefore
	 */
	List<IGameSummary> findTopRatedGames(Date releasedBefore, Date releaseAfter, int pageNum, int pageSize);
	
	/**
	 * Find games by title like
	 * @param content search title name 
	 * @param pageNum The page number 
	 * @param pageSize  The page size
	 * @return The games find by title name
	 */
	List<Game> findGameByTitleLike(String content ,int pageNum,int pageSize);
	List<IGameSummary> findTopTweetedGamesRestul(Date releasedBefore, Date releaseAfter, int pageNum, int pageSize);
}
