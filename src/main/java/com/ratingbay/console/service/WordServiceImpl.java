package com.ratingbay.console.service;

import java.util.*;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.dataimpl.*;
import com.ratingbay.console.dao.*;
import com.ratingbay.console.model.*;

public class WordServiceImpl extends BaseService implements IWordService {
	public WordTweetDAO wordTweetDao;

	public WordTweetDAO getWordTweetDao() {
		return wordTweetDao;
	}
	public void setWordTweetDao(WordTweetDAO wordTweetDao) {
		this.wordTweetDao = wordTweetDao;
	}
	
	@Override
	public Page<IHashTagSummary> findHashTagSummaryByGame(Long idGame, int pageNum, int pageSize) {
		final PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		
		Page<Object[]> hashTagPage = this.wordTweetDao.findWordSummaryByGameId(idGame, true, pageRequest);
		Iterator<Object[]> it = hashTagPage.iterator();

		List<IHashTagSummary> iHashTagList = new ArrayList<IHashTagSummary>();

		Object[] hashTagOne;
		while(it.hasNext()){
			hashTagOne = it.next();

			HashTagSummaryImpl hashTagSummary = new HashTagSummaryImpl();
			hashTagSummary.setText( (String)hashTagOne[0] );
			hashTagSummary.setCount( (Long)hashTagOne[1] );

			iHashTagList.add(hashTagSummary);
		}
		Page<IHashTagSummary> iHashTagPage = new PageImpl<IHashTagSummary>(iHashTagList, 
			pageRequest, hashTagPage.getTotalElements());
		return iHashTagPage;
	}
}
