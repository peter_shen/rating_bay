package com.ratingbay.console.service;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.ratingbay.console.dao.UserDAO;
import com.ratingbay.console.model.User;

public class UserServiceImpl extends BaseService implements IUserService, UserDetailsService {
	UserDAO userDao = null;
	public UserDAO getUserDao() {
		return userDao;
	}
	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}


	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user =  this.userDao.findByUsername(username);
		if (user == null)
			throw new UsernameNotFoundException("User " + username + " does not exist.");
		return (UserDetails) user;
	}
	

	@Override
	public User findUserById(String id) {	
		return this.userDao.findById(id);
	}

    @Override
	public User findByUsername(String username) {
		return this.userDao.findByUsername(username);
	}

	@Override
	public void saveUser(User user) {
		this.userDao.save(user);
	}

	@Override
	public void save(User user) {
		this.userDao.save(user);
	}

}
