package com.ratingbay.console.service;

import java.util.*;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.xml.crypto.Data;

import org.apache.commons.beanutils.*;
import org.springframework.data.domain.*;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;

import com.ratingbay.console.dataface.*;
import com.ratingbay.console.dataimpl.*;
import com.ratingbay.console.dao.*;
import com.ratingbay.console.model.*;
import com.ratingbay.console.naivequery.*;

public class GameServiceImpl extends BaseService implements IGameService {
	// need injected objs 
	public GenreDAO genreDao;
	public GenreDAO getGenreDao() {
		return genreDao;
	}
	public void setGenreDao(GenreDAO genreDao) {
		this.genreDao = genreDao;
	}

	public GameDAO gameDao;
	public GameDAO getGameDao() {
		return gameDao;
	}
	public void setGameDao(GameDAO gameDao) {
		this.gameDao = gameDao;
	}

	public RbGameTweetTrendDAO rbGameTweetTrendDao;
	public RbGameTweetTrendDAO getRbGameTweetTrendDao() {
		return rbGameTweetTrendDao;
	}
	public void setRbGameTweetTrendDao(RbGameTweetTrendDAO rbGameTweetTrendDao) {
		this.rbGameTweetTrendDao = rbGameTweetTrendDao;
	}

	public TweetDAO tweetDao;
	public TweetDAO getTweetDao() {
		return tweetDao;
	}
	public void setTweetDao(TweetDAO tweetDao) {
		this.tweetDao = tweetDao;
	}

	public SentimentScoreHistoryDAO sentimentScoreHistoryDao;
	public SentimentScoreHistoryDAO getSentimentScoreHistoryDao() {
		return sentimentScoreHistoryDao;
	}
	public void setSentimentScoreHistoryDao(SentimentScoreHistoryDAO sentimentScoreHistoryDao) {
		this.sentimentScoreHistoryDao = sentimentScoreHistoryDao;
	}

	public GameSummaryDAO gameSummaryDao;
	public GameSummaryDAO getGameSummaryDao(){
		return gameSummaryDao;
	}
	public void setGameSummaryDao(GameSummaryDAO gameSummaryDao){
		this.gameSummaryDao = gameSummaryDao;
	}

	public ITweetService tweetService;
	public ITweetService getTweetService() {
		return tweetService;
	}
	public void setTweetService(ITweetService tweetService) {
		this.tweetService = tweetService;
	}


	//method used in class inner
	protected List<Long> getIdGameList(List<Game> gameList){
		List<Long> idGamesList = new ArrayList<Long>();
		Iterator<Game> it= gameList.iterator();

		while(it.hasNext()){
            idGamesList.add( it.next().getId() );
        }
        return idGamesList;
	}

	protected List<GameSummary> getOrderedTargetContents(List<Game> orderedContents, List<GameSummary> targetContents){
		HashMap<Long, GameSummary> hashMap = new HashMap<Long, GameSummary>();
		for(GameSummary gs : targetContents){
			hashMap.put(gs.getIdGame(), gs);
		}

		List<GameSummary> orderedTargetContents = new ArrayList<GameSummary>();
		for(Game g : orderedContents){
			orderedTargetContents.add(hashMap.get(g.getId()));
		}
		return orderedTargetContents;
	}


	//implement interfaces
	@Override
	public Game findById(Long idGame) {
		return this.gameDao.findById(idGame);
	}

	@Override
	public Page<Game> findbyUserId(Long userId, int pageNum, int pageSize) {
		Page<Game> gamesOfPage = new PageImpl<Game>(new ArrayList<Game>());
		
		List<Game> gamesOfList = new ArrayList<Game>();
		List<Long> idGamesOfList = TweetNaiveQuery.getInstance().findIdGameByUser(userId,pageNum,pageSize);
		if(idGamesOfList.size()<1)return gamesOfPage;
		gamesOfList = gameDao.findGameIdIn(idGamesOfList);	
		
		gamesOfPage = new PageImpl<Game>( gamesOfList);
        return gamesOfPage;

	}

	@Override
	public Page<Game> findbyUserIds(List<Long> tUserIds, int pageNum, int pageSize) {
		Page<Game> gamesOfPage = new PageImpl<Game>(new ArrayList<Game>());
	    PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		Long countIdGame = TweetNaiveQuery.getInstance().findCountIdGameByUserIn(tUserIds);
		if(countIdGame < 1) return gamesOfPage;
		
		List<Game> gamesOfList = new ArrayList<Game>();
		List<Long> idGamesOfList = TweetNaiveQuery.getInstance().findIdGameByUserIn(tUserIds,pageNum,pageSize);
		for(Long k:idGamesOfList){
			List<Game> games = gameDao.findGameById(k);	
			gamesOfList.add(games.get(0));
		}
		
		gamesOfPage = new PageImpl<Game>( gamesOfList, pageRequest, countIdGame);
        return gamesOfPage;
	}

	@Override
	public IGameDetail findGameDetail(Long idGame){
		Game game = this.gameDao.findById(idGame);
		return GameDetailImplWithGame.getIGameDetail(game);
	}
	
	@Override
	public List<SentimentScoreHistory> findScoreHistory(Long idGame,Date beforeDate,Date curDate){
		
		Timestamp starttimestamp = new Timestamp(beforeDate.getTime());
		Timestamp endtimestamp = new Timestamp(curDate.getTime());
		List<SentimentScoreHistory> score = this.sentimentScoreHistoryDao.findScoreHistory(idGame,starttimestamp,endtimestamp);
		return score;
	}
	
	@Override
	public List<RbGameTweetTrend> findTweetHistory(Long idGame,Date beforeDate,Date curDate){
		
		Timestamp starttimestamp = new Timestamp(beforeDate.getTime());
		Timestamp endtimestamp = new Timestamp(curDate.getTime());
		List<RbGameTweetTrend> score = this.rbGameTweetTrendDao.findTweetHistory(idGame,starttimestamp,endtimestamp);
		return score;
	}

	@Override
	public Page<IGameSummary> findHeadlineGames(Date releasedBefore,
			Date releaseAfter, int pageNum, int pageSize) {
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		Timestamp start = new Timestamp(releasedBefore.getTime());
		Timestamp end= new Timestamp(releaseAfter.getTime());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize,
			new Sort(new Order(Direction.DESC, "releaseDate")));

		Page<Game> gamesPage = gameDao.findHeadlineByReleaseDate(start, end, pageRequest);
		List<Game> games = gamesPage.getContent();
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameOrderByScore(idGamesList);
		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, gamesPage.getTotalElements());
		return iGameSummaryPage;
	}

	//TODO calculate method isn't good enough
	@Override
	public Page<IGameSummary> findRelatedGamesByGame(Long idGame, int pageNum, int pageSize) {
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		List<Long> relatedGameIds = gameDao.findRelatedGameIdsByGame(idGame, pageNum, pageSize);
		if( relatedGameIds.size() < 1 ) return iGameSummaryPage;

		//List<Game> games = this.gameDao.findGameIdIn(relatedGameIds);
		//if(games.size() < 1) return iGameSummaryPage;

		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(relatedGameIds);
		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList);
		return iGameSummaryPage;
	}

	@Override
	public Page<IGameSummary> findRelatedGamesByGameAndUsers(Long idGame, List<Long> tUserIds, 
		int pageNum, int pageSize) {
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		List<Long> relatedGameIds = 
			GameNaiveQuery.getInstance().findRelatedGameIdsByGameAndUsers(idGame, tUserIds, pageNum, pageSize);
		if( relatedGameIds.size() < 1 ) return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameIdIn(relatedGameIds);
		if(games.size() < 1) return iGameSummaryPage;

		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(relatedGameIds);
		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList);
		return iGameSummaryPage;
	}

	//search page show games
	@Override
	public Page<IGameSummary> findByTitleContains(String title, int pageNum, int pageSize) {
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		String searchTitle = "%" + title + "%";
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByTitleLike(searchTitle);
		if(countGame < 1)return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameByTitleLike(searchTitle, pageNum, pageSize);
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}

	//search page show games
	@Override
	public Page<IGameSummary> findByTitleContains(String title, String orderField, String orderDirection, int pageNum, int pageSize) {
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		String searchTitle = "%" + title + "%";
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByTitleLike(searchTitle);
		if(countGame < 1)return iGameSummaryPage;
		List<Game> games = new ArrayList<Game>();

		if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTitleLikeASCOrderTC(searchTitle, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTitleLikeDESCOrderTC(searchTitle, pageNum, pageSize);
			}
		}else if(orderField.equals("RD")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTitleLikeASCOrderRD(searchTitle, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTitleLikeDESCOrderRD(searchTitle, pageNum, pageSize);
			}
		}else if(orderField.equals("SS")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTitleLikeASCOrderSS(searchTitle, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTitleLikeDESCOrderSS(searchTitle, pageNum, pageSize);
			}
		}
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}

	@Override
	public List<IGameSummary> findTopTweetedGamesRestul(Date releasedBefore,
			Date releaseAfter, int pageNum, int pageSize) {
		List<IGameSummary> res = new ArrayList<IGameSummary>();
		Timestamp starttimestamp = new Timestamp(releasedBefore.getTime());
		Timestamp endtimestamp = new Timestamp(releaseAfter.getTime()); 
		List<Game> games = this.gameDao.findTopTweetedGameRestul(pageNum, pageSize);
		if(games.size() < 1) return res;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		return GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
	}
	
	@Override
	public List<IGameSummary> findTopTweetedGames(Date releasedBefore,
			Date releaseAfter, int pageNum, int pageSize) {
		List<IGameSummary> res = new ArrayList<IGameSummary>();
		Timestamp starttimestamp = new Timestamp(releasedBefore.getTime());
		Timestamp endtimestamp = new Timestamp(releaseAfter.getTime()); 
		List<Game> games = this.gameDao.findTopTweetedGame(starttimestamp, endtimestamp, pageNum, pageSize);
		if(games.size() < 1) return res;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		return GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
	}

	@Override
	public List<IGameSummary> findTopRatedGames(Date releasedBefore,
			Date releaseAfter, int pageNum, int pageSize) {
		List<IGameSummary> res = new ArrayList<IGameSummary>();
		Timestamp starttimestamp = new Timestamp(releasedBefore.getTime());
		Timestamp endtimestamp = new Timestamp(releaseAfter.getTime()); 
		List<Game> games = this.gameDao.findTopRatedGame(starttimestamp, endtimestamp, pageNum, pageSize);
		if(games.size() < 1) return res;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		return GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
	}
	
	@Override
	public List<Game> findGameByTitleLike(String content ,int pageNum,int pageSize){
		String searchContent = "%" + content + "%";
		List<Game> searchGame = this.gameDao.findGameByTitleLike(searchContent, pageNum, pageSize);
		return searchGame;
	}
	
	@Override
	public Page<IGameSummary> findByGenre(Long genre,int pageNum,int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByGenre(genre);
		if(countGame < 1)return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameByGenre(genre, pageNum, pageSize);
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findGamesByGenre(Long genre, String orderField, String orderDirection, int pageNum, int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByGenre(genre);
		if(countGame < 1)return iGameSummaryPage;
		List<Game> games = new ArrayList<Game>();

		if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByGenreLikeASCOrderTC(genre, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByGenreLikeDESCOrderTC(genre, pageNum, pageSize);
			}
		}else if(orderField.equals("RD")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByGenreLikeASCOrderRD(genre, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByGenreLikeDESCOrderRD(genre, pageNum, pageSize);
			}
		}else if(orderField.equals("SS")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByGenreLikeASCOrderSS(genre, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByGenreLikeDESCOrderSS(genre, pageNum, pageSize);
			}
		}
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findByPlatform(String platform,int pageNum,int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		String searchPlatform = "%" + platform + "%";
		Long countGame = this.gameDao.findCountGameByPlatform(searchPlatform);
		if(countGame < 1)return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameByPlatformLikeDESCOrderSS(searchPlatform, pageNum, pageSize);
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findByPlatform(String platform, String orderField, String orderDirection, int pageNum, int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		String searchPlatform = "%" + platform + "%";
		Long countGame = this.gameDao.findCountGameByPlatform(searchPlatform);
		if(countGame < 1)return iGameSummaryPage;
		List<Game> games = new ArrayList<Game>();

		if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByPlatformLikeASCOrderTC(searchPlatform, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByPlatformLikeDESCOrderTC(searchPlatform, pageNum, pageSize);
			}
		}else if(orderField.equals("RD")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByPlatformLikeASCOrderRD(searchPlatform, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByPlatformLikeDESCOrderRD(searchPlatform, pageNum, pageSize);
			}
		}else if(orderField.equals("SS")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByPlatformLikeASCOrderSS(searchPlatform, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByPlatformLikeDESCOrderSS(searchPlatform, pageNum, pageSize);
			}
		}
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public List<String> findGenreById(Long id){
		List<String> genre = new ArrayList<String>(); 
		genre=this.gameDao.findGenreById(id);
		return genre;
	}
	@Override
	public List<String> findPlatformById(Long id){
		List<String> platform = new ArrayList<String>();
		platform=this.gameDao.findPlatformById(id);
		return platform;
	}
	//release and coming game
	
	@Override
	public Page<IGameSummary> findReleaseGames(Date now,int pageNum,int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByTimeBefore(now);
		if(countGame < 1)return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameByTimeBeforeLikeDESCOrderRD(now, pageNum, pageSize);
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findReleaseGames(Date now, String orderField, String orderDirection, int pageNum, int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		
        Long countGame = this.gameDao.findCountGameByTimeBefore(now);
		if(countGame < 1)return iGameSummaryPage;
		List<Game> games = new ArrayList<Game>();

		if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeBeforeLikeASCOrderTC(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeBeforeLikeDESCOrderTC(now, pageNum, pageSize);
			}
		}else if(orderField.equals("RD")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeBeforeLikeASCOrderRD(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeBeforeLikeDESCOrderRD(now, pageNum, pageSize);
			}
		}else if(orderField.equals("SS")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeBeforeLikeASCOrderSS(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeBeforeLikeDESCOrderSS(now, pageNum, pageSize);
			}
		}
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findComingGames(Date now,int pageNum,int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);

		Long countGame = this.gameDao.findCountGameByTimeAfter(now);
		if(countGame < 1)return iGameSummaryPage;

		List<Game> games = this.gameDao.findGameByTimeAfterLikeDESCOrderRD(now, pageNum, pageSize);
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public Page<IGameSummary> findComingGames(Date now, String orderField, String orderDirection, int pageNum, int pageSize){
		Page<IGameSummary> iGameSummaryPage = new PageImpl<IGameSummary>(new ArrayList<IGameSummary>());
		PageRequest pageRequest = new PageRequest(pageNum, pageSize);
		
        Long countGame = this.gameDao.findCountGameByTimeAfter(now);
		if(countGame < 1)return iGameSummaryPage;
		List<Game> games = new ArrayList<Game>();

		if(orderField.equals("TC")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeAfterLikeASCOrderTC(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeAfterLikeDESCOrderTC(now, pageNum, pageSize);
			}
		}else if(orderField.equals("RD")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeAfterLikeASCOrderRD(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeAfterLikeDESCOrderRD(now, pageNum, pageSize);
			}
		}else if(orderField.equals("SS")){
			if(orderDirection.equals("ASC")){
				games = this.gameDao.findGameByTimeAfterLikeASCOrderSS(now, pageNum, pageSize);
			}
			else{
				games = this.gameDao.findGameByTimeAfterLikeDESCOrderSS(now, pageNum, pageSize);
			}
		}
		
		if(games.size() < 1) return iGameSummaryPage;

		List<Long> idGamesList = getIdGameList(games);
		List<GameSummary> gameSummaryList = gameSummaryDao.findByIdGameIn(idGamesList);
		gameSummaryList = getOrderedTargetContents(games, gameSummaryList);

		List<IGameSummary> iGameSummaryList = GameSummaryImplWithModel.getIGameSummarys(gameSummaryList);
		
		iGameSummaryPage = new PageImpl<IGameSummary>(iGameSummaryList, 
			pageRequest, countGame);
		return iGameSummaryPage;
	}
	
	@Override
	public String findGameGenreName(Long genre){
		List<Genre> genr=genreDao.findGameGenreName(genre);
		String gen = genr.get(0).getName();
		return gen;
	}
	
	@Override
	public List<Game> findGameEnable(Long gameId){
		List<Game> game =  gameDao.findEnableGame(gameId);
		return game;
	}
}
