package com.ratingbay.console.service;

import java.util.Date;
import java.util.List;

import com.ratingbay.console.model.InfluentialUser;

import org.springframework.data.domain.Page;

public interface IInfluentialUserService {
	
	/**
	 * find top player
	 * @param pageNum
	 * @param pageSize
	 * @return home page most flowers 
	 */
	Page<InfluentialUser> findTopPlayer (int pageNum ,int pageSize);
	Page<InfluentialUser> findTopPlayers(int pageNum, int pageSize);
	Page<InfluentialUser> findTopPlayersByGame(Long idGame, int pageNum, int pageSize);
}