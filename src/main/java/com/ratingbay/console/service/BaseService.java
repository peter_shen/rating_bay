package com.ratingbay.console.service;
import java.sql.Timestamp;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.context.support.WebApplicationObjectSupport;

/**
 * This class is the abstract class of the service layer. All the service class
 * should extend this class.
 * 
 * @author lwang
 * 
 */
public abstract class BaseService extends WebApplicationObjectSupport {
    /** The logger */
	Logger logger = LogManager.getLogger(this.getClass().getName());
    
    public Object getBean(String name) {
        return this.getWebApplicationContext().getBean(name);
    }

}
