package com.ratingbay.console.common;

import java.util.*;
import javax.servlet.http.HttpSession;

public class HttpSessionImpl implements ISession{
	public HttpSessionImpl(){}

	private HttpSession session;
	public HttpSessionImpl(HttpSession session){
		this.session = session;
	}

	public HttpSession getSession(){
		return this.session;
	}
	public void setSession(HttpSession session){
		this.session = session;
	}

	@Override
	public Object get(String key){
		return this.session.getAttribute(key);

	}

	@Override
	public void put(String key, Object value){
		this.session.setAttribute(key, value);
	}
}