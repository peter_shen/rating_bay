package com.ratingbay.console.common;

import com.ratingbay.console.api.*;

public class ApiComponent extends BaseComponent{
    private ApiComponent(){}
    private static ApiComponent instance = null;
    public static ApiComponent getInstance(){
        if(null == instance){
            instance = new ApiComponent();
        }
        return instance;
    }

    public ServiceComponent serviceComponent = null;
    public ServiceComponent getServiceComponent(){
        if(null == serviceComponent){
            serviceComponent = ServiceComponent.getInstance();
        }
        return serviceComponent;
    }
    public void setServiceComponent(ServiceComponent serviceComponent){
        this.serviceComponent = serviceComponent;
    }
	
    public TweetApi tweetApi = null;
    public TweetApi getTweetApi(){
        if(null == tweetApi){
            tweetApi = TweetApi.getInstance(getServiceComponent().getTweetService());
        }
        return tweetApi;
    }

    GameApi gameApi = null;
    public GameApi getGameApi(){
        if(null == gameApi){
            gameApi = GameApi.getInstance(getServiceComponent().getGameService());
        }
        return gameApi;
    }

    LinkApi linkApi = null;
    public LinkApi getLinkApi(){
        if(null == linkApi){
            linkApi = LinkApi.getInstance(getServiceComponent().getLinkService());
        }
        return linkApi;
    }

    WordApi wordApi = null;
    public WordApi getWordApi(){
        if(null == wordApi){
            wordApi = WordApi.getInstance(getServiceComponent().getWordService());
        }
        return wordApi;
    }

    InfluentialUserApi iUserApi = null;
    public InfluentialUserApi getIUserApi(){
        if(null == iUserApi){
            iUserApi = InfluentialUserApi.getInstance(getServiceComponent().getInfluentialUserService());
        }
        return iUserApi;
    }
}