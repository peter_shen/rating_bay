package com.ratingbay.console.common;

import com.ratingbay.console.service.*;

public class ServiceComponent extends BaseComponent{
	public ServiceComponent(){
	    instance = this;
    }
    private static ServiceComponent instance = null;
    public static ServiceComponent getInstance(){
        return instance;
    }
	
	// need injected objs 
    public IWordService wordService;
    public IWordService getWordService() {
        return wordService;
    }
    public void setWordService(IWordService wordService) {
        this.wordService = wordService;
    }

    public IGameService gameService;
    public IGameService getGameService() {
        return gameService;
    }
    public void setGameService(IGameService gameService) {
        this.gameService = gameService;
    }

    public ITweetService tweetService;
    public ITweetService getTweetService() {
        return tweetService;
    }
    public void setTweetService(ITweetService tweetService) {
        this.tweetService = tweetService;
    }
    
    public ILinkService linkService;
    public ILinkService getLinkService() {
        return linkService;
    }
    public void setLinkService(ILinkService linkService) {
        this.linkService = linkService;
    }

    public ITwitterUserService twitterUserService;
    public ITwitterUserService getTwitterUserService(){
        return twitterUserService;
    }
    public void setTwitterUserService(ITwitterUserService twitterUserService){
        this.twitterUserService = twitterUserService;
    }

    public IInfluentialUserService influentialUserService;
    public IInfluentialUserService getInfluentialUserService() {
        return influentialUserService;
    }
    public void setInfluentialUserService(IInfluentialUserService influentialUserService) {
        this.influentialUserService = influentialUserService;
    }
}