package com.ratingbay.console.common;

public interface ISession{
	public Object get(String key);
	public void put(String key, Object value);
}