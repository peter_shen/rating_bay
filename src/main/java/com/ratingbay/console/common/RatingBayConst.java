package com.ratingbay.console.common;

import com.ratingbay.console.utils.PropertiesUtil;

public interface RatingBayConst {

	public static final String COOKIE_ID="rt_openDialogOnce";
	public static final String SESSION_KEY_USER="twitterUser";
	public static final String SESSION_KEY_TWITTER="twitter";
	
	public static final String LUCENE_REINDEX=PropertiesUtil.getValue("lucene.reindex");
	public static final String LUCENE_FILE_PATH=PropertiesUtil.getValue("lucene.file.path");
	public static final Integer LUCENE_COUNTS=Integer.parseInt(PropertiesUtil.getValue("lucene.counts"));
}
