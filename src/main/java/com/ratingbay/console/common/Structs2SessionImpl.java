package com.ratingbay.console.common;

import java.util.*;

public class Structs2SessionImpl implements ISession{
	public Structs2SessionImpl(){}

	private Map session;
	public Structs2SessionImpl(Map session){
		this.session = session;
	}

	public Map getSession(){
		return this.session;
	}
	public void setSession(Map session){
		this.session = session;
	}

	@Override
	public Object get(String key){
		return this.session.get(key);
	}

	@Override
	public void put(String key, Object value){
		this.session.put(key, value);
	}
}