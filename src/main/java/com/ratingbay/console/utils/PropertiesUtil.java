package com.ratingbay.console.utils;

import java.io.IOException;
import java.util.Properties;

/**
 * 
 *  Copyright (c) 2009 by vastech.
 *
 * @author peter
 * @version 1.0
 * @date 下午2:47:11 2014-5-13
 */
public class PropertiesUtil {
	private static Properties p = new Properties();

	/**
	 * 读取properties配置文件信息
	 */
	static{
		try {
			p.load(PropertiesUtil.class.getClassLoader().getResourceAsStream("app.properties"));
		} catch (IOException e) {
			e.printStackTrace(); 
		}
	}
	/**
	 * 根据key得到value的值
	 */
	public static String getValue(String key)
	{
		return p.getProperty(key);
	}
}

