package com.ratingbay.console.utils;

public class Helper{

    public static String getVideoId(String link){
        //http://www.youtube.com/watch?v=x5KJzLsyfBI
        //http://youtu.be/AXfQxWuc-8c?a
        //http://www.youtube.com/embed/R3TG31jSUQg?a?feature=player_detailpage
    	if(null == link){
    		return null;
    	}
        String videoId = "unkown";
        
        int start = 0;
        if( -1 != link.indexOf("youtu") ) {
            String prefix = "http://www.youtube.com/watch?v=";
            if( -1 != (start = link.indexOf(prefix)) ){
                videoId = link.substring(start+prefix.length(), start+prefix.length()+11);
            }else{
                prefix = "http://youtu.be/";
                if( -1 != (start = link.indexOf(prefix)) ){
                    videoId = link.substring(start+prefix.length(), start+prefix.length()+11);
                }else{
                    prefix = "http://www.youtube.com/embed/";
                    if( -1 != (start = link.indexOf(prefix)) ){
                        videoId = link.substring(start+prefix.length(), start+prefix.length()+11);
                    }
                }
            }
        }
        return videoId;
    }

}