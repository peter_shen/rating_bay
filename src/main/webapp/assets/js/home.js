var Ratingbay = {
    "TopVideos" : {
        "initImageAndTitle" : function(paramJson){
            var targetNode = $(paramJson['trigger']).find("li");
            targetNode.each(function(index, val){
                var oneNode = this;
                $(oneNode).hide();
                var videoLink = $(this).attr("BRT_video_link");
                var videoId = RatingbayUtils['getVideoId'](videoLink);
                RatingbayUtils['getVideoInfo'](videoId, function(image, title){
                   $(oneNode).find("[BRT_class='title']").html(title.substr(0, 20));
                   $(oneNode).find("[BRT_class='image']").attr('src', image);
                   var oldLink = $(oneNode).find("[BRT_class='link']").attr('href');
                   $(oneNode).find("[BRT_class='link']").attr('href', oldLink+'&videoId='+videoId);
                   $(oneNode).show();
                });
            });
        }
    },
    "TopRecentGames" : {
        "goToLoved" : function(){
            $("ul#topRecentGamesTab").find("li").removeClass('active');
            $("ul#topRecentGamesTab").find("li.l-tab").addClass('active');
            $("#topRecentGamesTweeted").hide();
            $("#topRecentGamesLoved").show();
        },
        "goToTweeted" : function(){
            $("ul#topRecentGamesTab").find("li").removeClass('active');
            $("ul#topRecentGamesTab").find("li.r-tab").addClass('active');
            $("#topRecentGamesLoved").hide();
            var $tweeted = $("#topRecentGamesTweeted");
            if(!$tweeted.data('isContentLoaded')){
                rt_loadTabContent($tweeted);
            }else{
                $tweeted.show();
            }
        }
    },
    "LatestTweets" : {
        "goToRecent" : function(){
            $("ul#latestTweetsTab").find("li").removeClass('active');
            $("ul#latestTweetsTab").find("li.r-tab").addClass('active');
            $("#latestTweetsPopular").hide();
            $("#latestTweetsRecent").show();
        },
        "goToPopular" : function(){
            $("ul#latestTweetsTab").find("li").removeClass('active');
            $("ul#latestTweetsTab").find("li.l-tab").addClass('active');
            $("#latestTweetsRecent").hide();
            $("#latestTweetsPopular").show();
        }
    },
    "SlideImage" : {
        "curIndex" : 0,
        'maxLength' : 0,
        "goToNext" : function(){
            var curIndex = Ratingbay["SlideImage"]["curIndex"];
            var maxLength = Ratingbay["SlideImage"]["maxLength"];
            if(0 == maxLength){
                maxLength = $("ul[BRT_exer='SlideImage'] li").length;
                Ratingbay["SlideImage"]["maxLength"]= maxLength;
            }
            var nextIndex = (curIndex+1) % maxLength;

            $("ul[BRT_exer='SlideImage'] li").each(function(index, val){
                if(index == nextIndex){
                    $(this).show();
                    Ratingbay["SlideImage"]["curIndex"] = nextIndex;
                }else{
                    $(this).hide();
                }
            });
            $("ul.slide_content").each(function(index, val){
                if(index == nextIndex){
                    $(this).show();
                    Ratingbay["SlideImage"]["curIndex"] = nextIndex;
                }else{
                    $(this).hide();
                }
           });
        }
    }
}