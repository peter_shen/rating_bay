var RatingbayUtils = {
  "getVideoId" : function(videoLink){
      var videoId = "unkown";
      if(!videoLink){
         return videoId;
      }
      var start = 0;
      if(-1 != videoLink.indexOf("youtu")){
          var prefix = "http://www.youtube.com/watch?v=";
          if( -1 != (start = videoLink.indexOf(prefix)) ){
              videoId = videoLink.substr(start+prefix.length, 11);
          }else{
              prefix = "http://youtu.be/";
              if( -1 != (start = videoLink.indexOf(prefix)) ){
                  videoId = videoLink.substr(start+prefix.length, 11);
              }else{
                  prefix = "http://www.youtube.com/embed/";
                  if( -1 != (start = videoLink.indexOf(prefix)) ){
                      videoId = videoLink.substr(start+prefix.length, 11);
                  }else{
                	  prefix = "https://www.youtube.com/watch?v=";
                      if( -1 != (start = videoLink.indexOf(prefix)) ){
                          videoId = videoLink.substr(start+prefix.length, 11);
                      }
                  }
              }
          }
      }
      return videoId;
  },
  "getVideoInfo": function(videoId, callback){
      var url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyAvQFniOjWtYZPRmpqZTETWpU-6jOuOK2Q&id='+videoId;
      $.get(url, function(response){
        if(response.items.length){
          var data = response.items[0].snippet;
          var title = data.title;
          var image = data.thumbnails.medium.url;
          var desc = data.description;
          callback(image, title, desc);
        }
      });
  },
  "getVideosInfo":function(ID,callback){
    var api = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&key=AIzaSyAvQFniOjWtYZPRmpqZTETWpU-6jOuOK2Q'
        ,idStr = ID.join(',')
        ;
      api+='&id='+idStr;
      $.get(api,function(data){
        if(data){
          callback(data.items)
        }
      })

  },
  "checkVideo": function(videoId, callback){
      var url = "https://gdata.youtube.com/feeds/api/videos/" + videoId + "?v=2&alt=json";
      $.getJSON(url, function(response){
          var accessControlItems = response["entry"]["yt$accessControl"];
          for(var itemOne in accessControlItems){
            if("allowed" != itemOne["permission"]){
              if("embed" == itemOne["action"]){
                return callback(false);
              }
            }
          }
          return callback(true);
      });
  }
}

function imgError(ele){
    var type = $(ele).attr("BRT_img")||$(ele).data('type');
    switch(type){
        case 'imgLink':
            $(ele).closest("li").hide();
            break;
        case 'gamePoster':
            $(ele).attr('src', 'assets/images/image_coming_soon.png');
            break;
        case 'imgUser':
        case 'userThumb':
            $(ele).attr('src', 'assets/images/thumb/default_user_thumb_120x120.jpg');
            break;
    }
    ele.onerror = null;
}

(function(){
    //remove bad tweet dom
    var targetNode = document.getElementById("blockquote_387786728066789376");
    if(targetNode)targetNode.parentNode.removeChild(targetNode);
    targetNode = document.getElementById("blockquote_384773679894130689");
    if(targetNode)targetNode.parentNode.removeChild(targetNode);

    //register signin action
    $(document).ready(function(){
       $("#signinFormA").click(function(){
           $("#signinForm").submit();
       });
    });
})();

//dispach page registered action
$(document).ready(function(){
    $("[BRT_event]").each(function(index, val) {
        var eventName = $(this).attr('BRT_event');
        var callName = $(this).attr("BRT_call");
        var exerName = $(this).attr("BRT_exer");
        var paramStr = $(this).attr("BRT_param");
        if(undefined == paramStr){
            var paramJson = {};
        }else{
            var paramJson = $.parseJSON(paramStr);
        }
        paramJson['trigger'] = this;

        switch(eventName){
            case "click":
                $("[BRT_event='click']").css("cursor", "pointer");
                $(this).click(function(event) {
                    Ratingbay[exerName][callName](paramJson);
                });
                break;

            case "timeout":
                setInterval(function(){
                    Ratingbay[exerName][callName](paramJson);
                }, paramJson['time'] * 1000);
                break;

            case "init":
                Ratingbay[exerName][callName](paramJson);
                break;
        }
    });
});