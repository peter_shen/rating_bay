$(function(){
	var container = $('.big-slide-box')
		,box = $('.big-slide',container)
		,slides = $('li',box)
		,nav = null
		,count = slides.length
		,now = -1
		,timer = null
		,a_timer = null
		,t = 5000;

	createNav(count);
	startSlide();

	function createNav(){
		nav = $('<div class="big-slide-nav">');
		for(var i=0;i<count;i++){
			nav.append($('<a class="'+(i==0?'active':'')+'" href="javascript:;">'+(i+1)+'</a>').data('index',i));
		}
		container.append(nav);
		container.on('mouseover','a',function(){
			var index = $(this).data('index');
			a_timer = setTimeout(function(){
				slide(index);
			},200);
		});
		container.on('mouseout','a',function(){
			clearTimeout(a_timer);
		});
	}
	function slide(num,loop){
		if(count&&!isNaN(num)){
			num = num>=count?0:num;
			var li = $(slides[num])
				,intro = $('.slide-intro',li)
				,next = num == count-1?0 : num+1
				;
			if(num != now){
				clearTimeout(timer);
				if(now>-1){
					//hide old slide
					var oldS = $(slides[now]);
					oldS.stop().animate({opacity:0},function(){
						$(this).css('z-index',1);
						li.css('z-index',9);
					});
					$('.slide-intro',oldS).stop().animate({top:'-370px',opacity:0});
				}else{
					li.css('z-index',9);
				}
				// show new slide
				li.stop().css({
					position:'absolute',
					display:'block',
					opacity:1
				});
				intro.stop().animate({top:'60px',opacity:1},'slow','swing');
				now = num;
				// change nav item
				$('.active',nav).removeClass('active');
				nav.find('a:eq('+num+')').addClass('active');
			}
			if(loop){	// slide to next
				timer = setTimeout(function(){slide(next,true)},t);
			}
		}
	}
	function startSlide(){
		slides.each(function(){
			var li = $(this)
				,image = li.data('image')
				,color = li.data('color')
				,link = li.data('link');
			$(this).css({
				position:'absolute',
				display:'block',
				zIndex:'1',
				opacity:0,
				backgroundImage:'url('+image+')',
				backgroundColor:color
			});
			link&&$('.slide-link',li).attr('href',link);
		});
		container.on('mouseenter',function(){
			clearTimeout(timer);
		});
		container.on('mouseleave',function(){
			timer = setTimeout(function(){slide(now+1,true);},t);
		})
		slide(0,true);
	}
});