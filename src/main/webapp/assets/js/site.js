$(function(){
	var container = $('.big-slide-box')
		,box = $('.big-slide',container)
		,slides = $('li',box)
		,nav = null
		,count = slides.length
		,now = -1
		,timer = null
		,a_timer = null
		,t = 5000;

	createNav(count);
	startSlide();

	function createNav(){
		nav = $('<div class="big-slide-nav">');
		for(var i=0;i<count;i++){
			nav.append($('<a class="'+(i==0?'active':'')+'" href="javascript:;">'+(i+1)+'</a>').data('index',i));
		}
		container.append(nav);
		container.on('mouseover','a',function(){
			var index = $(this).data('index');
			a_timer = setTimeout(function(){
				slide(index);
			},200);
		});
		container.on('mouseout','a',function(){
			clearTimeout(a_timer);
		});
	}
	function slide(num,loop){
		if(count&&!isNaN(num)){
			num = num>=count?0:num;
			var li = $(slides[num])
				,intro = $('.slide-intro',li)
				,next = num == count-1?0 : num+1
				;
			if(num != now){
				clearTimeout(timer);
				if(now>-1){
					//hide old slide
					var oldS = $(slides[now]);
					oldS.stop().animate({opacity:0},function(){
						$(this).css('z-index',1);
						li.css('z-index',9);
					});
					$('.slide-intro',oldS).stop().animate({top:'-370px',opacity:0});
				}else{
					li.css('z-index',9);
				}
				// show new slide
				li.stop().css({
					position:'absolute',
					display:'block',
					opacity:1
				});
				intro.stop().animate({top:'60px',opacity:1},'slow','swing');
				now = num;
				// change nav item
				$('.active',nav).removeClass('active');
				nav.find('a:eq('+num+')').addClass('active');
			}
			if(loop){	// slide to next
				timer = setTimeout(function(){slide(next,true)},t);
			}
		}
	}
	function startSlide(){
		slides.each(function(){
			var li = $(this)
				,image = li.data('image')
				,color = li.data('color')
				,link = li.data('link');
			$(this).css({
				position:'absolute',
				display:'block',
				zIndex:'1',
				opacity:0,
				backgroundImage:'url('+image+')',
				backgroundColor:color
			});
			link&&$('.slide-link',li).attr('href',link);
		});
		container.on('mouseenter',function(){
			clearTimeout(timer);
		});
		container.on('mouseleave',function(){
			timer = setTimeout(function(){slide(now+1,true);},t);
		})
		slide(0,true);
	}
});
// 翻看内容板块上下页内容
(function () {

	var prev = $('.browse-prev')
		next = $('.browse-next');

	prev.each(function(index,elem){
		var $elem = $(elem)
			,contUL = $elem.next()
			;
		contUL.data('prev',$elem);
		$elem.data('contUL',contUL);

		$elem.on('click',function(){
			browse($(this));
		});

	});
	next.each(function(index,elem){
		var $elem = $(elem)
			,contUL = $elem.prev()
			;

		contUL.data('next',$elem);
		$elem.data('contUL',contUL);

		$elem.on('click',function(){
			browse($(this));
		});
	});

	function browse($elem){
		var contUL = $elem.data('contUL')
			,pageNum = contUL.data('pageNum')||0
			,url = contUL.data('url')
			;
		if($elem.hasClass('browse-disable')){
			return;
		}else{
			$elem.hasClass('browse-prev')?pageNum--:pageNum++;
			if(pageNum>-1){
				contUL.data('ajax_request')&&contUL.data('ajax_request').abort();	//abort last request
				contUL.data('ajax_request',$.get(url+pageNum+'.do',function(data){
					fillContent(data,contUL);
				}));
			}
		}
	}

	function fillContent(data,container){
		if(data&&data.result){
			var result = data.result
				,html_module = container.data('module')
				,str = ''
				,pageNum = data.page
				,lastPage = container.data('pageNum')||0
				,prev = container.data('prev')
				,next = container.data('next')
				;
			if(data.hasNext){
				next.hasClass('browse-disable')&&next.removeClass('browse-disable');
			}else{
				next.addClass('browse-disable');
			}
			if(data.result.length){
				container.data('pageNum',pageNum);	//设置当前pageNum
				if(lastPage<pageNum){
					prev.hasClass('browse-disable')&&prev.removeClass('browse-disable');
				}
				if(pageNum==0){
					prev.addClass('browse-disable');
				}
				$.each(result,function(index,item){
					var cont_item = html_module;
					$.each(item,function(p,val){
						cont_item = cont_item.replace('{{'+p+'}}',val);
					});
					str+=cont_item;
				});
				container.empty().html(str);
				radialProgress(container);	//显示radial progress
			}
		}
	}

	function loadTabContent ($container){
		if(!$container.data('isContentLoading')){
			var contUL = $container.find('ul.cont-list')
				,url = contUL.data('url')
				,$loadingStat = $('<p class="loading-status" style="text-align:center">').html('Loading...');
			$.get(url+'0.do',function(data){
				rt_fillTabContent(data,contUL);
				$container.slideDown('slow');
				$loadingStat.remove();
				$container.data('isContentLoaded',true);
			});
			$container.parent().append($loadingStat);
			$container.data('isContentLoading',true);
		}
	}

	var dropdown = function(){	//create dropdown
		var $elem = $('.dropdown');
		$elem.each(function(i,elem){
			var $menu = $('.dropdown-menu',$(elem));
			if($menu.length){
				$(elem).on('mouseover',function(e){
					clearTimeout($(this).data('timer'));
					$menu.show();
				});
				$(elem).on('mouseout',function(){
					clearTimeout($(this).data('timer'));
					$(this).data('timer',setTimeout(function(){
						$menu.hide();
					},200))
				});
			}
		});
	}
	dropdown();
	window.rt_fillTabContent = fillContent;
	window.rt_loadTabContent = loadTabContent;

})();

// share to social media

var shareToSocial = function(type,link){
	var apis = {
		facebook:{
			url:'https://www.facebook.com/sharer/sharer.php',
			plink:'u'
		},
		tweet: {
			url:'https://twitter.com/share',
			plink:'url'
		},
		googleplus:{
			url:'https://plus.google.com/share',
			plink:'url'
		}
	}
	link = encodeURIComponent(link||location.href);
	var shareUrl = apis[type]['url']+'?'+apis[type]['plink']+'='+link;
	window.open(shareUrl);
}
$('.share-buttons .share-btn').on('click',function(){
	shareToSocial($(this).data('type'));
})
//global top search
var search_flag = $('#g-search .s-flag')
 ,search_box = $('#s-opts-box')
 ,search_opts = $('.s-opts',search_box)
 ,search_input = $('#keyword')
 ,search_btn = $('#g-search .s-btn-search')
 ,gotop = $('#gotop')
 ,scroll_timer = null
 ,maxScrollTop = 400
 ,input_timer = null
 ,search_ajaxRequest = null
 ;
$('#g-search').on('click',function(event){
	event.stopPropagation();
});
$(document).on('click',function(){
	search_box.hide();
})
search_btn.on('click',function(event){
	$('#g-search form').submit();
});
search_flag.on('click',function(event){
 toggleFlag(event);
 search_input.focus();
});
search_input.on('focus',function(event){
 toggleOpts();
});
search_input.on('blur',function(event){
 toggleFlag(event);
});
search_input.on('keydown',function(event){
	chooseItemBykey(event);
	var keyCode = event.which;
	if(keyCode == '38'||keyCode == '40'||keyCode == '13'){
		return;
	}
	toggleFlag(event);
 	toggleOpts();
});
search_input.on('keyup',function(event){
	var keyCode = event.which;
	if(keyCode == '38'||keyCode == '40'){
		return;
	}
	input_timer = setTimeout(function(){
		clearTimeout(input_timer);
		toggleOpts(event.which);
	},200);
});
search_box.on('keydown',chooseItemBykey);
$(document).on('mouseover','#s-opts-box li.opts-item',function(e){
	search_opts.find('li.selected').removeClass('selected');
	$(this).addClass('selected');
});
function chooseItemBykey(event){
 var keyCode = event.which;
 if(keyCode == '38'||keyCode == '40'||keyCode == '13'){
  event.preventDefault();
 }
 switch(keyCode){
  case 38:
   choosePrev();
   break;
  case 40:
   chooseNext();
   break;
  case 13:
   var s_link = $('li.selected',search_opts).length?$('li.selected a',search_opts):$('.s-game a',search_opts);
   location.href = s_link.attr('href');
   break;
 }
 function choosePrev () {
  var $now = $('li.selected',search_opts)
   ,$prev = $now.prev().length?$now.prevAll('.opts-item:eq(0)'):$('li:last',search_opts);
  $now.removeClass('selected');
  $prev.addClass('selected');
 }
 function chooseNext () {
  var $now = $('li.selected',search_opts)
   ,$next = $now.next().length?$now.nextAll('.opts-item:eq(0)'):$('li:first',search_opts);
  $now.removeClass('selected');
  $next.addClass('selected');
 }
}
function toggleOpts(){
	var text = $.trim(search_input.val())
		,ulink = $('.s-user',search_opts).data('link')
		,glink = $('.s-game',search_opts).data('link')
		,uem = $('.s-user strong',search_opts)
		,gem = $('.s-game strong',search_opts)
		;
	if(text!=''){
		search_ajaxRequest&&search_ajaxRequest.abort();
		var api = '/rest/game/searchGamesAndUsers/'+text+'.do';
		$.get(api,function(data){
			if(data&&data.game){
				autoComplete(data);
			}
		});
		uem.text(text);
		gem.text(text);
		$('.s-user a',search_opts).attr('href',ulink+text);
		$('.s-game a',search_opts).attr('href',glink+text);
		search_box.fadeIn();
		!search_opts.find('li.selected').length&&search_opts.find('li:eq(0)').addClass('selected');
	}else{
		search_box.fadeOut();
	}
	function autoComplete(data){
		$('.opts-recommand-item',search_box).remove();
		var str = '';
		$.each(data.game,function(i,val){
			str += '<li class="opts-recommand-item opts-item"><a href="/game.action?gameId='+val.gameId+'"><h5>'+val.gameName+'</h5></a></li>'
		});
		$('.s-game',search_opts).after(str);
	}
}

function toggleFlag (event) {
 var text = $.trim(search_input.val());
 switch(event.type){
  case "keydown":
   search_flag.hide();
   break;
  case "blur":
   if(text==''){
    search_input.val('');
    search_flag.show();
   }
   break;
 }
}
// game category
$('#gamecat-all').on('mouseenter',function(){
	this.mtimer&&clearTimeout(this.mtimer);
	$(this).find('.category-icon').addClass('active');
	$('#gamecat-list-box').stop(true,true).fadeIn();
});
$('#gamecat-all').on('mouseleave',function(){
	var $this = $(this);
	this.mtimer = setTimeout(function(){
		$this.find('.category-icon').removeClass('active');
		$('#gamecat-list-box').stop(true,true).fadeOut();
	},200);
});

$('#gamecat-list-box a.gamecat-item').on('click',function(){
	var $this = $(this)
		,cat = $this.data('category')||$this.text()
		,url = "category.action?"+($this.hasClass('genre-item')?"ge="+cat:"p="+cat)
		;
	location.href = url;
});

$(window).on('scroll',function(e){
	clearTimeout(scroll_timer);
	var st = $(window).scrollTop();
	scroll_timer = setTimeout(function(){
		if(st>maxScrollTop){
		gotop.fadeIn();
		}else{
		gotop.fadeOut();
		}
	},50);
	/*var h = $('#g-quic-nav').outerHeight();
	var t = st>h?0:h-st;
	$('#g-top-nav').css('top',t);*/
});

gotop.on('click',function(){
 $(window).scrollTop(0);
});

var toggleDesc = function(elem){
 var $p = $(elem).parent()
  ;
 if($.trim($(elem).text())=='Expand'){
  $('.brief',$p).hide();
  $('.details',$p).show();
  $(elem).text('Collapse');
 }else{
  $('.details',$p).hide();
  $('.brief',$p).show();
  $(elem).text('Expand');
 }
}
//游戏评分
function radialProgress($container){
	var $radials;
	if($container){
		$radials = $('.radial-progress',$container);
	}else{
		$radials = $('.radial-progress');
	}
    $radials.each(function(i,item){
        var $item = $(item)
            ,score = $item.data('score')
            ,type = $item.data('type')
            ,half = score;
	    if($item.data('complete'))
	    	return;
	    $item.html('<div class="circle"><div class="mask half"><div class="fill" style="-webkit-transform: rotate(180deg);"></div></div><div class="mask full"><div class="fill"></div></div></div><div class="inset"><span>'+score+'%</span></div>');

	    if(score>50){
	        var angel = Math.floor((score-50)*3.6);
	        half = 50;
	        $('.full .fill',$item).css({'transform':'rotate('+angel+'deg)'});
	    }
	    var angel = Math.floor(half*3.6);
	    $('.half .fill',$item).css({'transform':'rotate('+angel+'deg)'});
	    if(type=='overImage'){
	    	$item.css({'position': 'absolute',top:'-10px',right:'-10px'});
	    }
	    $item.show().data('complete',true);
    });
}

//	signinDialog Class
var signinDialog =  function(){
	var str = '<div id="signin-dialog-wrapper"> <div class="signin-dialog-title"> <a href="/home.action" class="dialog-logo"> </a> <sub class="dialog-slogan"> Find what your friends say about games </sub> <a href="javascript:;" title="close" class="btn close-btn">×</a> </div> <div class="signin-dialog-action"> <a href="javascript:;" class="btn signin-twitter-lg"> signin width twitter </a> <div class="tweetRB"> <input type="checkbox" name="tweetRB" value="true"/> <small> Tweet "I\'m using http://ratingbay.com/user/username to see what my friends say about games. Check it out http://ratingbay.com"</small> </div> <div class="nosignin"> <a href="javascript:;">No,thanks,just browsing</a> </div> </div> </div>';
	$dialog = this.dialog = $(str);
	$dialog.appendTo($('body'));
	$('.close-btn',$dialog).on('click',closeDialog);
	$('.nosignin',$dialog).on('click',closeDialog);
	$('.signin-dialog-action',$dialog).find('a.signin-twitter-lg').on('click',function(){
		if($('input[name="tweetRB"]:checked',$dialog).length){
			var tweetRB = $('input[name="tweetRB"]:checked',$dialog).val();
			$.setCookie('rt_willingTweetRB',1,1);
			$("#signinForm").append('<input name="willingTweetRB" type="hidden" value="'+tweetRB+'">');
		}
		$("#signinForm").submit();
	});
	function createModalOverlay(){
		var $overlay = $('<div>')
			,w = window.screen.availWidth
			,h = window.screen.availHeight
			;
		$overlay.css({
			backgroundColor:'rgba(41,47,51,.90)',
			opacity:0.6,
			position:'fixed',
			top:0,
			bottom:0,
			left:0,
			right:0,
			zIndex:9999
		});
		$overlay[0].style.cssText+=';-ms-filter: alpha(opacity=90)';
		$('body').append($overlay);
		this.overlay = $overlay;
	};

	function destroyModalOverlay(){
		this.overlay.remove();
		$('html').css('overflowY','auto');
	}
	function openDialog(){
		createModalOverlay();
		$dialog.show();
	}
	function closeDialog(){
		$dialog.fadeOut('slow', function() {
			destroyModalOverlay();
		});
	}
	this.openDialog = openDialog;
	this.closeDialog = closeDialog;
	this.removeDialog = function(){
		$dialog.remove();
		destroyModalOverlay();
	}
}


//user page tab
var rt_navtab = function($navtabs,callback){

	$navtabs.each(function(index,navtab){
		var $navtab = $(navtab)
			,$tabs = $('li',$navtab).length?$('li',$navtab):$('a',$navtab)
			;
		$tabs.each(function(i,tab){
			var $tab = $(tab);
			!$(tab).data('hasBindClick')&&$tab.on('click',function(e){
				$(tab).data('hasBindClick',true);
				e.preventDefault();
				exchangeTab($navtab,$(this),callback);
			})
		});
    	exchangeTab($navtab,$('.active',$navtab));
	});

    function exchangeTab($navtab,$tab,callback){
    	var $tabContent = $tabpane = $old_tabpane = null;
    	if($tab.attr('target')){
    		$tabContent = $navtab.parent().nextAll('.tab-content:eq(0)');
    		$tabpane = $($tab.attr('target'),$tabContent);
    		$old_tabpane = $('.active',$tabContent);
    	}else if($('a',$tab).attr('href')){
    		$tabpane = $($tab.find('a').attr('href'));
    		$old_tabpane = $($('.active',$navtab).find('a').attr('href'));
    	}
		$('.active',$navtab).removeClass('active');
		$old_tabpane.hasClass('active')&&$old_tabpane.removeClass('active');
		$tab.addClass('active');
		$tabpane.addClass('active');
		callback&&callback($tabpane);
	}
};

// request data and fill content when click tab
var rt_requestTabcontent = function($container){

    if($container.data('hasClicked'))
        return;
    var url = $container.data('url')
        ,type = $container.data('type')
        ,html_module = $container.data('module')
        ;
    if(url&&html_module){
        $container.data('stopLoading',true);
		$container.data('hasClicked',true);
        var $loadingStat = $('<li class="loading-status" style="display:block;float:none;text-align:center;width:100%;">').html('Loading...');
        $container.append($loadingStat);
        $('.scroll-loading-status').remove();	//移除所有scroll-loading-status
        $.get(url,function(data){
            if(data&&data.result.length){
            	$loadingStat.remove();
                if(type=='video'){
                    $.each(data.result,function(index,obj){
                    	var vid_arr = [];
                        vid_arr.push(RatingbayUtils['getVideoId'](obj.mediaDirectLink));
                        RatingbayUtils['getVideosInfo'](vid_arr,function(items){
                        	if(items.length&&items[0].snippet){
	                        	var video = items[0].snippet;
	                        	obj.videoId = items[0].id;
	                            obj.image = video.thumbnails.medium.url;
	                            obj.title = video.title;
	                            obj.desc = video.description;
	                            fillContent($container,obj);
                            }
                        })
                    })

                }else{
                    fillContent($container,data.result);
                }
	        	$container.data('stopLoading',false);
            }else{
            	$container.html('<li style="padding: 25px;text-align: center; float: none; width: auto;">sorry,there is no content</li>');
            }
        });
    }
}
var fillGameTabs = fillContent = function($container,result){
    var html_module = $container.data('module')
    	type = $container.data('type')
        ,str = ''
        ;

    if($.isArray(result)){
        $.each(result,function(i,obj){
            str += replaceHtmlStr(obj,html_module);
        });
    }else{
        str = replaceHtmlStr(result,html_module);
    }
    $container.append($(str)).data('loaded',true);
    radialProgress();
    if(type=='tweets'){
    	$('body').append('<script type="text/javascript" charset="utf-8" src="http://platform.twitter.com/widgets.js">');
    }
    function replaceHtmlStr(obj,htmlStr){
        var reg_ite = /\{\{\{\[([\w]+)\](.+)\}\}\}/g
            ,res = null
            ;
        $.each(obj,function(p,val){
            while(res=reg_ite.exec(htmlStr)){
                var ite_htmlStr = res[0]
                    ,ite_param = res[1]
                    ,ite_html_module = res[2]
                    ,ite_str='';
                if(obj[ite_param]){
                    $.each(obj[ite_param],function(i,val){
                        ite_str += replaceHtmlStr(val,ite_html_module);
                    })
                }
                htmlStr = htmlStr.replace(ite_htmlStr,ite_str);
            }
            if(p=='desc'){  //desc属性做截断
                htmlStr = htmlStr.replace('{{'+p+'Ori}}','<span class="details">'+val+'</span>');
                val = val.length>220?'<span class="brief">'+val.substr(0,215)+'...</span><a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>':'<span class="brief">'+val+'</span>';
            }
            var reg = new RegExp('\{\{'+p+'\}\}','g');
            htmlStr = htmlStr.replace(reg,val);
        });
        return htmlStr;
    }
}

//set signin redirUrl
$("#signinForm").submit(function(){
	var url = window.location.href;
    var st = url.replace('http://www.ratingbay.com/console/','/');
    $.setCookie("ratingbayPostSignInUrl",st,1);
 });

//add twiiter script to page
var addTwiiterScript = function(){
	$('body').append('<script type="text/javascript" charset="utf-8" src="http://platform.twitter.com/widgets.js">');
}

$.setCookie = function(name,value,expires){

    var argv = arguments;
    var expires = (argv > 2) ? argv[2] : null;
    if(expires!=null)
    {
        var LargeExpDate = new Date ();
        LargeExpDate.setTime(LargeExpDate.getTime() + (expires*1000*3600*24));
    }
    document.cookie = name + "=" + escape (value)+((expires == null) ? "" : ("; expires=" +LargeExpDate.toGMTString()));
}
$.getCookie = function(name){
    var search = name + "="
    if(document.cookie.length > 0)
    {
        offset = document.cookie.indexOf(search)
        if(offset != -1){
            offset += search.length
            end = document.cookie.indexOf(";", offset)
            if(end == -1) end = document.cookie.length
            return unescape(document.cookie.substring(offset, end))
        }
        else return ""
    }
}
// scrolling load game list
var scrollingLoad = function(callback){
	if($('.scrolling-loading').length){
		$(window).on('scroll',function(){
			$('.scrolling-loading').each(function(i,item){
				var $item = $(item);
				clearTimeout($item.data('timer'));
				$item.data('timer',setTimeout(function(){
					$item.is(':visible')&&!$item.data('stopLoading')&&scrolling($item);
				},200));
			});
		});
	}
	var scrolling = function(container){
		var H = container.offset().top+container.height()
			,bodyH = $(document.body).height()
			,st = $(window).scrollTop();

		if(st+bodyH>=H){
			loading(container);
		}
	}
	var loading = function(container){

		var statusDOM = $('<p class="scroll-loading-status" style="text-align:center;">').html('Loading content...')
			,pageNum = container.data('pageNum')||1
			,type = container.data('type')
			,apiUrl = container.data('url')
			,url = (apiUrl.indexOf('.do')>-1?apiUrl.substring(0,apiUrl.lastIndexOf('/')+1):apiUrl)+pageNum+'.do'
			;

		container.parent().append(statusDOM);
		container.data('stopLoading',true);
		var req = $.get(url,function(data){
			if(data&&data.result){
				container.data('stopLoading',false);
				container.data('pageNum',pageNum+1);
				if(callback){
	                if(type=='video'){
	                    $.each(data.result,function(index,obj){
	                    	var vid_arr = [];
	                        vid_arr.push(RatingbayUtils['getVideoId'](obj.mediaDirectLink));
	                        RatingbayUtils['getVideosInfo'](vid_arr,function(items){
	                        	if(items.length&&items[0].snippet){
		                        	var video = items[0].snippet;
		                        	obj.videoId = items[0].id;
		                            obj.image = video.thumbnails.medium.url;
		                            obj.title = video.title;
		                            obj.desc = video.description;
		                            callback(container,obj);
	                            }
	                        })
	                    })

	                }else{
	                	callback(container,data.result);
	                }
				}
				if(!data.hasNext){
					container.data('stopLoading',true);
					statusDOM.text("It's the end!").delay(5000).fadeOut();
				}else{
					statusDOM.remove();
				}
			}
		});
	return req;
	}
}

function fillUserTabs(container,result){
	var htmlStr = '';
	$.each(result,function(i,val){
		var tweetStr = '';
		val.latestTweet&&$.each(val.latestTweet,function(i,val){
			tweetStr+='<li class="clearfix"> <blockquote class="twitter-tweet" width="900" id="blockquote_'
				+val.id
				+'"><a href="https://twitter.com/twitterapi/status/'
				+val.id
				+'"></a></blockquote></li>';
		});
		if(val.score!=0){
		htmlStr += '<li class="clearfix gamesAboutOne"> <div class="l"> <div class="game-thumb"> <div class="image-box"> <a class="thumb" href="game.action?gameId='
	        +val.id
	        +'"> <img src="'
	        +val.dvdImagePath
	        +'" data-type="gamePoster" onerror="imgError(this)" alt=""/> </a> </div> <div class="radial-progress" data-type="overImage" data-score="'
	        +val.score
	        +'"></div><span class="tweets">'
	        +val.tweetCount
	        +' Tweets</span> </div> <p class="game-name"> <a href="game.action?gameId='
	        +val.id
	        +'"> <strong>'
	        +val.title
	        +'</strong></a></p></div><div class="r"> <div class="t-action"> <span class="tweet-action"> <a href="javascript:;" target=".gamesAboutTweets" class="tweet active"> <em></em>Tweets </a> <a href="javascript:;" target=".gamesAboutVideos" class="video"> <em></em>Videos </a> <a href="javascript:;" target=".gamesAboutImages" class="image"> <em></em>Images </a> <a href="javascript:;" target=".gamesAboutLinks" class="links"> <em></em>Links </a> </span> </div> <div class="tab-content"> <ul class="tw-list active gamesAboutTweets"> '
	        +tweetStr
	        +'</ul> <ul class="list tw-list image-list gamesAboutVideos" data-url="/rest/link/userVideoLinksByGame/'
	        +val.gameUserId
	        +'/'
	        +val.id
	        +'/0.do" data-type="video" data-module=\'<li class="clearfix"> <div class="l"> <a class="thumb" href="game.action?gameId={{gameId}}"><img src="{{image}}" /> <b class="s-v-play"></b> </a> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc"">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"></a>}}}</label></div> </div> </li>\'> </ul> <ul class="list tw-list images-list-wgth gamesAboutImages clearfix" data-url="/rest/link/userImageLinksByGame/'
	        +val.gameUserId
	        +'/'
	        +val.id
	        +'/0.do" data-module=\'<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}" BRT_img="imgLink" onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div></li>\'> </ul> <ul class="list tw-list links-list gamesAboutLinks" data-url="/rest/link/userLinksByGame/'
	        +val.gameUserId
	        +'/'
	        +val.id
	        +'/0.do" data-module=\'<li class="clearfix"> <div class="intro"> <div class="title"> <a target="_blank" href="{{link}}"> <strong> {{link}} </strong> </a> </div> </div> </li>\'> </ul> </div> </div> </li>';
		}else{
			htmlStr += '<li class="clearfix gamesAboutOne"> <div class="l"> <div class="game-thumb"> <div class="image-box"> <a class="thumb" href="game.action?gameId='
		        +val.id
		        +'"> <img src="'
		        +val.dvdImagePath
		        +'" data-type="gamePoster" onerror="imgError(this)" alt=""/> </a> </div> </div><span class="tweets">'
		        +val.tweetCount
		        +' Tweets</span> </div> <p class="game-name"> <a href="game.action?gameId='
		        +val.id
		        +'"> <strong>'
		        +val.title
		        +'</strong></a></p></div><div class="r"> <div class="t-action"> <span class="tweet-action"> <a href="javascript:;" target=".gamesAboutTweets" class="tweet active"> <em></em>Tweets </a> <a href="javascript:;" target=".gamesAboutVideos" class="video"> <em></em>Videos </a> <a href="javascript:;" target=".gamesAboutImages" class="image"> <em></em>Images </a> <a href="javascript:;" target=".gamesAboutLinks" class="links"> <em></em>Links </a> </span> </div> <div class="tab-content"> <ul class="tw-list active gamesAboutTweets"> '
		        +tweetStr
		        +'</ul> <ul class="list tw-list image-list gamesAboutVideos" data-url="/rest/link/userVideoLinksByGame/'
		        +val.gameUserId
		        +'/'
		        +val.id
		        +'/0.do" data-type="video" data-module=\'<li class="clearfix"> <div class="l"> <a class="thumb" href="game.action?gameId={{gameId}}"><img src="{{image}}" /> <b class="s-v-play"></b> </a> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc"">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"></a>}}}</label></div> </div> </li>\'> </ul> <ul class="list tw-list images-list-wgth gamesAboutImages clearfix" data-url="/rest/link/userImageLinksByGame/'
		        +val.gameUserId
		        +'/'
		        +val.id
		        +'/0.do" data-module=\'<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}" BRT_img="imgLink" onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div></li>\'> </ul> <ul class="list tw-list links-list gamesAboutLinks" data-url="/rest/link/userLinksByGame/'
		        +val.gameUserId
		        +'/'
		        +val.id
		        +'/0.do" data-module=\'<li class="clearfix"> <div class="intro"> <div class="title"> <a target="_blank" href="{{link}}"> <strong> {{link}} </strong> </a> </div> </div> </li>\'> </ul> </div> </div> </li>';

		}
	});
	$(htmlStr).appendTo(container);
	rt_navtab($('.tweet-action'),rt_requestTabcontent);	//tab绑定click事件
	radialProgress(container);	//显示游戏评分
	addTwiiterScript();	//显示twitter组件
}
/*if(RatingBay.isLogined === false&&(!$.getCookie('rt_openDialogOnce')||$.getCookie('rt_openDialogOnce')==0)){
	var dialog = new signinDialog();
	dialog.openDialog();
	$.setCookie('rt_openDialogOnce',1,1);
}*/
//显示游戏评分
radialProgress();
if($('#t-user-profile').length){	//user页面scrolling load data
	scrollingLoad(fillUserTabs);
}
if($('#t-game-profile').length){	//game页面scrolling load data
	scrollingLoad(fillGameTabs);
}





