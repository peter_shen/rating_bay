var Ratingbay = {
    "gamesAboutVideos" : {
        "initImageAndTitle" : function(paramJson){
            var targetNode = $(paramJson['trigger']).find("[BRT_class='gameAboutVideoOne']");
            targetNode.each(function(index, val){
                var oneNode = this;
                $(oneNode).hide();
                var videoLink = $(oneNode).attr("BRT_video_link");
                var videoId = RatingbayUtils['getVideoId'](videoLink);
                RatingbayUtils['getVideoInfo'](videoId, function(image, title, desc){
                   $(oneNode).find("[BRT_class='title']").html(title);
                   $(oneNode).find("[BRT_class='desc']").html(desc);
                   $(oneNode).find("[BRT_class='image']").attr('src', image);
                   var oldLink = $(oneNode).find("[BRT_class='link']").attr('href');
                   $(oneNode).find("[BRT_class='link']").attr('href', oldLink+'&videoId='+videoId);
                   $(oneNode).show();
                });
            });
        }
    },
    "Games":{
        "goToUser" : function(paramJson){
            $(paramJson['trigger']).parent().find("li").removeClass('active');
            $(paramJson['trigger']).addClass('active');
            $("[BRT_id='userGame']").show();
            $("[BRT_id='friendGame']").hide();
        },
        "goToFriends": function(paramJson){
            $(paramJson['trigger']).parent().find("li").removeClass('active');
            $(paramJson['trigger']).addClass('active');
            $("[BRT_id='userGame']").hide();
            $("[BRT_id='friendGame']").show();
        }
    }
}

rt_navtab($('.tabs-hasborder'),rt_requestTabcontent);
rt_navtab($('.tweet-action'),rt_requestTabcontent);
