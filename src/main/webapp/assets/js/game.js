var Ratingbay = {
        "Videos" : {
                "goToPopular" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#videosFriends").hide();
                        $("#videosPopular").show();
                },

                "goToFriends" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#videosPopular").hide();
                        $("#videosFriends").show();
                }
        },
        "Images" : {
                "goToPopular" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#imagesFriends").hide();
                        $("#imagesPopular").show();
                },

                "goToFriends" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#imagesPopular").hide();
                        $("#imagesFriends").show();
                }
        },

        "Links" : {
                "goToPopular" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#linksFriends").hide();
                        $("#linksPopular").show();
                },

                "goToFriends" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#linksPopular").hide();
                        $("#linksFriends").show();
                }
        },

        "Tweets" : {
                "goToPopular" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#tweetsFriends").hide();
                        $("#tweetsPositive").hide();
                        $("#tweetsNagative").hide();
                        $("#tweetsLatest").hide();
                        $("#tweetsPopular").show();
                },

                "goToFriends" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#tweetsPopular").hide();
                        $("#tweetsPositive").hide();
                        $("#tweetsNagative").hide();
                        $("#tweetsLatest").hide();
                        $("#tweetsFriends").show();
                },

                "goToPositive" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#tweetsPopular").hide();
                        $("#tweetsFriends").hide();
                        $("#tweetsNagative").hide();
                        $("#tweetsLatest").hide();
                        $("#tweetsPositive").show();
                },

                "goToNagative" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#tweetsPopular").hide();
                        $("#tweetsFriends").hide();
                        $("#tweetsPositive").hide();
                        $("#tweetsLatest").hide();
                        $("#tweetsNagative").show();
                },
                "goToLatest" : function(paramJson){
                        $(paramJson['trigger']).parent().find("li").removeClass('active');
                        $(paramJson['trigger']).addClass('active');
                        $("#tweetsPopular").hide();
                        $("#tweetsFriends").hide();
                        $("#tweetsPositive").hide();
                        $("#tweetsNagative").hide();
                        $("#tweetsLatest").show();
                }
        },
        "VideoLinks" : {
                "popularVideoLinks" : function(paramJson){
                        var targetNode = $(paramJson['trigger']).find("li");
                        targetNode.each(function(index, val){
                                var oneNode = this;
                                $(oneNode).hide();
                                var videoLink = $(oneNode).attr("BRT_video_link");
                                var videoId = RatingbayUtils['getVideoId'](videoLink);
                                RatingbayUtils['getVideoInfo'](videoId, function(image, title, desc){
                                    $(oneNode).find("[BRT_class='title']").html(title);
                                    $desc = $(oneNode).find("[BRT_class='desc']");
                                    $desc.data('desc',desc).html(desc.length>225?desc.substr(0,220)+'...<a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>':desc);
                                    $(oneNode).find("[BRT_class='image']").attr('src', image);
                                    var oldLink = $(oneNode).find("[BRT_class='link']").attr('href');
                                    $(oneNode).find("[BRT_class='link']").attr('href', oldLink+'&videoId='+videoId);
                                    $(oneNode).show();
                                });
                        });
                },
                "friendsVideoLinks" : function(paramJson){
                        var targetNode = $(paramJson['trigger']).find("li");
                        targetNode.each(function(index, val){
                                var oneNode = this;
                                $(oneNode).hide();
                                var videoLink = $(oneNode).attr("BRT_video_link");
                                var videoId = RatingbayUtils['getVideoId'](videoLink);
                                RatingbayUtils['getVideoInfo'](videoId, function(image, title, desc){
                                    $(oneNode).find("[BRT_class='title']").html(title);
                                    $desc = $(oneNode).find("[BRT_class='desc']");
                                    $desc.data('desc',desc).html(desc.length>225?desc.substr(0,220)+'...<a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>':desc);
                                    $(oneNode).find("[BRT_class='image']").attr('src', image);
                                    var oldLink = $(oneNode).find("[BRT_class='link']").attr('href');
                                    $(oneNode).find("[BRT_class='link']").attr('href', oldLink+'&videoId='+videoId);
                                    $(oneNode).show();
                                });
                        });
                }
        }
}
$(document).ready(function(){
    $("#gameIntroFold").click(function(event) {
        var state = $("#gameIntroCont").attr('state');
        if(state == "folded"){
            $("#gameIntroCont").css({'height': 'auto'});
            $("#gameIntroCont").attr('state', 'unfold');
        }else{
            $("#gameIntroCont").css({'height': '25px'});
            $("#gameIntroCont").attr('state', 'folded');
        }
    });

    rt_navtab($('.tabs-hasborder'),rt_requestTabcontent);   //点击tab请求数据
    rt_navtab($('.nav-tabs'),function($container){
        $('.tabs-hasborder',$container).find('.active').trigger('click');
    });
    // 请求第一个tweets tab的数据
    $('.tabs-hasborder','#tweets').find('.active').trigger('click');
    //请求You may also like 数据
    rt_requestTabcontent($('#recommandGames'));
})