<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>

<c:forEach items="${gameAboutData}" var="gameOne">
<li class="clearfix">
    <div class="l">
        <div class="like-thumb">
            <div class="image-box">
                <a class="thumb" href="game.action?gameId=${gameOne.gameDetail.id}">
                    <img src="${gameOne.gameDetail.dvdImagePath}">
                </a>
            </div>
            <c:if test="${gameOne.gameDetail.score}!=0">
            <span class="like-percent">
                ${gameOne.gameDetail.score} %
            </span>
            </c:if>
            <span class="tweets">
                ${gameOne.gameDetail.tweetCount} Tweets
            </span>
        </div>
        <p class="game-name">
            <a href="game.action?gameId=${gameOne.gameDetail.id}">
            <strong>
                ${gameOne.gameDetail.title}
            </strong>
            </a>
        </p>
    </div>
    <div class="r">
        <div class="t-action">
                                
            <span class="tweet-action">
            <a href="javascript:;" target=".gamesAboutTweets" class="tweet active">
                                            <em></em>Tweets
            </a>
            <a href="javascript:;" target=".gamesAboutVideos" class="video">
                                            <em></em>Videos
            </a>
            <a href="javascript:;" target=".gamesAboutImages" class="image">
                                            <em></em>Images
            </a>
            <a href="javascript:;" target=".gamesAboutLinks" class="links">
                                            <em></em>Links
            </a>
            </span>
        </div>
        <div class="tab-content">
            <ul class="tw-list active gamesAboutTweets">
                
                <c:forEach items="${latestTweets}" var="latestTweetOne">
                <li class="clearfix">
                    <blockquote class="twitter-tweet" width="900" id="blockquote_${latestTweetOne.id}"><a href="https://twitter.com/twitterapi/status/${latestTweetOne.id}"></a></blockquote>
                </li>
                </c:forEach>
            </ul>
            <ul class="list tw-list image-list gamesAboutVideos" data-url="/rest/link/userVideoLinksByGame/${gameOne.gameUserId}/${gameOne.gameDetail.id}/0.do" data-type="video" data-module='<li class="clearfix"> <div class="l"> <a class="thumb" href="game.action?gameId={{gameId}}"><img src="{{image}}" /> <b class="s-v-play"></b> </a> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc"">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"></a>}}}</label></div> </div> </li>'>
            </ul>
            <ul class="list tw-list images-list-wgth gamesAboutImages clearfix" data-url="/rest/link/userImageLinksByGame/${gameOne.gameUserId}/${gameOne.gameDetail.id}/0.do" data-module='<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}" BRT_img="imgLink" onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div></li>'>
            </ul>
            <ul class="list tw-list links-list gamesAboutLinks" data-url="/rest/link/userLinksByGame/${gameOne.gameUserId}/${gameOne.gameDetail.id}/0.do" data-module='<li class="clearfix"> <div class="intro"> <div class="title"> <a target="_blank" href="{{link}}"> <strong> {{link}} </strong> </a> </div> </div> </li>'>
            </ul>
        </div>
    </div>
</li>
</c:forEach>
