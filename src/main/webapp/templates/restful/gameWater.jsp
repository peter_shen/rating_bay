<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>

<c:forEach items="${gameList}" var="gameOne">
<li class="clearfix">
	<div class="l">
		<div class="like-thumb">
			<div class="image-box">
				<a class="thumb" href="game.action?gameId=${gameOne.id}">
				    <img src="${gameOne.dvdImagePath}">
				</a>
			</div>
			<c:if test="${gameOne.sentimentScore!=0}">
			<div class="radial-progress" data-type="overImage" data-score="${gameOne.sentimentScore}"></div>
			</c:if>
			<span class="tweets">
				${gameOne.tweetCount} Tweets
			</span>
		</div>
	</div>
	<div class="r">
		<div class="intro">
			<div class="title">
				<strong><a href="game.action?gameId=${gameOne.id}">${gameOne.title}</a></strong>
				<em class="time">
				<fmt:formatDate value="${gameOne.releaseDate}" pattern="yy-MM-dd"/>
				</em>
			</div>
			<p class="mute desc">
				<span class="details">
					${gameOne.description}
				</span>
				<c:if test="${gameOne.description.length() > 380}">
					<span class="brief">
						${fn:substring(gameOne.description,0, 375)}...
					</span><a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>
                </c:if>
                <c:if test="${gameOne.description.length() <= 380}">
                    <span class="brief">${gameOne.description}</span>
                </c:if>
			</p>
		</div>
		<div class="related">
			<label>
				<c:forEach items="${gameOne.twitterUsers}" var="twitterUser">
				<a href="user.action?userId=${twitterUser.id}" title="${twitterUser.username}">
					<img class="avatar-32" src="${twitterUser.profileImageURL}" data-type="userThumb" onerror="imgError(this)"/>
				</a>
				</c:forEach>
			</label>
		</div>
	</div>
</li>
</c:forEach>
