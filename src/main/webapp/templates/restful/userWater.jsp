<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="x" uri="http://java.sun.com/jstl/xml" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>

<c:forEach items="${userList}" var="userOne">
 <li class="clearfix">
        <div class="l">
            <a class="thumb" href="user.action?userId=${userOne.id}">
                <img id="${userOne.id}" class="avatar-128" data-type="userThumb" onerror="imgError(this)" src="${userOne.profileImageURL}"/>
                <script>
                          var normalSrc="${userOne.profileImageURL}";
                          var largeSrc = normalSrc.replace("_normal", "");
                          $("img#${userOne.id}").attr("src", largeSrc);
                        </script>
            </a>
        </div>
        <div class="r">
            <div class="intro">
                <div class="title">
                  <strong>
                    <a href="user.action?userId=${userOne.id}">
                      ${userOne.username}
                    </a>
                  </strong>
                  <em class="time">${userOne.location}</em>
                  <span class="twitter-follow">
                      <a href="https://twitter.com/${userOne.username}" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" data-lang="en">
                          Follow @${userOne.username}
                      </a>
                  </span>
                </div>
                <p class="u-intro mute">
                   ${userOne.description}
                </p>
            </div>
            <div class="tw-profile">
                <div class="t">
                    <h2>
                        <a href="https://twitter.com/intent/tweet?text=${userOne.username} on ratingbay&url=http://www.ratingbay.com/user.action?userId=${userOne.id}" target="_blank">
                            <img src="/assets/images/tw_logo_01.jpg"/>
                        </a>
                        Share
                    </h2>
                </div>
                <ul class="b tw-stats clearfix">
                    <li>
                        <label>
                            <strong>
                                ${userOne.friendsCount}
                            </strong>
                            <span class="mute">Following</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <strong>
                                ${userOne.followersCount}
                            </strong>
                            <span class="mute">Followers</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <strong>
                                ${userOne.gameCount}
                            </strong>
                            <span class="mute">Games</span>
                        </label>
                    </li>
                </ul>
            </div>
        </div>
</li>
</c:forEach>
