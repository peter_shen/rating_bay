<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:iterator value="userPageDataBean" var="userPageData">
<div class="main-content">
    <div class="container clearfix">
        <!-- column left -->
        <div class="u-column-left">
            <div id="t-user-profile" class="clearfix">
                <div class="l">
                    <a class="thumb">
                        <img id="twitter_avatar" src="<s:property value="iTwitterUser.getProfileImageURL()"/>" data-type="userThumb" onerror="imgError(this)"/>
                        <script>
                          var normalSrc="<s:property value="iTwitterUser.getProfileImageURL()"/>";
                          var largeSrc = normalSrc.replace("_normal", "");
                          $("img#twitter_avatar").attr("src", largeSrc);
                        </script>
                    </a>
                </div>
                <div class="r">
                    <h3 class="u-name">
                        <s:property value="iTwitterUser.getUsername()"/>
                        <span class="mute">
                            @<s:property value="iTwitterUser.getUsername()"/>
                        </span>
                        <span class="twitter-follow">
                        <a href="https://twitter.com/<s:property value="iTwitterUser.getUsername()"/>" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" data-lang="en">
                            Follow @<s:property value="iTwitterUser.getUsername()"/>
                        </a>
                        </span>
                    </h3>
                    <p class="u-location">
                        <s:property value="iTwitterUser.getLocation()"/>
                    </p>
                    <p class="u-home">
                        <a class="link" href="#"></a>
                    </p>
                    <p class="u-intro mute">
                        <s:property value="iTwitterUser.getDescription()"/>
                        <span class="link"><a href="#"></a></span>
                    </p>
                </div>
            </div>
            <div id="u-profile-main">
                <div class="game-tweets-top t">
                    <h2>Games</h2>
                    <ul class="tabs tabs-hasborder clearfix">
                        <li class="l-tab active">
                            <a href="#userGame">
                                <s:property value="iTwitterUser.getUsername()"/>
                            </a>
                        </li>
                        <li class="r-tab">
                            <a href="#friendGame">Friends</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content">
                    <!-- userGame -->
                    <ul id="userGame" data-url="rest/twitterUser/getUserActionWater/<s:property value="getUserId()"/>/" class="game-tweets-list tab-pane scrolling-loading">
                        <s:iterator value="uGameAboutDataBeans" var="uGameAboutData">
                        <li class="clearfix gamesAboutOne">
                            <div class="l">
                                <div class="game-thumb">
                                <div class="image-box">
                                    <a class="thumb" href="game.action?gameId=<s:property value="gameDetail.getId()"/>" >
                                        <img src="<s:property value="gameDetail.dvdImagePath"/>" data-type="gamePoster" onerror="imgError(this)" alt=""/>
                                    </a>
                                </div>
                                <s:if test="gameDetail.getScore()!=0">
                                <div class="radial-progress" data-type="overImage" data-score="<s:property value="gameDetail.getScore()"/>"></div>
                                </s:if>
                                <!-- <span class="like-percent"><s:property value="gameDetail.getScore()"/>%</span> -->
                                <span class="tweets"><s:property value="gameDetail.getTweetCount()"/> Tweets</span>
                                </div>
                                <p class="game-name">
                                    <a href="game.action?gameId=<s:property value="gameDetail.getId()"/>">
                                    <strong>
                                        <s:property value="gameDetail.getTitle()"/>
                                    </strong>
                                    </a>
                                </p>
                            </div>
                            <div class="r">
                                <div class="t-action">
                                    <span class="tweet-action">
                                        <a href="javascript:;" target=".gamesAboutTweets" class="tweet active">
                                            <em></em>Tweets
                                        </a>
                                        <a href="javascript:;" target=".gamesAboutVideos" class="video">
                                            <em></em>Videos
                                        </a>
                                        <a href="javascript:;" target=".gamesAboutImages" class="image">
                                            <em></em>Images
                                        </a>
                                        <a href="javascript:;" target=".gamesAboutLinks" class="links">
                                            <em></em>Links
                                        </a>
                                    </span>
                                </div>
                                <div class="tab-content">
                                    <ul class="tw-list active gamesAboutTweets">
                                        <s:subset source="latestTweets" start="0" count="10">
                                        <s:iterator>
                                        <li class="clearfix">
                                            <blockquote class="twitter-tweet" width="900" id="blockquote_<s:property value="getId()"/>"><a href="https://twitter.com/twitterapi/status/<s:property value="getId()"/>"></a></blockquote>
                                        </li>
                                        </s:iterator>
                                        </s:subset>
                                    </ul>
                                    <ul class="list tw-list image-list gamesAboutVideos" data-url="/rest/link/userVideoLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-type="video" data-module='<li class="clearfix"> <div class="l"> <a class="thumb" href="game.action?gameId={{gameId}}&videoId={{videoId}}"><img src="{{image}}" /> <b class="s-v-play"></b> </a> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc"">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"></a>}}}</label></div> </div> </li>'>
                                    </ul>
                                    <ul class="list tw-list images-list-wgth gamesAboutImages clearfix" data-url="/rest/link/userImageLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}" BRT_img="imgLink" onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div></li>'>
                                    </ul>
                                    <ul class="list tw-list links-list gamesAboutLinks" data-url="/rest/link/userLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="intro"> <div class="title"> <a target="_blank" href="{{link}}"> <strong> {{link}} </strong> </a> </div> </div> </li>'>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        </s:iterator>
                    </ul>
                    <s:if test="isLogged">
                    <!-- friendGame -->
                    <ul id="friendGame" class="game-tweets-list tab-pane" >
                        <s:iterator value="fGameAboutDataBeans" var="fGameAboutData">
                        <li class="clearfix gamesAboutOne">
                            <div class="l">
                                <a class="thumb" href="game.action?gameId=<s:property value="gameDetail.getId()"/>">
                                    <img src="<s:property value="gameDetail.dvdImagePath"/>" data-type="gamePoster" onerror="imgError(this)" alt=""/>
                                </a>
                                <s:if test="gameDetail.getScore()!=0">
                                <div class="radial-progress" data-type="overImage" data-score="<s:property value="gameDetail.getScore()"/>"></div>
                                </s:if>
                                <!-- <span class="like-percent"><s:property value="gameDetail.getScore()"/>%</span> -->
                                <span class="tweets"><s:property value="gameDetail.getTweetCount()"/> Tweets</span>
                                <span style="position: absolute;text-align:center;width:100%;margin:5px 0px 0px;">
                                    <a href="game.action?gameId=<s:property value="gameDetail.getId()"/>"><s:property value="gameDetail.getTitle()"/></a>
                                </span>
                            </div>
                            <div class="r">
                                <div class="t-action">
                                    <!--
                                    <strong>
                                        <s:property value="gameDetail.getTitle()"/>
                                    </strong>
                                    -->
                                    <span class="tweet-action">
                                        <a href="javascript:;" class="tweet active" target=".gamesAboutTweets"><em></em>Tweets</a>
                                        <a href="javascript:;" class="video" target=".gamesAboutVideos"><em></em>Videos</a>
                                        <a href="javascript:;" class="image" target=".gamesAboutImages"><em></em>Images</a>
                                        <a href="javascript:;" class="links" target=".gamesAboutLinks"><em></em>Links</a>
                                    </span>
                                </div>
                                <div class="tab-content">
                                    <!-- tweets -->
                                    <ul class="tw-list gamesAboutTweets">
                                        <s:subset source="latestTweets" start="0" count="10">
                                        <s:iterator>
                                        <li class="clearfix">
                                            <blockquote class="twitter-tweet" width="900" id="blockquote_<s:property value="getId()"/>"><a href="https://twitter.com/twitterapi/status/<s:property value="getId()"/>"></a></blockquote>
                                        </li>
                                        </s:iterator>
                                        </s:subset>
                                    </ul>
                                    <!-- video -->
                                    <ul class="list tw-list image-list gamesAboutVideos" data-type="video" data-url="/rest/link/friendsVideoLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="l"> <a class="thumb" href="game.action?gameId={{gameId}}&videoId={{videoId}}"><img src="{{image}}"/> <b class="s-v-play"></b> </a> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc"">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div> </div> </li>"'>

                                    </ul>
                                    <!-- images -->
                                    <ul class="list tw-list images-list-wgth  gamesAboutImages clearfix" data-url="/rest/link/friendsImageLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}" BRT_img="imgLink" onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/></a>}}}</label></div></li>'>

                                    </ul>
                                    <!-- link -->
                                    <ul class="list tw-list links-list gamesAboutLinks" data-url="/rest/link/friendsLinksByGame/<s:property value="getUserId()"/>/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="intro"> <div class="title"> <a target="_blank" href="{{link}}"> <strong> {{link}} </strong> </a> </div> </div> </li>'>

                                    </ul>
                                </div>
                            </div>
                        </li>
                        </s:iterator>
                    </ul>
                    </s:if>
                    <s:else>
                        <ul id="friendGame" class="game-tweets-list tab-pane" >
                            <li class="leadto-signin">To find the information posted by your friends, please <a href="javascript:;" onclick="$('#signinForm').submit()">sign in with twitter</a>
                            </li>
                        </ul>
                    </s:else>
                </div>
            </div>
        </div>

        <!-- column right -->
        <div class="u-column-right">
            <!-- twitter profile -->
            <div id="tw-profile">
                <div class="t">
                    <h2>
                        <a href="https://twitter.com/intent/tweet?text=<s:property value="iTwitterUser.getUsername()"/> on ratingbay&url=http://www.ratingbay.com/user.action?userId=<s:property value="iTwitterUser.getId()"/>" target='_blank'>
                            <img src="/assets/images/tw_logo_01.jpg"/>
                        </a>
                        Share
                    </h2>
                </div>
                <ul class="b tw-stats clearfix">
                    <li>
                        <a href="#">
                            <strong>
                                <s:property value="iTwitterUser.getFriendsCount()"/>
                            </strong>
                            <span class="mute">Following</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <strong><s:property value="iTwitterUser.getFollowersCount()"/></strong>
                            <span class="mute">Followers</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <!--<strong><s:property value="userGameCount"/></strong>-->
                            <strong></strong>
                            <span class="mute">Games</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- Most Influential Games -->
            <dl <s:if test="isLogged"></s:if><s:else>style="display:none"</s:else> class="m-cont-list">
                <dt class="icon-game">
                    <b></b>
                    <span>Recent Active Friends</span>
                </dt>
                <dd>
                    <ul class="cont-list">
                        <s:iterator value="recentActiveGamers">
                        <li class="has-thumb vthumb">
                            <div class="clearfix">
                                <div class="l">
                                    <a class="thumb" href="user.action?userId=<s:property value="getUserId()"/>" title="">
                                        <img src="<s:property value="getProfileImageURL()"/>" alt="" data-type="userThumb" onerror="imgError(this)"/>
                                    </a>
                                    <span class="follow">
                                        <a href="https://twitter.com/<s:property value="getUsername()"/>" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @<s:property value="getUsername()"/></a>
                                    </span>
                                </div>
                                <div class="r">
                                    <ul class="fans-info">
                                        <li>
                                            <a href="user.action?userId=<s:property value="getUserId()"/>"><span><s:property value="getUsername()"/></span></a>
                                        </li>
                                        <li>
                                            <h4><s:property value="getGameCount()"/></h4>
                                            <span>Games</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        </s:iterator>
                    </ul>
                </dd>
            </dl>
        </div>
    </div>
</div>
</s:iterator>
<!-- twitter follow button -->
<script type="text/javascript">
    var userId = '<s:property value="getUserId()"/>';
    var gcUrl = 'rest/tweet/countGameByUserId/'+userId+'.do '
    $.get(gcUrl,function(data){
        var gc = data.GameCount;
        $('#tw-profile li:last-child strong').text(gc);
    });

</script>
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
</script>
