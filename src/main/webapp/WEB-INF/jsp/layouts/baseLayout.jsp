<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<html locale="true">
    <head>
        <%-- Include common set of meta tags for each layout --%>
        <%@ include file="/WEB-INF/jsp/common/meta.jsp" %>

        <%-- Push tiles attributes in page context --%>
        <tiles:importAttribute />
        <title><s:text name="RatingBay,game review,video games, game rating, sentiment, sentiment analysis, game tweets"/></title>

        <%-- jquery theme --%>
        <sj:head locale="en" jqueryui="true" jquerytheme="cupertino"/>

        <%-- Get List of Stylesheets --%>
        <tiles:useAttribute id="styleList" name="styles"
            classname="java.util.List" ignore="true"/>

        <s:iterator id="css" value="#attr['styleList']">
            <link rel="stylesheet" type="text/css" media="all" href="<s:url value="%{css}"/>" />
        </s:iterator>

        <%-- List of ExtStyle --%>
        <tiles:useAttribute id="extStyleList" name="extStyles"
            classname="java.util.List" ignore="true"/>

        <s:iterator id="css" value="#attr['extStyleList']">
            <link rel="stylesheet" type="text/css" media="all" href="<s:url value="%{css}"/>" />
        </s:iterator>
		<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    </head>
    <body>
        
        <tiles:insertAttribute name="header"/>
        <tiles:insertAttribute name="content"/>
        <tiles:insertAttribute name="footer"/>

        <%-- Get Javascript List --%>
        <tiles:useAttribute id="scriptList" name="scripts"
            classname="java.util.List" ignore="true"/>
        <s:iterator id="js" value="#attr['scripts']">
            <script type="text/javascript" src="<s:url value="%{js}"/>" > </script>
        </s:iterator>

        <%-- List of ExtScripts --%>
        <tiles:useAttribute id="extScriptList" name="extScripts"
            classname="java.util.List" ignore="true"/>
        <s:iterator id="js" value="#attr['extScriptList']">
            <script type="text/javascript" src="<s:url value="%{js}"/>" > </script>
        </s:iterator>

    </body>
</html>
