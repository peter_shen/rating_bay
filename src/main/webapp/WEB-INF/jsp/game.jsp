<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<s:iterator value="gamePageDataBean" var="gamePageData">
<div class="main-content">
    <div class="container clearfix">
        <!-- game profile -->
        <s:iterator value="gameDetail" var="gameDetailOne">
        <div id="t-game-profile" class="clearfix">
            <div class="l">
                <div class="game-left">
                    <div class="t-profile">
                        <div class="like-thumb">
                            <div class="image-box">
                                <a class="thumb">
                                    <img src="<s:property value="getDvdImagePath()"/>"  data-type="gamePoster" onerror="imgError(this)" alt="">
                                </a>
                            </div>
<!--                             <span class="like-percent">
                                <s:property value="getScore()"/>%
                            </span> -->
                            <s:if test="getScore()!=0">
                            <div class="radial-progress" data-type="overImage" data-score="<s:property value="getScore()"/>"></div>
                            </s:if>
                            <div class="like-tweets">
                                <s:property value="getTweetCount()"/> Tweets
                            </div>
                        </div>
                        <p><label>Genre:</label>
                        <s:iterator value="getGameGenres()" var="Genre">
                            <s:property value="getGenre().getName()"/>
                        </s:iterator>
                        </p>

                        <p><label>System:</label>
                        <s:iterator value="getGamePlatforms()" var="Platform">
                            <s:property value="getPlatform().getName()"/>,
                        </s:iterator>
                        </p>

                        <p><label>Release:</label><s:property value="getReleaseDate()"/></p>
                        <p><label>Publisher:</label><s:property value="getPublisher()"/></p>
                        <p><label>Tweets:</label><s:property value="getTweetCount()"/></p>
                    </div>
                    <!--
                    <div class="b-review">
                        <p><strong>What do you think?</strong></p>
                        <div>
                            <a href="javascript:;" class="agree"></a>
                            <a href="javascript:;" class="disagree"></a>
                        </div>
                    </div>
                     -->
                </div>
            </div>
            <div class="r">
                <h2 class="game-name"><s:property value="getTitle()"/></h2>
                <div id="game-media-content">
                    <dl class="media-switcher">
                        <dt class="switcher-tabs">
                            <a class="tab history-chart" data-type="history-chart" href="javascript:;"><span>Trend</span></a><a class="tab tweets-chart" data-type="tweets-chart" href="javascript:;"><span>Tweets</span></a><a class="tab game-video" data-type="game-video" href="javascript:;"><span>Video</span></a>
                        </dt>
                        <dd class="switcher-body">
                            <div class="media-item history-chart">


                            </div>
                            <div class="media-item tweets-chart">


                            </div>
                            <div class="media-item game-video">
                                <div class="game-video-screenhot">
                                    <iframe id="videoPlayer" width="820" height="464" frameborder="0" allowfullscreen="true" scrolling="no" marginheight="0" marginwidth="0" src="about:blank"></iframe>
                                    <script type="text/javascript">
                                        gameVideoId = "<s:property value="getVideoId()"/>";
                                        gameId = "<s:property value="getId()"/>";
                                    </script>
                                </div>
                            </div>
                        </dd>
                    </dl>

                </div>
                <div class="game-tags">
                    <label class="tags"><strong>Hash Tags:</strong></label>
                    <s:iterator value="hashTags">
                    <a href="https://twitter.com/search?q=<s:property value="getText()"/>&src=tyah" class="link"><s:property value="getText()"/></a>
                    (<s:property value="getCount()"/>),
                    </s:iterator>
                </div>
                <p class="game-intro desc">
                    <s:if test="getDescription().length() > 360">
                        <span class="details">
                            <s:property value="getDescription()"/>
                        </span>
                        <span class="brief">
                            <s:property value="getDescription().substring(0, 360)" />...
                        </span>
                        <a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>
                    </s:if>
                    <s:else>
                        <s:property value="getDescription()" />
                    </s:else>
                </p>
            </div>
        </div>
        </s:iterator>
        <!-- column left -->
        <div class="u-column-left">
            <div id="u-profile-main">
                <div class="nav-tabs-wrapper">
                    <ul class="nav-tabs tabs tabs-large clearfix">
                        <li class="active"><a href="#tweets">Tweets</a></li>
                        <li><a href="#videos">Videos</a></li>
                        <li><a href="#links">Links</a></li>
                        <li><a href="#images">Images</a></li>
                    </ul>
                    <div class="tab-content">
                        <!-- Videos -->
                        <div id="videos" class="tab-pane">
                            <div class="game-tweets-top t">
                               <!--<h2>Videos</h2>-->
                                <ul class="tabs tabs-hasborder clearfix">
                                    <li class="active">
                                        <a href="#videosPopular">Most Popular</a>
                                    </li>
                                    <li>
                                        <a href="#videosFriends">Friends</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <ul id="videosPopular" class="list image-list tab-pane scrolling-loading" data-type="video" data-url="rest/link/popularVideoLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="l"><div class="rel-thumb"><a class="thumb" data-video-id="{{videoId}}" href="#videoPlayer" onclick="playGameVideo(this)"><img src="{{image}}"/> <b class="s-v-play"></b> </a></div></div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" data-type="userThumb" onerror="imgError(this)" src="{{profileImgUrl}}"></a>}}}</label></div> </div> </li>"'>

                                </ul>
                                 <s:if test="isLogged">
                                <ul id="videosFriends" class="list image-list tab-pane scrolling-loading" data-type="video" data-url="rest/link/usersVideoLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="l"><div class="rel-thumb"><a class="thumb" href="game.action?gameId={{gameId}}&videoId={{videoId}}"><img src="{{image}}"/> <b class="s-v-play"></b> </a></div> </div> <div class="r"> <div class="intro"> <div class="title"> <strong>{{title}}</strong> </div><p class="mute desc">{{descOri}}{{desc}}</p></div><div class="related"><label>{{{[users]<a href="user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"></a>}}}</label></div> </div> </li>"'>
                                </ul>
                                </s:if>
                                <s:else>
                                    <ul id="videosFriends"  class="list tweets-list tab-pane">
                                        <li class="leadto-signin">To find the information posted by your friends, please <a href="javascript:;" onclick="$('#signinForm').submit()">sign in with twitter</a>
                                        </li>
                                    </ul>
                                </s:else>

                            </div>
                        </div>
                        <!-- Images -->
                        <div id="images" class="tab-pane">
                            <div class="game-tweets-top t">
                                <!--<h2>Images</h2>-->
                                <ul class="tabs tabs-hasborder clearfix">
                                    <li class="active"><a href="#imagesPopular">Most Popular</a></li>
                                    <li><a href="#imagesFriends">Friends</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                            <ul id="imagesPopular" class="list images-list tab-pane scrolling-loading" data-url="rest/link/popularImageLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}"  onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" BRT_img="imgUser" onError="imgError(this)" /></a>}}}</label></div></li>'>

                            </ul>
                            <s:if test="isLogged">
                            <ul id="imagesFriends" class="list images-list tab-pane scrolling-loading" data-url="rest/link/usersImageLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li><div class="image-box"><a href="{{link}}" target="_blank" class="thumb"><img src="{{mediaDirectLink}}"  onError="imgError(this)"></a></div><div class="related"><label>{{{[users] <a href="user.action?userId={{userId}}" title="{{username}}"><img class="avatar-32" src="{{profileImgUrl}}" BRT_img="imgUser" onError="imgError(this)" /></a>}}}</label></div></li>'>

                            </ul>
                            </s:if>
                            <s:else>
                                <ul id="imagesFriends"  class="list tweets-list tab-pane">
                                    <li class="leadto-signin">To find the information posted by your friends, please <a href="javascript:;" onclick="$('#signinForm').submit()">sign in with twitter</a>
                                    </li>
                                </ul>
                            </s:else>
                            </div>
                        </div>
                        <!-- Links -->
                        <div id="links" class="tab-pane">
                            <div class="game-tweets-top t">
                                <!--<h2>Links</h2>-->
                                <ul class="tabs tabs-hasborder clearfix">
                                    <li class="active"><a href="#linksPopular">Most Popular</a></li>
                                    <li><a href="#linksFriends">Friends</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                            <ul id="linksPopular" class="list links-list tab-pane scrolling-loading" data-url="rest/link/popularLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="intro"> <div class="title"> <strong><a target="_blank" href="{{link}}">{{link}}</a></strong> </div> </div> <div class="related">{{{[users] <label> <a href="/user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/> </a> </label>}}}</div> </li>'>
                            </ul>
                            <s:if test="isLogged">
                            <ul id="linksFriends" class="list links-list tab-pane scrolling-loading"data-url="rest/link/usersLinksByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <div class="intro"> <div class="title"> <strong><a target="_blank" href="{{link}}">{{link}}</a></strong> </div> </div> <div class="related">{{{[users] <label> <a href="/user.action?userId={{userId}}" title="{{username}}"> <img class="avatar-32" src="{{profileImgUrl}}" data-type="userThumb" onerror="imgError(this)"/> </a> </label>}}}</div> </li>'>

                            </ul>
                            </s:if>
                            <s:else>
                                <ul id="linksFriends"  class="list tweets-list tab-pane">
                                    <li class="leadto-signin">To find the information posted by your friends, please <a href="javascript:;" onclick="$('#signinForm').submit()">sign in with twitter</a>
                                    </li>
                                </ul>
                            </s:else>
                            </div>
                        </div>
                        <!-- Tweets -->
                        <div id="tweets" class="tab-pane">
                            <div class="game-tweets-top t">
                                <!--<h2>Tweets</h2>-->
                                <ul class="tabs tabs-hasborder clearfix">
                                    <li class="active">
                                        <a href="#tweetsPopular">Most Popular</a>
                                    </li>
                                    <li><a href="#tweetsPositive">Positive</a></li>
                                    <li><a href="#tweetsNagative">Negative</a></li>
                                    <li><a href="#tweetsLatest">Latest</a></li>
                                    <li><a href="#tweetsFriends">Friends</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <ul id="tweetsPopular" class="list tweets-list tab-pane scrolling-loading" data-url="rest/tweet/popularTweetsByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <blockquote class="twitter-tweet" width="900"><a href="https://twitter.com/twitterapi/status/{{id}}"></a></blockquote> </li>' data-type="tweets">
                                </ul>
                                <ul id="tweetsPositive" class="list tweets-list tab-pane scrolling-loading" data-url="rest/tweet/positiveTweetsByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <blockquote class="twitter-tweet" width="900"><a href="https://twitter.com/twitterapi/status/{{id}}"></a></blockquote> </li>' data-type="tweets">
                                </ul>
                                <ul id="tweetsNagative" class="list tweets-list tab-pane scrolling-loading" data-url="rest/tweet/negativeTweetsByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <blockquote class="twitter-tweet" width="900"><a href="https://twitter.com/twitterapi/status/{{id}}"></a></blockquote> </li>' data-type="tweets">
                                </ul>
                                <ul id="tweetsLatest" class="list tweets-list tab-pane scrolling-loading" data-url="rest/tweet/latestTweetsByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <blockquote class="twitter-tweet" width="900"><a href="https://twitter.com/twitterapi/status/{{id}}"></a></blockquote> </li>' data-type="tweets">
                                </ul>
                                <s:if test="isLogged">
                                   <ul id="tweetsFriends" class="list tweets-list tab-pane scrolling-loading" data-url="rest/tweet/usersTweetsByGame/<s:property value="gameDetail.getId()"/>/0.do" data-module='<li class="clearfix"> <blockquote class="twitter-tweet" width="900"><a href="https://twitter.com/twitterapi/status/{{id}}"></a></blockquote> </li>' data-type="tweets">
                                    </ul>
                                </s:if>
                                <s:else>
                                    <ul id="tweetsFriends"  class="list tweets-list tab-pane">
                                        <li class="leadto-signin">To find the information posted by your friends, please <a href="javascript:;" onclick="$('#signinForm').submit()">sign in with twitter</a>
                                        </li>
                                    </ul>
                                </s:else>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!-- column right -->
        <div class="u-column-right">
            <!-- Share this page -->
            <dl class="m-cont-list">
                <dt class="icon-talk">
                    <b></b>
                    <span>Share This Page</span>
                </dt>
                <dd>
                    <ul class="v-share">
                        <li>
                            <a href="https://twitter.com/share" class="twitter-share-button" data-lang="en">Tweet</a>
                            <script>
                            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');
                            </script>
                        </li>
                        <li>
                            <!-- <a href="javascript:;" class="fb" onclick=" window.open( 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(location.href), 'facebook-share-dialog', 'width=626,height=436'); return false;"><em></em>share on facebook</a> -->
                            <div class="fb-share-button" data-type="button_count"></div>
                            <div id="fb-root"></div>
                            <script>(function(d, s, id) {
                              var js, fjs = d.getElementsByTagName(s)[0];
                              if (d.getElementById(id)) return;
                              js = d.createElement(s); js.id = id;
                              js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                              fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        </li>
                        <li>
                            <div class="g-plusone"></div>
                            <script type="text/javascript">
                              window.___gcfg = {lang: 'en-GB'};

                              (function() {
                                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                                po.src = 'https://apis.google.com/js/platform.js';
                                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
                              })();
                            </script>
                      </li>
                    </ul>
                </dd>
            </dl>
            <!-- May Also Like -->
            <dl class="m-cont-list">
                <dt class="icon-game">
                    <b></b>
                    <span>You May Also Like</span>
                </dt>
                <dd>
                    <ul id="recommandGames" class="cont-list"
                        data-url="rest/tweet/topRecommendedGames/<s:property value="gameDetail.getId()"/>/0.do"
                        data-module='<li class="has-n-thumb"> <div class="clearfix"> <div class="l"> <div class="like-thumb"> <div class="image-box"> <a class="thumb" href="game.action?gameId={{id}}"> <img src="{{dvdImagePath}}" data-type="gamePoster" onerror="imgError(this)" alt=""> </a> </div><div class="radial-progress" data-score="{{score}}" data-type="overImage"></div> <div class="like-tweets">{{tweetsCount}}Tweets </div> </div> </div> <div class="r"> <a href="game.action?gameId={{id}}" target="_balnk"> <strong>{{name}}</strong> </a> <p class="mute"> </p> </div> </div> </li>'>
                    </ul>
                </dd>
            </dl>
            <!-- Top Players -->
            <dl class="m-cont-list">
                <dt class="icon-game">
                    <b></b>
                    <span>Top Players</span>
                </dt>
                <dd>
                    <ul class="cont-list">
                        <s:iterator value="topPlayers" var="topPlayer">
                        <li class="has-thumb vthumb">
                            <div class="clearfix">
                                <div class="l">
                                    <a class="thumb" href="user.action?userId=<s:property value="getTwitterUserId()"/>" title="">
                                    <img src="<s:property value="getUserProfileImgUrl()"/>" alt="" data-type="userThumb" onerror="imgError(this)"/>
                                    </a>
                                    <span class="follow">
                                        <a href="https://twitter.com/<s:property value="getUsername()"/>" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @<s:property value="getUsername()"/></a>
                                    </span>
                                </div>
                                <div class="r">
                                    <a href="user.action?userId=<s:property value="getTwitterUserId()"/>" target="_balnk">
                                        <strong><s:property value="getUsername()"/></strong>
                                    </a>
                                    <p class="mute">
                                        <s:property value="getUserDescription()"/>
                                    </p>
                                    <ul class="fans-info">
                                        <li>
                                            <h4><s:property value="getUserFriendsCount()"/></h4>
                                            <span>friends</span>
                                        </li>
                                        <li>
                                            <h4><s:property value="getUserFollowersCount()"/></h4>
                                            <span>followers</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        </s:iterator>
                    </ul>
                </dd>
            </dl>
        </div>
    </div>
</div>
</s:iterator>
<script type="text/javascript" src="assets/js/highstock.js"></script>
<script type="text/javascript">
function rt_loadGameVideo(gameId,gameVideoId){
    var videoId = gameVideoId;
    var autoplay = 0;
    if( ( videoId.length > 11 ) && ( -1 != videoId.indexOf("?autoplay=1") ) ){
        autoplay = 1;
        videoId = videoId.substr(0, 11);
    }

    var initVideoPlayerTimer = setInterval(function(){
        if( "undefined" != typeof RatingbayUtils ){
            clearInterval(initVideoPlayerTimer);
            initVideoPlayer();
        }
    }, 10);

    var initVideoPlayer  = function(){
        var videoPlayerSrc = "http://www.youtube.com/embed/" + videoId + "?autoplay=" + autoplay;
        $("iframe#videoPlayer").attr("src", videoPlayerSrc);
        videoPlayerReady = true;
    };
};

function playGameVideo(btn){
    var $btn = $(btn)
        ,$videoPlayer = $("iframe#videoPlayer")
        ;
    var gameVideoId = $(btn).data('video-id')+'?autoplay=1';
    $('#video-thumb-playing').removeAttr('id')&&$btn.parents('li:eq(0)').attr('id','video-thumb-playing');
    $videoPlayer.attr("src",'about:blank');
    switchMedia($('.switcher-tabs .game-video'));
    rt_loadGameVideo(gameId,gameVideoId);    
};

var $st = $('.switcher-tabs')
    ,$sb = $('.switcher-body')
    ;
$('.tab',$st).each(function(i,elem){
    var $elem =$(elem)
        ,$bd = $sb.find('.media-item:eq('+i+')')
        ;
    $elem.on('click',function(){
        switchMedia($(this));
    });
    var $active_tab = $('.history-chart',$st);
    if(location.href.indexOf('videoId=')!=-1){
        $active_tab = $('.game-video',$st);
    }
    switchMedia($active_tab);
});
function switchMedia($tab){
    var type = $tab.data('type');
    var $bd = $sb.find('.'+type);
    if(!$bd.data('clicked')){
        if(type=='game-video'){
            rt_loadGameVideo(gameId,gameVideoId);
        }else if(type=='history-chart'){
            generateChart()
        }else if(type=='tweets-chart'){
            generateChart('tweets')
        }
        $bd.data('clicked',true);
    }
    if(!$tab.hasClass('tab-active')){
        $('.tab-active',$st).removeClass('tab-active');
        $tab.addClass('tab-active');
        $('.media-item',$sb).hide();
    }
    $bd.show();
}
function generateChart(type){
    var type = type||'trend'
        ,$chartbox = type=='tweets'? $('.tweets-chart',$sb):$('.history-chart',$sb)
        ,url = type=='tweets'?'/rest/game/tweetHistory/'+gameId+'.do':'/rest/game/scoreHistory/'+gameId+'.do'
        ;

    $.get(url,function(data){

        if(!data.result.length){
            $chartbox.append($('<h2 style="padding: 10px;color: #FFF;">No Data</h2>'));
            return ;
        }
        var stockData = [];
        $.each(data.result,function(i,val){
            var arr = [];
            
            if(type=='tweets'){
                arr.push(new Date(val.create_date).valueOf());
                arr.push(val.tweetCount);
            }else{
                arr.push(new Date(val.insert_time).valueOf());
                arr.push(parseFloat(val.score)*100);  
            }
            stockData.unshift(arr);
        });
        console.log(stockData)
        // Create the chart
        Highcharts.setOptions({colors:['#EA4E01', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#8085e8', '#8d4653', '#91e8e1']});

        $chartbox.highcharts('StockChart', {

            chart:{ 
                height:468
            },

            credits:{
                enabled:false
            },
            rangeSelector : {
                enabled:false,
                selected : 0,
                inputEnabled: true
            },

            title : {
                text : (type=='tweets'?'The Tweets Count Of ':'The Sentiment Trend Of ')+$('.game-name').text()
            },
            xAxis:{                
                minTickInterval:24*3600*1000,
                tickInterval:24*3600*1000
            },
            yAxis:{
                showLastLabel:true,
                // tickPositions:[0,10,20,30,40,50,60,70,80,90,100],
                labels:{
                    format:type=='tweets'?'{value}':'{value}%'
                }
            },
            navigator:{
                maskFill:'rgba(252, 80, 23, 0.2)'
            },
            scrollbar:{
                enabled:false
            },
            tooltip:{
                valueSuffix:"%",
                valueDecimals: 2
            },
            series : [{
                name : type=='tweets'?'Tweets Count':'Sentiment Score',
                data : stockData,
                threshold : null,
                type : 'area',
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0,Highcharts.getOptions().colors[0]],
                        [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                    ]
                }
            }]
        });
    });
}

</script>
<!-- twitter follow button -->
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
</script>