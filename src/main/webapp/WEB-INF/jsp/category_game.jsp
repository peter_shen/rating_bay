<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:iterator value="searchPageDataBean" var="caregoryPageData">
<div id="search-wrapper" class="main-content">
	<div class="container clearfix">
		<div class="search-box">
			<div class="search-top">
			<h2>${caregoryPageData.kws} Games</h2>
				<strong>Sort By:</strong>
				<a id="RD" class="btn order-btn order-desc" href="javascript:;">
					Release Date<em></em>
				</a>
				<a id="TC" class="btn order-btn order-desc" href="javascript:;">
					Tweet Count<em></em>
				</a>
				<a id="SS" class="btn order-btn order-desc" href="javascript:;">
					Sentiment Score<em></em>
				</a>
			</div>
			<s:if test="getGameList().getSize()==0">
				<h2 style="padding:25px;text-align:center;">We can't find any contents,please try to search other words</h2>
			</s:if>
			<s:else>
			<ul class="list image-list search-result-list game-result-list">
				<s:iterator value="gameList" var="gameOne">
				<li class="clearfix">
					<div class="l">
						<div class="like-thumb">
							<div class="image-box">
								<a class="thumb" href="game.action?gameId=<s:property value="getId()"/>">
								    <img src="<s:property value="getDvdImagePath()"/>">
								</a>
							</div>
                            <div class="radial-progress" data-type="overImage" data-score="<s:property value="getSentimentScore()"/>"></div>
<!-- 							<span class="like-percent">
								<s:property value="getSentimentScore()"/>%
							</span> -->
							<span class="tweets">
								<s:property value="getTweetCount()"/> Tweets
							</span>
						</div>
					</div>
					<div class="r">
						<div class="intro">
							<div class="title">
								<strong><a href="game.action?gameId=<s:property value="getId()"/>"><s:property value="getTitle()"/></a></strong>
								<em class="time"><s:property value="getReleaseDate()"/></em>
							</div>
							<p class="mute desc">
								<span class="details">
									<s:property value="getDescription()"/>
								</span>
								<s:if test="getDescription().length() > 380">
                        		<span class="brief">
                        			<s:property value="getDescription().substring(0, 375)" />...
                        		</span><a href="javascript:;" class="link" onclick="toggleDesc(this)">Expand</a>
			                    </s:if>
			                    <s:else>
			                        <s:property value="getDescription()" />
			                    </s:else>
							</p><br>
								<div class="genre">
										<strong>Genre:</strong>
										<s:set name="i" value="1"></s:set>
										<s:iterator value="getGenre()" var="genreOne">
										<s:if test="#i==1">
										<s:property value="genreOne"/>
										<s:set name="i" value="0"></s:set>
										</s:if>
										<s:if test="#i==0">
										,&nbsp<s:property value="genreOne"/>
										</s:if>
										</s:iterator>
										<s:set name="i" value="1"></s:set>
									<br>
								</div>
								<div class="platform">
										<strong>platform:</strong>
										<s:set name="i" value="1"></s:set>
										<s:iterator value="getPlatform()" var="genreOne">
										<s:if test="#i==1">
										<s:property value="genreOne"/>
										<s:set name="i" value="0"></s:set>
										</s:if>
										<s:if test="#i==0">
										,&nbsp<s:property value="genreOne"/>
										</s:if>
										</s:iterator>
										<s:set name="i" value="1"></s:set>
								</div>
						</div>
						<div class="related">
							<label>
								<s:iterator value="getTwitterUsers()" var="twitterUser">
								<a href="user.action?userId=<s:property value="getId()"/>" title="<s:property value="getUsername()"/>">
									<img class="avatar-32" src="<s:property value="getProfileImageURL()"/>" data-type="userThumb" onerror="imgError(this)"/>
								</a>
								</s:iterator>
							</label>

						</div>
					</div>
				</li>
				</s:iterator>
			</ul>
			</s:else>
		</div>

    <s:iterator value="pageWidget">
      <div class="paging" style="display:none">
        <s:if test="isFirstPage">
        </s:if>
        <s:else>
          <a href="category.action?<s:property value="prevPageLink"/>">Prev Page</a>
        </s:else>

        <s:iterator value="pageList">
          <s:if test="isMorePage">
            <a href="javascript:;">...</a>
          </s:if>
          <s:else>
            <s:if test="isCurPage">
              <a href="category.action?<s:property value="link"/>" style="font-weight:bold;"><s:property value="display"/></a>
            </s:if>
            <s:else>
                <a href="categoryh.action?<s:property value="link"/>"><s:property value="display"/></a>
            </s:else>
          </s:else>
        </s:iterator>

        <s:if test="isLastPage">
        </s:if>
        <s:else>
          <a href="category.action?<s:property value="nextPageLink"/>">Next Page</a>
        </s:else>

        <a href="javascript:;">totoal: <s:property value="totalPages"/></a>
      </div>
      </s:iterator>

		<script type="text/javascript">
		var RatingbaySearch = {
			'keyword': "<s:property value="kw"/>"
		};
		</script>
	</div>
</div>
</s:iterator>