<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.font.css">
<script type="text/javascript" src="/assets/js/global.js"></script>
<div class="wrapper">

<div id="g-top-nav">
<div id="g-quic-nav">
  <div class="container clearfix">
  <a href="category.action?p=PC"><strong>PC</strong></a>
  <a href="category.action?p=Sony Playstation 3"><strong>PS3</strong></a>
  <a href="category.action?p=Sony Playstation 4"><strong>PS4</strong></a>
  <a href="category.action?p=3DS"><strong>3DS</strong></a>
  <a href="category.action?p=WII U"><strong>WII U</strong></a>
  <a href="category.action?p=Vita"><strong>Vita</strong></a>
  <a href="category.action?p=Xbox One"><strong>Xbox One</strong></a>
  <a href="category.action?p=Xbox 360"><strong>Xbox 360</strong></a>
    <div class="share-buttons">
        <span>Share RatingBay To: </span>
        <img src="/assets/images/facebook_16.png" title="Facebook" class="share-btn" data-type="facebook"/>
        <img src="/assets/images/twitter_16.png" title="Tweet" class="share-btn" data-type="tweet"/>
        <img src="/assets/images/googleplus_16.png" title="Google +" class="share-btn" data-type="googleplus"/>
    </div>
  </div>
</div>
   <div class="container clearfix">
      <h1 class="logo">
         <a href="/">
         </a>
      </h1>
      <ul class="t-action">
         <s:if test="isLogged">
         <li class="dropdown">
            <div class="u-signin-profile">
            <a class="u-thumb" href="user.action?userId=<s:property value="iTwitterUser.getId()"/>">
               <img class="avatar-32" src="<s:property value="iTwitterUser.getProfileImageURL()"/>" alt="<s:property value="iTwitterUser.getUsername()"/>" />
            </a>
            <div class="dropdown-menu">
                  <div class="dropdown-caret">
                    <span class="caret-outer"></span>
                  </div>
                  <ul>
                    <li class="current-user">
                      <a class="account-summary" href="user.action?userId=<s:property value="iTwitterUser.getId()"/>">
                      <div class="account-group">
                           <img class="avatar avatar-32" src="<s:property value="iTwitterUser.getProfileImageURL()"/>" alt="<s:property value="iTwitterUser.getUsername()"/>" alt="<s:property value="iTwitterUser.getUsername()"/>"/>
                           <b class="fullname">
                            <s:property value="iTwitterUser.getUsername()"/>
                          </b>
                           <small class="metadata">View profile</small>
                      </div>
                      </a>
                    </li>
                    <li class="dropdown-divider"></li>
                    <li><a href="signout.action">Sign out</a></li>
                  </ul>
                </div>
            </div>
         </li>
         </s:if>
         <s:else>
          <li>
            <form id="signinForm" action="signin/twitter.do" method="POST" style="display:none">
               <input type="submit" value="signin" >
            </form>
             <form id="connectForm" action="connect/twitter.do" method="POST" style="display:none">
               <input type="submit" value="signin" >
            </form>
            <a id="signinFormA" href="#" class="t-icon">
               <span class="icon"></span>
               Sign in with Twitter
            </a>
         </li>
         </s:else>
          <li class="action-search">
            <div id="gamecat-all">
              <a href="javascript:;" class="category-icon">
                <strong>Browse</strong>
                <b></b>
              </a>
              <div id="gamecat-list-box">
                <ul>
                  <li class="gamecat-platform gamecat-list">
                    <dl>
                      <dt><strong>Platform</strong></dt>
                      <dd>
                        <a href="#" class="gamecat-item platform-item">Android</a>
                        <a href="#" class="gamecat-item platform-item">IOS</a>
                        <a href="#" class="gamecat-item platform-item">Mac OS</a>
                        <a href="#" class="gamecat-item platform-item">Xbox</a>
                        <a href="#" class="gamecat-item platform-item" data-category="PlayStation Network">PSN</a>
                        <a href="#" class="gamecat-item platform-item">3DS</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Nintendo DS">DS</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Nintendo Wii">Wii</a>
                        <a href="#" class="gamecat-item platform-item">WII U</a>
                        <a href="#" class="gamecat-item platform-item">PC</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Sony Playstation 2">PS2</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Sony Playstation 3">PS3</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Sony Playstation 4">PS4</a>
                        <a href="#" class="gamecat-item platform-item">Vita</a>
                        <a href="#" class="gamecat-item platform-item">PSP</a>
                        <a href="#" class="gamecat-item platform-item" data-category="Windows Phone">WP</a>
                      </dd>
                    </dl>
                  </li>
                  <li class="dropdown-divider"></li>
                  <li class="gamecat-genre  gamecat-list">
                    <dl>
                      <dt><strong>Genre</strong></dt>
                      <dd>
                        <a href="#" class="gamecat-item genre-item" data-category="1">Shooter</a>
                        <a href="#" class="gamecat-item genre-item" data-category="2">Fighting</a>
                        <a href="#" class="gamecat-item genre-item" data-category="3">Action</a>
                        <a href="#" class="gamecat-item genre-item" data-category="4">Adventure</a>
                        <a href="#" class="gamecat-item genre-item" data-category="5">MMO</a>
                        <a href="#" class="gamecat-item genre-item" data-category="6">Strategy</a>
                        <a href="#" class="gamecat-item genre-item" data-category="7">Role Playing</a>
                        <a href="#" class="gamecat-item genre-item" data-category="8">Sandbox</a>
                        <a href="#" class="gamecat-item genre-item" data-category="9">Platform</a>
                        <a href="#" class="gamecat-item genre-item" data-category="10">Horror</a>
                        <a href="#" class="gamecat-item genre-item" data-category="11">Puzzle</a>
                        <a href="#" class="gamecat-item genre-item" data-category="12">Music</a>
                        <a href="#" class="gamecat-item genre-item" data-category="13">Racing</a>
                        <a href="#" class="gamecat-item genre-item" data-category="14">Stealth</a>
                        <a href="#" class="gamecat-item genre-item" data-category="15">Sports</a>
                        <a href="#" class="gamecat-item genre-item" data-category="16">Life Simulation</a>
                        <a href="#" class="gamecat-item genre-item" data-category="17">Pinball</a>
                      </dd>
                    </dl>
                  </li>
                </ul>
              </div>
            </div>

            <div id="g-search" class="t-search">
              <form action="search.action">
               <div class="bg">
                  <span class="s-box">
                  <input type="text" class="keyword" name="g" id="keyword" autocomplete="off"><span class="s-btn-search glyphicon glyphicon-search"></span>
                  <label class="s-flag">
                    <%
                    String ulg=(String)request.getAttribute("g");
                    String ulu=(String)request.getAttribute("u");
                    if(ulg==null&&ulu==null){out.print("Input Game Name or Twitter Username");}
                    if(ulg==null&&ulu!=null){out.print("Search for user '<strong>" + ulu + "</strong>'");}
                    if(ulg!=null&&ulu==null&&ulg.length()>0){out.print("Search for game '<strong>" + ulg + "</strong>'");}
                    if(ulg!=null&&ulu==null&&ulg.length()==0){out.print("Input Game Name or Twitter Username");}
                    %>
                  </label>
                  </span>
               </div>
               </form>
                <div id="s-opts-box">
                 <ul class="s-opts">
                   <li class="s-game opts-item" data-link="search.action?g=">
                      <a href="search.action?g=">Search for game<span>"<strong></strong>"</span></a>
                   </li>
                   <li class="dropdown-divider"></li>
                   <li class="s-user opts-item" data-link="search.action?u=">
                      <a href="search.action?u=">Search for user<span>"<strong></strong>"</span></a>
                   </li>
                 </ul>
                </div>
            </div>
         </li>
      </ul>

   </div>
</div>