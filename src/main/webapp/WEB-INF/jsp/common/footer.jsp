<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
</div>
<div id="g-footer">
	<div class="container">
		<span class="links">
			<a href="about.action">About us</a>
			<!--  <a href="partners.action">Partners</a>-->
			<a href="termOfService.action">Term of Service</a>
			<a href="privacyOfPolicy.action">Privacy of Policy</a>
			<!-- <a href="help.action">Help</a>-->
		</span>
		<span>
			Copyright 2014@Rating Bay
		</span>
	</div>
	<a href="javascript:;" id="gotop">Top</a>
</div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51161131-1', 'ratingbay.com');
  ga('send', 'pageview');

</script> 
<script type="text/javascript">
var RatingBay = {};
RatingBay.isLogined = <s:property value="isLogged"/>;
</script>

