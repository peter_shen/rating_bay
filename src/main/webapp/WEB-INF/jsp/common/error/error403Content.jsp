<%@ page language="java" isErrorPage="true" %>
<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<div class="container_full">
  <div class="section_title"><h2>403. That’s an error.</h2></div>
  <div class="section_titleline">
</div>

Your client does not have permission to get the URL from this server.That's all we know.
<br/><br/><br/>
    <div class="section_titleline">
<br/>
<%
if (exception != null) {
    out.println("Exception: " + exception);    
}
%>
</div>
</div>