<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>

<s:iterator value="searchPageDataBean" var="searchPageData">
<div class="main-content" id="search-wrapper">
  <div class="container clearfix">
    <div class="search-box">
      <div class="search-top">
        <strong>Sort By:</strong>
        <!--
        <a id="UN" class="btn order-btn <s:if test='d=="DESC"&&f=="UN"'>order-desc</s:if><s:else>order-asc</s:else> <s:if test='f=="UN"'>order-btn-active</s:if>" href="javascript:;">
          Username<em></em>
        </a>
        -->
        <!--
        <a id="GC" class="btn order-btn <s:if test='d=="ASC"'>order-asc</s:if><s:else>order-desc</s:else> <s:if test='f=="GC"'>order-btn-active</s:if>" href="javascript:;">
          Game Count<em></em>
        </a>
         -->
        <a id="TC" class="btn order-btn <s:if test='d=="ASC"&&f=="TC"'>order-asc</s:if><s:else>order-desc</s:else> <s:if test='f=="TC"'>order-btn-active</s:if>" href="javascript:;">
          Tweets Count<em></em>
        </a>
        <a id="FINGC" class="btn order-btn <s:if test='d=="ASC"&&f=="FINGC"'>order-asc</s:if><s:else>order-desc</s:else> <s:if test='f=="FINGC"'>order-btn-active</s:if>" href="javascript:;">
          Friends Count<em></em>
        </a>
        <a id="FEDC" class="btn order-btn <s:if test='d=="ASC"&&f=="FEDC"'>order-asc</s:if><s:else>order-desc</s:else> <s:if test='f=="FEDC"'>order-btn-active</s:if>" href="javascript:;">
          Followers Count<em></em>
        </a>

      </div>

      <s:if test="getUserList().getSize()==0">
        <h2 style="padding:25px;text-align:center;">We can't find any contents,please try to search other words</h2>
      </s:if>
      <s:else>
      <ul class="list image-list search-result-list user-search-result-list">
        <s:iterator value="userList" var="userOne">
        <li class="clearfix">
          <div class="l">
              <a class="thumb" href="user.action?userId=<s:property value="getId()"/>">
                <img id="${userOne.id}" class="avatar-128" src="<s:property value="getProfileImageURL()"/>" data-type="userThumb" onerror="imgError(this)"></img>
                <script>
                          var normalSrc="<s:property value="getProfileImageURL()"/>";
                          var largeSrc = normalSrc.replace("_normal", "");
                          $("img#${userOne.id}").attr("src", largeSrc);
                        </script>
              </a>
              <!--
              <ul class="u-profile-sta clearfix">
                <li>
                  <strong class="link">
                    <s:property value="getFriendsCount()"/>
                  </strong>
                  <span class="mute">Following</span>
                </li>
                <li>
                  <strong class="link">
                    <s:property value="getFollowersCount()"/>
                  </strong>
                  <span class="mute">Followers</span>
                </li>
                <li>
                  <strong class="link">
                    <s:property value="getGameCount()"/>
                  </strong>
                  <span class="mute">Games</span>
                </li>
              </ul>
               -->
          </div>
          <div class="r">
              <div class="intro">
                <div class="title">
                  <strong>
                    <a href="user.action?userId=<s:property value="getId()"/>">
                      <s:property value="getUsername()"/>
                    </a>
                  </strong>
                  <em class="time"><s:property value="getLocation()"/></em>
                  <span class="twitter-follow">
                      <a href="https://twitter.com/<s:property value="getUsername()"/>" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false" data-lang="en">
                          Follow @<s:property value="getUsername()"/>
                      </a>
                  </span>
                </div>
                <p class="u-intro mute">
                  <s:property value="getDescription()"/>
                </p>
              </div>
              <div class="tw-profile">
                <div class="t">
                    <h2>
                        <a href="https://twitter.com/intent/tweet?text=<s:property value="getUsername()"/> on ratingbay&url=http://www.ratingbay.com/user.action?userId=<s:property value="getId()"/>" target="_blank">
                            <img src="/assets/images/tw_logo_01.jpg"/>
                        </a>
                        Share
                    </h2>
                </div>
                <ul class="b tw-stats clearfix">
                    <li>
                        <label>
                            <strong>
                                <s:property value="getFriendsCount()"/>
                            </strong>
                            <span class="mute">Following</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <strong><s:property value="getFollowersCount()"/></strong>
                            <span class="mute">Followers</span>
                        </label>
                    </li>
                    <li>
                        <label>
                            <strong><s:property value="getGameCount()"/></strong>
                            <span class="mute">Games</span>
                        </label>
                    </li>
                </ul>
            </div>
          </div>
          </li>
          </s:iterator>
        </ul>
        </s:else>
<!--
    <div id="pagination">
    <s:iterator value="pageWidget">
      <s:if test='totalPages>0'>
        <strong>page:</strong>
      </s:if>
      <ul class="pagination">
        <s:if test="isFirstPage">
        </s:if>
        <s:else>
          <li>
            <a href="search.action?<s:property value="prevPageLink"/>">Prev Page</a>
          </li>
        </s:else>

        <s:iterator value="pageList">
          <s:if test="isMorePage">
            <li class="disable">
             <a href="javascript:;">...</a>
            </li>
          </s:if>
          <s:else>
            <s:if test="isCurPage">
              <li class="disable">
                <a href="javascript:;">
                  <s:property value="display"/>
                </a>
              </li>
            </s:if>
            <s:else>
              <li>
                <a href="search.action?<s:property value="link"/>">
                  <s:property value="display"/>
                </a>
              </li>
            </s:else>
          </s:else>
        </s:iterator>

        <s:if test="isLastPage">
        </s:if>
        <s:else>
          <li>
            <a href="search.action?<s:property value="nextPageLink"/>">
              Next Page
            </a>
          </li>
        </s:else>

      </ul>
      </s:iterator>
      </div>
     -->
    <script type="text/javascript">
    var RatingbaySearch = {
      'keyword': "<s:property value="kw"/>"
    };
    </script>
  </div>
</div>
</div>
</s:iterator>
<!-- twitter follow button -->
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
</script>

