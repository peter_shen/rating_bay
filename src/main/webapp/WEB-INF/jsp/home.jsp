<%@ include file="/WEB-INF/jsp/common/taglibs.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<s:iterator value="homePageDataBean" var="homePageData">

<!-- big banner -->
<div class="big-slide-box" id="big-banner">
    <ul class="big-slide">
    <c:forEach items="1,2,3" var="item" varStatus="status">
     <c:if test = "${item==1}">
      <li data-color="#1a1a1a" data-image="images/rt.jpg">
      <a class="slide-link" href="javascript:;" onclick="$('#signinForm').submit()"></a></li>
      </c:if>
      <c:if test = "${item==2}">
      <li data-color="#1a1a1a" data-image="images/rs.jpg"></li>
      </c:if>
      <c:if test = "${item==3}">
      <s:iterator value="headlineGames" var="headlineGame">
       <li data-color="#1a1a1a" data-image="<s:property value="getHeadlineImagePath()"/>">
       <a class="slide-link" href="game.action?gameId=<s:property value="getId()"/>"></a>
       <s:if test="getId() == 95">
        <div class="container slide-main">
          <dl class="slide-intro">
            <dt>
              <h2><span><s:property value="getTitle()"/></span></h2>
            </dt>
            <dd>
              <div>game score</div>
              <strong><s:property value="getSentimentScore()"/>%</strong>
            </dd>
            <dd>
              <p>Titanfall is actually incredibly fun.</p>
            </dd>
            <dd>
              <p>titanfall review: its getting sold on ebay. no depth, no story, crap game dynamics, one of the worst games ive ever played.</p>
            </dd>
          </dl>
        </div>
        </s:if><s:else>
        <div class="container slide-main">
          <dl class="slide-intro">
            <dt>
              <h2><span><s:property value="getTitle()"/></span></h2>
            </dt>
            <dd>
              <div>game score</div>
              <strong><s:property value="getSentimentScore()"/>%</strong>
            </dd>
            <dd>
              <p><s:property value="getMostPositiveTweet().getContent()"/></p>
            </dd>
            <dd>
              <p><s:property value="getMostNegativeTweet().getContent()"/></p>
            </dd>
          </dl>
        </div>
        </s:else>
      </li>
      </s:iterator>
      </c:if>
      </c:forEach>
    </ul>
</div>
<!-- <div id="site-slogan">
  Find what your friends say about games
</div> -->
<!-- main content -->
<div class="main-content nomargin">
  <div class="container clearfix">
    <!-- column left -->
    <div class="column-left">
      <dl class="m-cont-list">
        <dt class="icon-game">
          <b></b>
          <span>Top Recent Games</span>
        </dt>
        <dd>
          <ul class="tabs clearfix" id="topRecentGamesTab">
            <li class="l-tab active" BRT_event="click" BRT_call="goToLoved" BRT_exer="TopRecentGames" ><a href="javascript:;">Loved</a></li>
            <li class="r-tab" BRT_event="click" BRT_call="goToTweeted" BRT_exer="TopRecentGames"><a href="javascript:;" >Tweeted</a></li>
          </ul>
          <!-- ajax获取分页内容 -->
          <div id="topRecentGamesLoved">
          <div class="browse-prev browse-disable">
            <b></b>
          </div>
          <ul class="cont-list" data-url="rest/game/topRatedGames/" data-module="<li><div class='clearfix'><div class='l'><div class='radial-progress large-radial-progress' data-score='{{score}}'></div></div><div class='r'><a href='game.action?gameId={{id}}' target='_balnk'>{{name}}</a></div></div></li>">
            <s:iterator value="topRatedGames" var="topRatedGame">
            <li>
              <div class="clearfix">
                <div class="l">
                  <!-- <span class="like-percent"><s:property value="getSentimentScore()"/>%</span> -->
                  <div class="radial-progress large-radial-progress" data-score="<s:property value="getSentimentScore()"/>"></div>
                </div>
                <div class="r">
                  <a href="game.action?gameId=<s:property value="getId()"/>" target="_balnk">
                    <s:property value="getTitle()"/>
                  </a>
                </div>
              </div>
            </li>
            </s:iterator>
          </ul>
          <div class="browse-next">
            <b></b>
          </div>
          </div>
          <div id="topRecentGamesTweeted"  style="display:none">
          <div class="browse-prev browse-disable">
            <b></b>
          </div>
          <ul class="cont-list" data-url="rest/game/topTweetedGames/" data-module='<li> <div class="clearfix"><div class="l"><strong class="highlight">{{tweetsCount}}</strong><p>Tweets</p></div><div class="r"> <a href="game.action?gameId={{id}}" target="_balnk">{{name}}</a></div></div></li>'>
          </ul>
          <div class="browse-next">
            <b></b>
          </div>
          </div>
        </dd>
      </dl>
      <dl class="m-cont-list">
        <dt class="icon-talk">
          <b></b>
          <span>Latest Tweets</span>
        </dt>
        <dd>
          <ul class="tabs clearfix" id="latestTweetsTab">
            <li class="l-tab active" BRT_event="click" BRT_call="goToPopular" BRT_exer="LatestTweets"><a href="javascript:;">Popular</a></li>
            <li class="r-tab" BRT_event="click" BRT_call="goToRecent" BRT_exer="LatestTweets"><a href="javascript:;">Recent</a></li>
          </ul>
          <ul class="cont-list" id="latestTweetsPopular">
            <s:iterator value="popularTweets" var="popularTweet">
            <li>
              <blockquote class="twitter-tweet" width="300" id="blockquote_<s:property value="getId()"/>" ><a href="https://twitter.com/twitterapi/status/<s:property value="id"/>"></a></blockquote>
            </li>
            </s:iterator>
          </ul>
          <ul class="cont-list" id="latestTweetsRecent" style="display:none">
            <s:iterator value="recentTweets" var="recentTweet">
            <li>
              <blockquote class="twitter-tweet" width="300" id="blockquote_<s:property value="getId()"/>" ><a href="https://twitter.com/twitterapi/status/<s:property value="id"/>"></a></blockquote>
            </li>
            </s:iterator>
          </ul>
        </dd>
      </dl>
    </div>
    <!-- column-middle -->
    <div class="column-middle">
      <dl class="m-cont-list m-middle-list">
        <dt class="icon-game">
          <b></b>
          Recently Released<a class="more" href="allgame.action?ag=released">See More>></a>
        </dt>
        <dd>
          <ul class="game-list clearfix">
            <s:iterator value="newReleasedGames" var="newReleasedGame">
            <li>
              <div class="t-thumb">
                <div class="image-box">
                <a href="game.action?gameId=<s:property value="getId()"/>" class="thumb">
                  <img src="<s:property value="dvdImagePath"/>" data-type="gamePoster" onerror="imgError(this)"/>
                </a>
                <!-- <span class="like-percent"><s:property value="getSentimentScore()"/>%</span> -->
                <s:if test="getSentimentScore()!=0">
                <div class="radial-progress" data-type="overImage" data-score="<s:property value="getSentimentScore()"/>"></div>
                </s:if>
                </div>
                <div class="tweets">
                  <s:property value="getTweetCount()"/> Tweets
                </div>
              </div>
              <div class="b-desc">
                <strong><s:property value="title"/></strong>
                <div class="u-review mute">
                  <a href="user.action?userId=<s:property value="getMostRetweetTweet().getUserId()"/>" class="u-thumb">
                    <img src="<s:property value="getMostRetweetTweet().getTwitterUser().userProfileImgUrl"/>" BRT_img="imgUser" onError="imgError(this)" />
                  </a>
                  <span><s:property value="getMostRetweetTweet().getContent()"/></span>
                </div>
              </div>
            </li>
            </s:iterator>
          </ul>

        </dd>
      </dl>
      <dl class="m-cont-list m-middle-list">
        <dt class="icon-game">
          <b></b>
          Coming Soon<a class="more" href="allgame.action?ag=coming">See More>></a>
        </dt>
        <dd>
          <ul class="game-list clearfix">
            <s:iterator value="comingSoonGames" var="comingSoonGame">
            <li>
              <div class="t-thumb">
                <div class="image-box">
                <a href="game.action?gameId=<s:property value="getId()"/>" class="thumb">
                  <img src="<s:property value="dvdImagePath"/>" data-type="gamePoster" onerror="imgError(this)"/>
                </a>
                <!-- <span class="like-percent"><s:property value="getSentimentScore()"/>%</span> -->
                <s:if test="getSentimentScore()!=0">
                <div class="radial-progress" data-type="overImage" data-score="<s:property value="getSentimentScore()"/>"></div>
                </s:if>
                </div>
                <div class="tweets">
                  <s:property value="getTweetCount()"/> Tweets
                </div>
              </div>
              <div class="b-desc">
                <strong><s:property value="title"/></strong>
                <div class="u-review mute">
                  <a href="user.action?userId=<s:property value="getMostRetweetTweet().getUserId()"/>" class="u-thumb">
                    <img src="<s:property value="getMostRetweetTweet().userProfileImgUrl"/>" BRT_img="imgUser" onError="imgError(this)"/>
                  </a>
                  <span><s:property value="getMostRetweetTweet().getContent()"/></span>
                </div>
              </div>
            </li>
            </s:iterator>
          </ul>
        </dd>
      </dl>
    </div>
    <!-- column right -->
    <div class="column-right">
      <dl class="m-cont-list">
        <dt class="icon-video">
          <b></b>
          <span>Top Tweeted Videos</span>
        </dt>
        <dd>
          <ul class="cont-list" BRT_event="init" BRT_call="initImageAndTitle" BRT_exer="TopVideos">
            <s:iterator value="popularVideoLinks" var="popularVideoLink">
            <li class="has-thumb" BRT_video_link="<s:property value="link"/>">
              <div class="clearfix">
                <div class="l">
                  <a class="thumb" BRT_class="link" href="game.action?gameId=<s:property value="getIdGame()"/>">
                    <img BRT_class="image" alt=""/>
                    <b class="s-v-play"></b>
                  </a>
                </div>
                <div class="r">
                  <a class="thumb" BRT_class="link" href="game.action?gameId=<s:property value="getIdGame()"/>">
                    <strong BRT_class="title"></strong>
                    </a>
                
                 <!--  <a href="game.action?gameId=<s:property value="getIdGame()"/>">
                    <strong BRT_class="title"></strong>
                  </a> -->
                  <p class="vt">
                    <strong><s:property value="getTweetCount()"/></strong>
                    <span class="mute">Tweets</span>
                  </p>
                </div>
              </div>
            </li>
            </s:iterator>
          </ul>
        </dd>
      </dl>
      <!-- Most Influential Games -->
      <dl class="m-cont-list">
        <dt class="icon-gamer">
          <b></b>
          <span>Most Influential Gamers</span>
        </dt>
        <dd>
          <ul class="cont-list">
            <s:iterator value="topInfluencialUsers" var="topInfluencialUser">
            <li class="has-thumb vthumb">
              <div class="clearfix">
                <div class="l">
                  <a class="thumb" href="user.action?userId=<s:property value="getTwitterUserId()"/>" title="">
                  <img src="<s:property value="getUserProfileImgUrl()"/>" BRT_img="imgUser" onError="imgError(this)"/>
                  </a>
                  <span class="follow">
                    <a href="https://twitter.com/<s:property value="getUsername()"/>" class="twitter-follow-button" data-show-count="false" data-show-screen-name="false">Follow @<s:property value="getUsername()"/></a>
                  </span>
                </div>
                <div class="r">
                  <a href="user.action?userId=<s:property value="getTwitterUserId()"/>" target="_balnk">
                    <strong><s:property value="getUsername()"/></strong>
                  </a>
                  <p class="mute oneline-desc" title="<s:property value="getUserDescription()" />">
                    <s:property value="getUserDescription()" />
                  </p>
                  <ul class="fans-info">
                    <li>
                      <h4><s:property value="getUserFriendsCount()"/></h4>
                      <span>following</span>
                    </li>
                    <li>
                      <h4><s:property value="getUserFollowersCount()"/></h4>
                      <span>followers</span>
                    </li>
                  </ul>
                </div>
              </div>
            </li>
            </s:iterator>
          </ul>
        </dd>
      </dl>
    </div>
  </div>
</div>

</s:iterator>
<!-- twitter follow button -->
<script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
</script>